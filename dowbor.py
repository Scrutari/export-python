#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://dowbor.org
#-------------------------------------------------------------------

from scrutaridataexport import *
from scrutaridataexport_utils import *
from wordpressapi import *

#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
LANG_CSV_FILE = sys.argv[3]
NAME = "dowbor"
HOST = "dowbor.org"
PER_PAGE = "100"
SHORT_URL = "https://dowbor.org/?p="
POSTS_URL = "https://dowbor.org/wp-json/wp/v2/posts?per_page=" + PER_PAGE + "&categories="
LANG_DICT = utils_initDictFromCsv(LANG_CSV_FILE)


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def readLivroPage(jsonResponse):
    readPostPage(jsonResponse, "livro")
    
def readArtigoPage(jsonResponse):
    readPostPage(jsonResponse, "artigo")

def readPostPage(jsonResponse, corpus):
    jsonData = utils_toJson(jsonResponse)
    for item in jsonData:
        titre = wp_getText(item, "title")
        ficheId = item["id"]
        ficheExport = scrutariDataExport.newFiche(ficheId)
        ficheIdStr = str(ficheId)
        ficheExport.setTitre(titre)
        ficheExport.setHref(SHORT_URL + ficheIdStr)
        if ficheIdStr in LANG_DICT:
            ficheExport.setLang(LANG_DICT[ficheIdStr])
        else:
            ficheExport.setLang(utils_langidentify(titre))
        ficheExport.setDate(wp_getDate(item))
        ficheExport.setSoustitre(wp_getFirstParagraph(item, "excerpt"))
        ficheExport.addAttributeValue("sct", "authors", "Ladislau Dowbor")


#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#start
scrutariDataFile = utils_openScrutariDataFile(DESTINATION_DIRECTORY, NAME)
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))
metadataWrapper = utils_initMetadataWrapper(YAML_FILE)

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_livro
corpusMetadataExport = scrutariDataExport.newCorpus("livro");
metadataWrapper.fillCorpus(corpusMetadataExport)
wp_readPages(POSTS_URL + "678", HOST, readLivroPage)

#corpus_artigo
corpusMetadataExport = scrutariDataExport.newCorpus("artigo");
metadataWrapper.fillCorpus(corpusMetadataExport)
wp_readPages(POSTS_URL + "723", HOST, readArtigoPage)

#end
scrutariDataExport.endExport()
utils_closeScrutariDataFile(DESTINATION_DIRECTORY, NAME, scrutariDataFile)
utils_writeScrutariInfoFile(DESTINATION_DIRECTORY, NAME)
