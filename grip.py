#!/usr/bin/env python3
#-------------------------------------------------------------------
# Script de génération du fichier ScrutariData pour le site http://www.grip.org
#
#-------------------------------------------------------------------

#-------------------------------------------------------------------
# Ce script utilise le module BeautifulSoup http://www.crummy.com/software/BeautifulSoup/
# ainsi que le module lxml (voir l'appel de BeautifulSoup(htmlContent, "lxml"))
# Les autres modules utilisés sont standards
# scrutaridataexport correspond au contenu du fichier scrutaridataexport.py
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re
from bs4 import BeautifulSoup
from scrutaridataexport import *

#-------------------------------------------------------------------
# Ce script a un seul argument obligatoire : le chemin du répertoire de destination
# où sera enregistré les fichiers grip.scrutari-data.xml et grip.scrutari-info.xml
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
RACINE_SITE="http://www.grip.org"
ANGLAIS=["1221","1018"]

def checkType(type):
    rootUrl = RACINE_SITE + "/publications?type=" +type + "&field_theme_recherche_tid=All&field_auteur_target_id=All&page="
    page = 0
    while(True):
        url = rootUrl
        if page > 0:
            url = rootUrl + str(page)
        htmlContent = urllib.request.urlopen(url)
        soup = BeautifulSoup(htmlContent,"lxml")
        articleList = soup.select("div.view-content article")
        if len(articleList) > 0:
            for article in articleList:
                checkArticle(article)
            page = page + 1
        else:
            break
        
def checkArticle(article):
    h1 = article.find("h1")
    link = h1.find("a")
    href = link["href"]
    last = href.rfind('/')
    ficheId = href[last+1:]
    ficheExport = scrutariDataExport.newFiche(ficheId)
    if ficheId in ANGLAIS:
        ficheExport.setLang("en")
    else:
        ficheExport.setLang("fr")
    ficheExport.setHref(RACINE_SITE + href)
    ficheExport.setTitre(link.get_text())
    dateDiv = article.find("div", class_="submitted")
    if dateDiv:
        date_text = dateDiv.get_text()
        last = date_text.rfind(' ')
        date = date_text[last + 1:]
        ficheExport.setDate(date)
    auteursDiv = article.find("div", class_="field-name-field-auteur")
    if auteursDiv:
        auteurList = auteursDiv.find_all("div", class_="field-item")
        collectif = False
        for auteur in auteurList:
            auteur_text = auteur.get_text()
            if auteur_text.find('- Collectif') == 0:
                collectif = auteur_text[2:]
            else:
                if collectif:
                    auteur_text = collectif + " " + auteur_text
                    collectif = False
                ficheExport.addAttributeValue("sct","authors", auteur_text)
    body = article.find("div", class_="field-name-body")
    if body:
        firstP = body.find("p")
        if firstP:
            soustitre = checkSoustitre(firstP.get_text())
            ficheExport.setSoustitre(soustitre)

def checkSoustitre(soustitre):
    soustitreLen = len(soustitre)
    if soustitreLen < 150:
        return soustitre
    idx = soustitre.find(".", 150, soustitreLen)
    if idx > 0:
        return soustitre[0:(idx+1)]
    return soustitre
    

#-------------------------------------------------------------------
# Procédure principale
#-------------------------------------------------------------------

#Départ de l'exportation
scrutariDataPath = DESTINATION_DIRECTORY + "grip.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))


baseMetadataExport = scrutariDataExport.startExport()
baseMetadataExport.setAuthority("grip.org")
baseMetadataExport.setBaseName("site")
baseMetadataExport.setBaseIcon("http://static.scrutari.net/grip.png")
baseMetadataExport.setIntitule(INTITULE_SHORT, "fr", "Grip")
baseMetadataExport.setIntitule(INTITULE_LONG, "fr", "Grip, Groupe de Recherche et d’Information sur la Paix et la sécurité")
baseMetadataExport.addLangUI("fr")


#Création du corpus des analyses
corpusMetadataExport = scrutariDataExport.newCorpus("analyse");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Analyses")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Analyse n° ")

checkType("note_analyse")

#Création du corpus des éclairages
corpusMetadataExport = scrutariDataExport.newCorpus("eclairage");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Éclairages")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Éclairage n° ")

checkType("breve")

#Création du corpus des livres
corpusMetadataExport = scrutariDataExport.newCorpus("rapport");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Rapport")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Rapport n° ")

checkType("rapport_du_grip")

#Création du corpus des livres
corpusMetadataExport = scrutariDataExport.newCorpus("livre");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Livres")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Livre n° ")

checkType("livre")



#Fin de l'export
scrutariDataExport.endExport()
scrutariDataFile.close()

#Renommage du fichier temporaire
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#Création du fichier d'information avec la date du jour
scrutariInfoFile = open(DESTINATION_DIRECTORY + "grip.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "grip.scrutari-data.xml")
scrutariInfoFile.close()