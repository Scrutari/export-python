#!/usr/bin/env python3
#-------------------------------------------------------------------
# http://www.tiess.ca
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *

#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
RACINE_SITE="http://www.tiess.ca"
EXCLUDE=["418","1305"]

#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def checkPublications():
    publicationUrl= RACINE_SITE + "/publications/"
    htmlContent =  urllib.request.urlopen(publicationUrl)
    soup = BeautifulSoup(htmlContent,"lxml")
    checkList(soup)
    
def checkList(soup):
    articles = soup.find_all("article", class_="tg-item")
    for article in articles:
        if not testRapportAnnuel(article):
            h2 = article.find("h2", class_="tg-item-title")
            if h2:
                link = h2.find("a")
                if link:
                    if isValid(link["href"]):
                        checkPost(link["href"])
            
def checkPost(href):
    htmlContent = urllib.request.urlopen(href)
    soup = BeautifulSoup(htmlContent,"lxml")
    link = soup.find("link", rel="shortlink")
    if not link:
        print(href)
        return
    href = link["href"]
    idx = href.rfind("=")
    postId = href[(idx +1):]
    if postId in EXCLUDE:
        return
    ficheExport = scrutariDataExport.newFiche(postId)
    ficheExport.setTitre(getOg(soup, "og:title"))
    ficheExport.setSoustitre(getOg(soup, "og:description"))
    ficheExport.setHref(href)
    ficheExport.setLang("fr")
    leftDiv = soup.find("div", class_="left_div")
    if leftDiv:
        span = leftDiv.find("span")
        if span:
             for text in span.stripped_strings:
                annee = int(text[len(text)-4:])
                if annee:
                    ficheExport.setDate(str(annee))
                    break;
        authors = leftDiv.find_all("a", rel="author")
        for author in authors:
            ficheExport.addAttributeValue("sct", "authors", author.get_text())
    
def getOg(soup, name):
    meta = soup.find("meta", property=name)
    if meta:
        return meta["content"]
    else:
        return ""
    
def isValid(href):
    idx = href.find(".pdf")
    if idx > 0:
        return False
    else:
        return True
    
def testRapportAnnuel(article):
    category = article.find("a", attrs={'data-term-id' : "42"})
    if category:
        return True
    else:
        return False
    
    
#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding = "UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "tiess.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding="UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_article
corpusMetadataExport = scrutariDataExport.newCorpus("publication");
metadataWrapper.fillCorpus(corpusMetadataExport)

checkPublications()


#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "tiess.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "tiess.scrutari-data.xml")
scrutariInfoFile.close()

