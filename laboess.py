#!/usr/bin/env python3

#-------------------------------------------------------------------
# http://www.lelabo-ess.org
#-------------------------------------------------------------------

import sys, shutil, urllib.request, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *

#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
ROOT="https://www.lelabo-ess.org/"
INDEX_URL="https://www.lelabo-ess.org/-nos-publications-.html"
LANG="fr"
PREFIX="article-titre-"
PREFIX_LENGTH=len(PREFIX)
SHORT_URL="https://www.lelabo-ess.org/spip.php?article"


def checkIndex(url, checkPagination):
    htmlContent = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlContent,"lxml")
    div = soup.find("div", class_="menu_articles")
    listArticles(div)
    if checkPagination:
        pagination = div.find("p", class_="pagination")
        if pagination:
            links = pagination.find_all("a")
            for link in links:
                checkIndex(getPaginationUrl(link["href"]), False)
    
def listArticles(div):
    ul = div.find("ul")
    for li in ul.find_all("li"):
        link = li.find("a")
        if link:
            href=link["href"]
            loadArticle(href)
            
def loadArticle(href):
    url = ROOT + href
    htmlContent = urllib.request.urlopen(ROOT + href)
    soup = BeautifulSoup(htmlContent,"lxml")
    main = soup.find("div", class_="main")
    h1 = main.find("h1")
    ficheId = getFicheId(h1)
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setTitre(getTitre(h1))
    ficheExport.setLang(LANG)
    ficheExport.setHref(SHORT_URL + ficheId)
    ficheExport.setDate(getDate(main))
    ficheExport.setSoustitre(getSoustitre(main))
    
def getFicheId(element):
    for className in element["class"]:
        if className.startswith(PREFIX):
            return className[PREFIX_LENGTH:]
    return False

def getTitre(element):
    return element.get_text()

def getDate(element):
    abbr = element.find("abbr", class_="published")
    title = abbr["title"]
    return title[0:10]

def getSoustitre(element):
    div = element.find("div", class_="chapo")
    if div:
        p = div.find("p")
        if p:
            for span in p.find_all("span", class_="gl_dl"):
                span.clear()
            return p.get_text()
    return False


def getPaginationUrl(href):
    anchor = href.rfind("#")
    href = href[0:anchor]
    return ROOT + href + "&var_ajax=1&var_ajax_env=ZKQxeamcxVqmDHhq2FTEhjacXY%2F4ZBiacut0LZkwLFtqVyVcKqBUR0iAdyM8t2ciYesMS%2BSe9m2X7qJYECfcldHyA6Ds1XYEzv6mkDJkLIZWi29OxYCupKmAqwqozUo1zxfE%2FKdZT2oMx6pa&var_ajax_ancre=pagination_articles2&var_t=1599683443276"

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------


#wrapper
yamlFile = open(YAML_FILE, encoding="UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "laboess.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_experience
corpusMetadataExport = scrutariDataExport.newCorpus("publication");
metadataWrapper.fillCorpus(corpusMetadataExport)

checkIndex(INDEX_URL, True)


#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "laboess.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "laboess.scrutari-data.xml")
scrutariInfoFile.close()
