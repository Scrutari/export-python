#!/usr/bin/env python3

#-------------------------------------------------------------------
# http://www.doc-transition-energetique.info (dépendant de http://www.cler.org)
#-------------------------------------------------------------------

import sys, shutil, urllib.request, yaml, datetime
from bs4 import BeautifulSoup
#https://docs.python.org/3.5/library/xml.etree.elementtree.html
import xml.etree.ElementTree as ET
from scrutaridataexport import *



#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
OAI_DIRECTORY = sys.argv[2]
YAML_FILE = sys.argv[3]
NAMESPACES={'oai': "http://www.openarchives.org/OAI/2.0/",
            'oai_dc': "http://www.openarchives.org/OAI/2.0/oai_dc/",
            'dc': "http://purl.org/dc/elements/1.1/"}
URL="http://www.doc-transition-energetique.info/OAI?verb=ListRecords&set=Articles&metadataPrefix=oai_dc"


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

#http://www.doc-transition-energetique.info/OAI?verb=ListIdentifiers&set=Articles&metadataPrefix=oai_dc

def readIds():
    tree = ET.parse(OAI_DIRECTORY + "list.xml");
    identifiers = tree.getroot().findall("oai:ListIdentifiers/oai:header/oai:identifier", NAMESPACES)
    print(str(len(identifiers)))
    for identifier in identifiers:
        text = identifier.text
        idx = text.find(":")
        ficheId = text[idx+1:]
        
def yearLoop():
    currentYear = datetime.date.today().year
    year = 2002
    while year < currentYear:
        checkYearFile(year)
        year = year + 1
    currentYearFilePath = getYearFilePath(currentYear)
    download(currentYear, currentYearFilePath)
    readYearFile(currentYearFilePath)
    
        
def checkYearFile(year):
    yearFilePath = getYearFilePath(year)
    if not os.path.exists(yearFilePath):
        download(year, yearFilePath)
    readYearFile(yearFilePath)
    
        
def getYearFilePath(year):
    return OAI_DIRECTORY + "articles-" + str(year) + ".xml"

def download(year, destinationPath):
    url = URL + "&from=" + str(year) + "-01-01&until=" + str(year) + "-12-31"
    urllib.request.urlretrieve(url, destinationPath)
    
def readYearFile(path):
    checkXML(path)
    tree = ET.parse(path);
    records = tree.getroot().findall("oai:ListRecords/oai:record", NAMESPACES)
    for record in records:
        if isFromCler(record):
            readRecord(record)
        
def readRecord(record):
    ficheExport = scrutariDataExport.newFiche(getId(record))
    ficheExport.setDate(record.find("oai:header/oai:datestamp", NAMESPACES).text)
    dc = record.find("oai:metadata/oai_dc:dc", NAMESPACES)
    titreElement = dc.find("dc:title", NAMESPACES)
    ficheExport.setTitre(titreElement.text)
    hrefElement = dc.find("dc:identifier", NAMESPACES)
    ficheExport.setHref(hrefElement.text)
    ficheExport.setLang(getLang(dc))
    ficheExport.setSoustitre(getSoustitre(dc))
    subjects = dc.findall("dc:subject", NAMESPACES)
    for subject in subjects:
        ficheExport.addAttributeValue("sct", "tags", subject.text)
    

def getId(record):
    identifier = record.find("oai:header/oai:identifier", NAMESPACES)
    text = identifier.text
    idx = text.find(":")
    return text[idx+1:]

def isFromCler(record):
    publishers = record.findall("oai:metadata/oai_dc:dc/dc:publisher", NAMESPACES)
    for publisher in publishers:
        if publisher.text == "Réseau pour la transition énergétique - CLER":
            return True
    return False

def getLang(dc):
    language = dc.find("dc:language", NAMESPACES)
    if language is not None:
        languageString = language.text
        if languageString == "Français":
            return "fr"
        else:
            print(languageString)
            return ""
    return ""

def getSoustitre(dc):
    description = dc.find("dc:description", NAMESPACES)
    if description is None:
        return ""
    text = description.text
    text = text.replace('ARTICLE EN LIGNE', '')
    return text
    

def checkXML(path):
    #read input file
    fin = open(path, "rt")
    #read file contents to string
    data = fin.read()
    #replace all occurrences of the required string
    data = data.replace('<dc:description ', '<dc:description>')
    data = data.replace('<http:', '&lt;http:')
    #close the input file
    fin.close()
    #open the input file in write mode
    fin = open(path, "wt")
    #overrite the input file with the resulting data
    fin.write(data)
    #close the file
    fin.close()

    

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------


#wrapper
yamlFile = open(YAML_FILE, encoding="UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)


#start
scrutariDataPath = DESTINATION_DIRECTORY + "cler-ressources.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_article
corpusMetadataExport = scrutariDataExport.newCorpus("article");
metadataWrapper.fillCorpus(corpusMetadataExport)

yearLoop()


#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "cler-ressources.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "cler-ressources.scrutari-data.xml")
scrutariInfoFile.close()
