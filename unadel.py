#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://unadel.org
#-------------------------------------------------------------------

import sys, shutil, urllib.request, json, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *

#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
PER_PAGE="100"
POST_URL = "https://unadel.org/wp-json/wp/v2/posts?per_page=" + PER_PAGE
TAGS_URL = "https://unadel.org/wp-json/wp/v2/tags?per_page=" + PER_PAGE
LANG="fr"
#Actualités, Actualités législatives, Evénements & RDV des réseaux, Autres catégories, Edito, Tribune, Forum permanent des pays
EXCLUDE=[66,8,1,7,54,44,26] 

tagMap = {}

def initMap(itemMap, url):
    jsonResponse = urllib.request.urlopen(url)
    totalPages = int(jsonResponse.getheader("X-WP-TotalPages"))
    initItems(itemMap, jsonResponse)
    if totalPages > 1:
      for i in range(2, totalPages + 1):
        jsonResponse = urllib.request.urlopen(url + "&page=" + str(i))
        initItems(itemMap, jsonResponse)
    
        
def initItems(itemMap,jsonResponse):
    jsonData = json.loads(jsonResponse.read().decode("UTF-8"))
    for item in jsonData:
        itemMap[item["id"]] = convert(item["name"])

def loadPosts(url):
    jsonResponse = urllib.request.urlopen(url)
    totalPages = int(jsonResponse.getheader("X-WP-TotalPages"))
    readPostPage(jsonResponse)
    if totalPages > 1:
      for i in range(2, totalPages + 1):
        jsonResponse = urllib.request.urlopen(url + "&page=" + str(i))
        readPostPage(jsonResponse)
    
    

def readPostPage(jsonResponse):
  jsonData = json.loads(jsonResponse.read().decode("UTF-8"))
  for item in jsonData:
      if isRessource(item):
            ficheId = item["id"]
            ficheExport = scrutariDataExport.newFiche(ficheId)
            ficheExport.setTitre(convert(item["title"]["rendered"]))
            ficheExport.setHref("https://unadel.org/?p=" + str(ficheId))
            ficheExport.setLang(LANG)
            ficheExport.setDate(getDate(item))
            content = item["content"]["rendered"]
            if content:
                soup = BeautifulSoup(content,"lxml")
                firstP = soup.find("p")
                if firstP:
                    ficheExport.setSoustitre(firstP.get_text())
            tagArray = item["tags"]
            for tagId in tagArray:
                if tagId in tagMap:
                    tagName = tagMap[tagId]
                    ficheExport.addAttributeValue("sct", "tags", tagName)

def getDate(item):
    date = item["date"][0:10]
    if date[0] == "0":
        date = item["modified"][0:10]
    return date

def isRessource(item):
    for category in item["categories"]:
        if not category in EXCLUDE: 
            return True
    return False

def convert(text):
    return BeautifulSoup(text,"lxml").get_text()


#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#init
#initMap(authorMap, AUTHORS_URL)
initMap(tagMap, TAGS_URL)
#initLanguageMap(languageMap, LANGUAGES_URL)  


#wrapper
yamlFile = open(YAML_FILE, encoding="UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "unadel.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_post
corpusMetadataExport = scrutariDataExport.newCorpus("post");
metadataWrapper.fillCorpus(corpusMetadataExport)

loadPosts(POST_URL)


#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "unadel.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "unadel.scrutari-data.xml")
scrutariInfoFile.close()

