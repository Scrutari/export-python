#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://energy-cities.eu
#-------------------------------------------------------------------

from bs4 import BeautifulSoup
from scrutaridataexport import *
from scrutaridataexport_utils import *
from wordpressapi import *

#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
NAME = "energycities"
HOST = "energy-cities.eu"
PER_PAGE = "100"
PUBLICATION_URL = "https://energy-cities.eu/wp-json/wp/v2/publications?per_page=" + PER_PAGE
PRACTICE_URL = "https://energy-cities.eu/wp-json/wp/v2/best_practices?per_page=" + PER_PAGE
TOPIC_URL = "https://energy-cities.eu/wp-json/wp/v2/topic?per_page=" + PER_PAGE
LANG = "en"
SHORT_URL = "https://energy-cities.eu/?p="


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def readPublicationPage(jsonResponse):
    readPostPage(jsonResponse, "publication")
    
def readPracticePage(jsonResponse):
    readPostPage(jsonResponse, "practice")

def readPostPage(jsonResponse, corpus):
  jsonData = utils_toJson(jsonResponse)
  for item in jsonData:
        ficheId = item["id"]
        ficheExport = scrutariDataExport.newFiche(ficheId)
        ficheExport.setTitre(wp_getText(item, "title"))
        ficheExport.setHref(SHORT_URL + str(ficheId))
        ficheExport.setLang(LANG)
        ficheExport.setDate(wp_getDate(item))
        ficheExport.setSoustitre(wp_getFirstParagraph(item, "content"))
        topicArray = item["topic"]
        for topicId in topicArray:
            motcleId = str(topicId)
            if topicBuffer.containsMotcle(motcleId):
                scrutariDataExport.addIndexation(corpus, ficheId, "topic", motcleId, 1)


#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#start
scrutariDataFile = utils_openScrutariDataFile(DESTINATION_DIRECTORY, NAME)
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))
metadataWrapper = utils_initMetadataWrapper(YAML_FILE)

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#topicBuffer
topicBuffer = ThesaurusBuffer()
wp_populateThesaurus(topicBuffer, TOPIC_URL, "en", HOST)

#corpus_publication
corpusMetadataExport = scrutariDataExport.newCorpus("publication");
metadataWrapper.fillCorpus(corpusMetadataExport)
wp_readPages(PUBLICATION_URL, HOST, readPublicationPage)

#corpus_pratice
corpusMetadataExport = scrutariDataExport.newCorpus("practice");
metadataWrapper.fillCorpus(corpusMetadataExport)
wp_readPages(PRACTICE_URL, HOST, readPracticePage)

#thesaurus_topic
thesaurusMetadataExport = scrutariDataExport.newThesaurus("topic");
metadataWrapper.fillThesaurus(thesaurusMetadataExport)
topicBuffer.exportMotscles(scrutariDataExport)

#end
scrutariDataExport.endExport()
utils_closeScrutariDataFile(DESTINATION_DIRECTORY, NAME, scrutariDataFile)
utils_writeScrutariInfoFile(DESTINATION_DIRECTORY, NAME)
