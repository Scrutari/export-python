#!/usr/bin/env python3
#-------------------------------------------------------------------
# Script de génération du fichier ScrutariData pour le site http://www.amisdelaterre.org
#
#-------------------------------------------------------------------

#-------------------------------------------------------------------
# Ce script utilise le module BeautifulSoup http://www.crummy.com/software/BeautifulSoup/
# ainsi que le module lxml (voir l'appel de BeautifulSoup(htmlContent, "lxml"))
# Les autres modules utilisés sont standards
# scrutaridataexport correspond au contenu du fichier scrutaridataexport.py
#-------------------------------------------------------------------

import sys, shutil, urllib.request
from bs4 import BeautifulSoup
from scrutaridataexport import *

#-------------------------------------------------------------------
# Ce script a un seul argument obligatoire : le chemin du répertoire de destination
# où sera enregistré les fichiers ijcrr.scrutari-data.xml et ijcrr.scrutari-info.xml
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
RUBRIQUES=[60,68,56,64,72, 76,81,89,318,88,95, 100,108,114,101,109, 154,162,170,254,158,166,174, 134,147,127]
RACINE_SITE="http://www.amisdelaterre.org/"
URL_SET = set()

def checkRubriques():
    for id_rubrique in RUBRIQUES:
        htmlContent = urllib.request.urlopen(RACINE_SITE + "spip.php?rubrique" + str(id_rubrique))
        soup = BeautifulSoup(htmlContent,"lxml")
        checkListeArticles(soup)
        paginationDiv = soup.find("div", class_="pagination")
        if paginationDiv:
            paginationLinks = paginationDiv.find_all("a")
            for paginationLink in paginationLinks:
                paginationHtmlContent = urllib.request.urlopen(RACINE_SITE + paginationLink["href"])
                paginationSoup = BeautifulSoup(paginationHtmlContent,"lxml")
                checkListeArticles(paginationSoup)
            
def checkListeArticles(soup):
    links = soup.select("strong.h3.entry-title > a")
    for link in links:
        checkArticle(link["href"])

def checkArticle(href):
    if href in URL_SET:
        return
    URL_SET.add(href)
    htmlContent = urllib.request.urlopen(RACINE_SITE + href)
    soup = BeautifulSoup(htmlContent,"lxml")
    titreSpans = soup.select("header.cartouche h1 > span")
    if len(titreSpans) == 0:
        return
    titreSpan = titreSpans[0]
    ficheId = ""
    for className in titreSpan["class"]:
        if className.startswith("article-titre-"):
            ficheId = className[14:]
    if len(ficheId) == 0:
        return
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setTitre(titreSpan.get_text())
    ficheExport.setLang("fr")
    ficheExport.setHref(RACINE_SITE + "spip.php?article" + ficheId)
    time = soup.find("time", pubdate="pubdate")
    if time:
        ficheExport.setDate(time["datetime"][0:10])
    soustitre = checkSoustitre(soup.find("div", class_="article-chapo-"+ficheId))
    ficheExport.setSoustitre(soustitre)

def checkSoustitre(element):
    if not element:
        return ""
    soustitre = element.get_text()
    idx = soustitre.find("\n")
    if idx > 0:
        soustitre = soustitre[0:idx]
    soustitreLen = len(soustitre)
    if soustitreLen < 150:
        return soustitre
    idx = soustitre.find(".", 150, soustitreLen)
    if idx > 0:
        return soustitre[0:(idx+1)]
    return soustitre
    
    
    

#-------------------------------------------------------------------
# Procédure principale
#-------------------------------------------------------------------

#Départ de l'exportation
scrutariDataPath = DESTINATION_DIRECTORY + "amisdelaterre.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))


baseMetadataExport = scrutariDataExport.startExport()
baseMetadataExport.setAuthority("amisdelaterre.org")
baseMetadataExport.setBaseName("site")
baseMetadataExport.setBaseIcon("http://static.scrutari.net/amisdelaterre.png")
baseMetadataExport.setIntitule(INTITULE_SHORT, "fr", "Les Amis de la Terre")
baseMetadataExport.setIntitule(INTITULE_LONG, "fr", "Les Amis de la Terre, réseau écologiste mondial")
baseMetadataExport.addLangUI("fr")




#Création du corpus des possibles
corpusMetadataExport = scrutariDataExport.newCorpus("publication");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Publications")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Publication n° ")


#Récupération des pages avec la liste des articles par langue
checkRubriques()

#Fin de l'export
scrutariDataExport.endExport()
scrutariDataFile.close()

#Renommage du fichier temporaire
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#Création du fichier d'information avec la date du jour
scrutariInfoFile = open(DESTINATION_DIRECTORY + "amisdelaterre.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "amisdelaterre.scrutari-data.xml")
scrutariInfoFile.close()