#!/usr/bin/env python3

#-------------------------------------------------------------------
# http://www.territoires-energie-positive.fr
#-------------------------------------------------------------------

import sys, shutil, urllib.request, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *

#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
ROOT="http://www.territoires-energie-positive.fr"
INDEX_URL="http://www.territoires-energie-positive.fr/experiences"
LANG="fr"

hrefSet = {}


def checkIndex(url):
    htmlContent = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlContent,"lxml")
    ul = soup.find("ul", class_="slides")
    links = ul.find_all("a")
    for link in links:
        href = link["href"]
        if len(href) > 0:
            if not href in hrefSet:
                hrefSet[href] = True
                loadResource(href)

def loadResource(href):
    htmlContent = urllib.request.urlopen(href)
    soup = BeautifulSoup(htmlContent,"lxml")
    div = soup.find("div", id="section_to_print")
    ficheId = getFicheId(href)
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setTitre(getTitre(div))
    ficheExport.setLang(LANG)
    ficheExport.setDate(getDate(div))
    ficheExport.setHref(href)
    ficheExport.setSoustitre(getSoustitre(div))
    

def getFicheId(href):
    lastIndex = href.rfind("/")
    return href[lastIndex+1:]

def getTitre(element):
    return element.find("h2", class_="content-title").get_text()

def getSoustitre(element):
    div = element.find("div", class_="attribute-short")
    if div:
        p = div.find("p")
        if p:
            return p.get_text()
    return False

def getDate(element):
    div = element.find("div", class_="publication")
    if div:
        text = div.get_text()
        lastIndex = text.rfind("/")
        return text[lastIndex+1:]

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------


#wrapper
yamlFile = open(YAML_FILE, encoding="UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "tepos.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_experience
corpusMetadataExport = scrutariDataExport.newCorpus("experience");
metadataWrapper.fillCorpus(corpusMetadataExport)

checkIndex(INDEX_URL)


#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "tepos.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "tepos.scrutari-data.xml")
scrutariInfoFile.close()
