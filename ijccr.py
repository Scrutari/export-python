#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://ijccr.net/
#-------------------------------------------------------------------


#-------------------------------------------------------------------
# Ce script récupère sur la page d'accueil les liens vers les volumes (menu volumes)
# puis construit les fiches à partir des informations sur la page du volume
# la principale information qui manque est celle de l'auteur qui est difficile récupérable
# à partir de la page de l'article
#-------------------------------------------------------------------

from bs4 import BeautifulSoup
from scrutaridataexport import *
from scrutaridataexport_utils import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
LANG_CSV_FILE = sys.argv[3]
NAME = "ijccr"
HOST = "ijccr.net"
ISSUES_URL = "https://ijccr.net/past-issues/"
LANG_DICT = utils_initDictFromCsv(LANG_CSV_FILE)


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def checkIssues():
    "Lit le tableau de la page des past issues"
    htmlContent = utils_urlopen(ISSUES_URL, HOST)
    pastIssues = BeautifulSoup(htmlContent, "lxml")
    tables = pastIssues.find_all("table")
    for table in tables:
        rows = table.find_all("tr")
        for row in rows:
            cells = row.find_all("td")
            year = cells[1].get_text().strip()
            if year.isdigit():
                reference = cells[2].find("a")
                if reference:
                    titre = reference.get_text()
                    if not titre.startswith("Editorial"):
                        href = reference["href"]
                        authors = cells[3].get_text()
                        readPage(titre, href, year, authors)
                            
                                    
def readPage(titre, href, year, authors):
    try:
        pageResponse = utils_urlopen(href, HOST)
    except:
        return
    page = BeautifulSoup(pageResponse, "lxml")
    article = page.find("article")
    if not article:
        return
    if article["id"]:
        ficheId = article["id"][5:] #l'identifiant est de la forme post-1126
        ficheExport = scrutariDataExport.newFiche(ficheId)
        ficheExport.setDate(year)
        if ficheId in LANG_DICT:
            ficheExport.setLang(LANG_DICT[ficheId])
        else:
            ficheExport.setLang(utils_langidentify(titre))
        ficheExport.setHref(href)
        ficheExport.setTitre(titre)
        entryContent = article.find("div", class_="entry-content")
        #if entryContent:
        #    entryContentP = entryContent.find("p")
        #    if entryContentP:
        #        ficheExport.setSoustitre(entryContentP.get_text())
        if len(authors) > 0:
            ficheExport.addAttributeValue("sct", "authors", authors)
    
    


#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#start
scrutariDataFile = utils_openScrutariDataFile(DESTINATION_DIRECTORY, NAME)
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))
metadataWrapper = utils_initMetadataWrapper(YAML_FILE)

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_paper
corpusMetadataExport = scrutariDataExport.newCorpus("paper");
metadataWrapper.fillCorpus(corpusMetadataExport)
checkIssues()

#end
scrutariDataExport.endExport()
utils_closeScrutariDataFile(DESTINATION_DIRECTORY, NAME, scrutariDataFile)
utils_writeScrutariInfoFile(DESTINATION_DIRECTORY, NAME)
