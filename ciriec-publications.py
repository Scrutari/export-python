#!/usr/bin/env python3

#-------------------------------------------------------------------
# http://www.ciriec.uliege.be/
#-------------------------------------------------------------------

import langid
from bs4 import BeautifulSoup
from scrutaridataexport_utils import *
from scrutaridataexport import *
from wordpressapi import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
NAME = "ciriec-publications"
HOST = "www.ciriec.uliege.be"
INDEX_URL = "http://www.ciriec.uliege.be/publications/wp/"
ROOT_URL = "http://www.ciriec.uliege.be/"

FR=["1703","1612","1610","1608","1606","1602","1601","1505", "1504", "1503", "1502", "1415", "1414", "1406", "1302", "1201", "1108", "1104", "1011", "1010", "1009", "1008", "1007", "1005", "0906", "0809", "0807", "0805", "0804", "0801", "0705", "0704", "0702", "0701"]
NL=["1613","1611","1609","1607"]
ES=["1604"]
DE=["1004"]

PER_PAGE="100"
SHORT_URL= "http://www.ciriec.uliege.be/?p="
BOOK_URL = "http://www.ciriec.uliege.be/wp-json/wp/v2/pages?parent=3834&per_page=" + PER_PAGE
REPORT_URL = "http://www.ciriec.uliege.be/wp-json/wp/v2/pages?parent=3837&per_page=" + PER_PAGE


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def checkIndex():
    htmlContent = utils_urlopen(INDEX_URL, HOST)
    soup = BeautifulSoup(htmlContent,"lxml")
    accordionDivs = soup.find_all("div", class_="accordion-content")
    for div in accordionDivs:
        ps = div.find_all("p")
        for p in ps:
            checkParagraph(p)

def checkParagraph(p):
    strong = p.find("strong")
    if not strong:
        return
    link = p.find("a")
    if not link:
        return
    ficheId = strong.get_text().strip()
    if len(ficheId) == 0:
        return
    ficheId = ficheId[2:].strip()
    ficheId = ficheId.replace("/", "")
    date = "20" + ficheId[0:2]
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setLang(getLang(ficheId))
    ficheExport.setTitre(link.get_text())
    ficheExport.setHref(link["href"])
    ficheExport.setDate(date)
    authors = getAuthors(p.get_text())
    for author in authors:
        author = author.strip()
        if len(author) > 0:
            ficheExport.addAttributeValue("sct", "authors", author)
    
def getLang(ficheId):
    if ficheId in FR:
        return "fr"
    elif ficheId in NL:
        return "nl"
    elif ficheId in ES:
        return "es"
    elif ficheId in DE:
        return "de"
    else:
        return "en"
    
def getAuthors(text):
    id1 = text.find("–")
    if id1 > 0:
        id2 = text.find("–", id1 + 1)
        if id2 > 0:
            return text[id1+1:id2].split("/")
    return []

def readBookApi(jsonResponse):
    readApiPage(jsonResponse, "book")

def readReportApi(jsonResponse):
    readApiPage(jsonResponse, "report")
    
def readApiPage(jsonResponse, corpus):
  jsonData = utils_toJson(jsonResponse)
  for item in jsonData:
    soustitre = wp_getFirstParagraph(item, "excerpt")
    if len(soustitre) > 0:
        titre = wp_getText(item, "title")
        ficheId = item["id"]
        ficheExport = scrutariDataExport.newFiche(ficheId)
        ficheExport.setTitre(titre)
        ficheExport.setHref(SHORT_URL + str(ficheId))
        ficheExport.setLang(utils_langidentify(titre))
        ficheExport.setDate(wp_getDate(item))
        ficheExport.setSoustitre(soustitre)


#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#start
scrutariDataFile = utils_openScrutariDataFile(DESTINATION_DIRECTORY, NAME)
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))
metadataWrapper = utils_initMetadataWrapper(YAML_FILE)

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_wp
corpusMetadataExport = scrutariDataExport.newCorpus("wp");
metadataWrapper.fillCorpus(corpusMetadataExport)
checkIndex()

#corpus_book
corpusMetadataExport = scrutariDataExport.newCorpus("book");
metadataWrapper.fillCorpus(corpusMetadataExport)
wp_readPages(BOOK_URL, HOST, readBookApi)

#corpus_report
corpusMetadataExport = scrutariDataExport.newCorpus("report");
metadataWrapper.fillCorpus(corpusMetadataExport)
wp_readPages(REPORT_URL, HOST, readReportApi)

#end
scrutariDataExport.endExport()
utils_closeScrutariDataFile(DESTINATION_DIRECTORY, NAME, scrutariDataFile)
utils_writeScrutariInfoFile(DESTINATION_DIRECTORY, NAME)
