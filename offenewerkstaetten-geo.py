#!/usr/bin/env python3

#-------------------------------------------------------------------
# http://redeconvergir.net/
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re, json, yaml, csv
from bs4 import BeautifulSoup
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
MAP_URL = "http://www.offene-werkstaetten.org/werkstatt-suche"
ROOT_URL = "http://www.offene-werkstaetten.org/werkstatt/"
START = "vow.Map("
END = ", 'search'"


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------
existingSet = {}

def loadMapPage():
    htmlResponse =  urllib.request.urlopen(MAP_URL)
    soup = BeautifulSoup(htmlResponse.read(), "lxml")
    return soup
    
    
def extractWorkshops(soup):
    scripts = soup.find_all("script")
    for script in scripts:
        if "src" not in script:
            scriptContent = script.get_text()
            start = scriptContent.find(START)
            end = scriptContent.find(END)
            if start > 0:
                start = start + len(START)
                jsonText = scriptContent[start:end]
                parseJson(jsonText)

def parseJson(jsonText):
    jsonData = json.loads(jsonText)
    for item in jsonData:
        workshop = item["Workshop"]
        ficheId = workshop["uid"]
        ficheExport = scrutariDataExport.newFiche(ficheId)
        ficheExport.setTitre(workshop["name"])
        ficheExport.setHref(ROOT_URL + workshop["url"])
        ficheExport.setGeoloc(workshop["lat"], workshop["lng"])
        ficheExport.setLang("de")
        categories = workshop["categories"]
        for category in categories:
            if category in existingSet:
                scrutariDataExport.addIndexation("workshop", ficheId, "category", category, 1)
            
def extractCategory(soup):
    select = soup.find("select", id="workshopSearchCategories1")
    options = select.find_all("option")
    for option in options:
        value = option["value"]
        if len(value) > 0:
            existingSet[value] = True
            motcleExport = scrutariDataExport.newMotcle(value)
            motcleExport.setLibelle("de", option.get_text())
            

    
#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding="UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "offenewerkstaetten-geo.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding="UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

pageSoup = loadMapPage()

#thesaurus_category
thesaurusMetadataExport = scrutariDataExport.newThesaurus("category");
metadataWrapper.fillThesaurus(thesaurusMetadataExport)

extractCategory(pageSoup)

#corpus_workshop
corpusMetadataExport = scrutariDataExport.newCorpus("workshop")
metadataWrapper.fillCorpus(corpusMetadataExport)

extractWorkshops(pageSoup)
    
#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "offenewerkstaetten-geo.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "offenewerkstaetten-geo.scrutari-data.xml")
scrutariInfoFile.close()