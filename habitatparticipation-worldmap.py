#!/usr/bin/env python3

#-------------------------------------------------------------------
# http://habitat-worldmap.org
#-------------------------------------------------------------------


import sys, shutil, urllib.request, json, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *

#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
PER_PAGE="100"
POSTS_URL="https://habitat-worldmap.org/wp-json/wp/v2/posts?per_page=" + PER_PAGE + "&categories="


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def loadPosts(category, lang):
    postsReader = PostsReader(scrutariDataExport, lang)
    WordpressJson.readPosts(POSTS_URL + str(category), postsReader)


class PostsReader(object):
    
    def __init__(self, scrutariDataExport, lang):
        self.scrutariDataExport = scrutariDataExport
        self.lang = lang
        
    def handlePost(self, post):
        ficheId = post["id"]
        ficheExport = self.scrutariDataExport.newFiche(ficheId)
        ficheExport.setTitre(post["title"]["rendered"])
        ficheExport.setLang(self.lang)
        ficheExport.setHref(post["link"])
        soustitre = post["excerpt"]["rendered"]
        if soustitre:
            idx = soustitre.find('<a class="view-article')
            if idx > 0:
                soustitre = soustitre[0:idx]
            soup = BeautifulSoup(soustitre,"lxml")
            ficheExport.setSoustitre(soup.get_text())

#-------------------------------------------------------------------
# Utils
#-------------------------------------------------------------------

class WordpressJson(object):
    
    def __init__(self, handler):
        self.handler = handler
    
    @staticmethod
    def readPosts(pageUrl, postHandler):
        wj = WordpressJson(postHandler)
        jsonResponse = urllib.request.urlopen(pageUrl)
        totalPages = int(jsonResponse.getheader("X-WP-TotalPages"))
        wj.parsePosts(jsonResponse)
        if totalPages > 1:
            for i in range(2, totalPages + 1):
                jsonResponse = urllib.request.urlopen(pageUrl + "&page=" + str(i))
                wj.parsePosts(jsonResponse)
    
    def parsePosts(self, jsonResponse):
        jsonData = json.loads(jsonResponse.read().decode("UTF-8"))
        for post in jsonData:
            self.handler.handlePost(post)

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#init
#initMap(authorMap, AUTHORS_URL)
#initMap(tagMap, TAGS_URL)
#initLanguageMap(languageMap, LANGUAGES_URL)  


#wrapper
yamlFile = open(YAML_FILE, encoding="UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "habitatparticipation-worldmap.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_pays
#corpusMetadataExport = scrutariDataExport.newCorpus("pays");
#metadataWrapper.fillCorpus(corpusMetadataExport)

#loadPosts(41, "fr")

#corpus_thematique
corpusMetadataExport = scrutariDataExport.newCorpus("thematique");
metadataWrapper.fillCorpus(corpusMetadataExport)

loadPosts(41, "fr")

#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "habitatparticipation-worldmap.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "habitatparticipation-worldmap.scrutari-data.xml")
scrutariInfoFile.close()
