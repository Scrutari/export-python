# Scrutarisations basées sur l'API Wordpress

/wp-json/wp/v2/, /index.php?rest_route=/wp/v2/

Référence : energycities.py
Plus complexe : sciencescitoyennes.py 

- algeriawatch.py
- economiasolidaria.py
- energycities.py
- habitatparticipation-worldmap.py
- jeanlouislaville.py
- sciencescitoyennes.py
- semeoz-geo.py
- unadel.py
- unsse-hub.py

En cas d'erreur 403, il faut ajoute des en-têtes à la requête
- algeriawatch.py

# Scrutarisation de XML

- cler-ressources.py

# Cache de fichiers

- cler-ressources.py

# Reconnaissance des langues

scrutaridataexport_utils charge la bibliothèque langid, il faut ensuite utiliser la fonction utils_langidentify. Exemple : altramerica.py
