#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://sciencescitoyennes.org
#-------------------------------------------------------------------

from bs4 import BeautifulSoup
from scrutaridataexport import *
from scrutaridataexport_utils import *
from wordpressapi import *

#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
NAME = "sciencescitoyennes"
HOST = "sciencescitoyennes.org"
PER_PAGE = "100"
DOSSIERS_URL = "https://sciencescitoyennes.org/wp-json/wp/v2/dossier/?per_page=100"
PARAM_LIST = "tet"
POSTS_URL = "https://sciencescitoyennes.org/wp-json/wp/v2/posts?per_page=" + PER_PAGE + "&dossier="
SHORT_URL = "https://sciencescitoyennes.org/?p="
AUTHOR_URL = "https://sciencescitoyennes.org/wp-json/wp/v2/users/"
AUTHOR_DICT = dict()
EXCLUDE_SET = {515, 563}
AUTHOR_EXCLUDE = {118,4}


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def loadDossiers(dossierBuffer):
    jsonData = utils_toJson(utils_urlopen(DOSSIERS_URL, HOST))
    dossierParam = ""
    for item in jsonData:
        id = item["id"]
        if id not in EXCLUDE_SET:
            if len(dossierParam) > 0:
                dossierParam = dossierParam + ","
            dossierParam = dossierParam + str(id)
            dossierBuffer.checkMotcle(str(id), "fr", item["name"])
    return dossierParam

def readPostPage(jsonResponse):
  jsonData = utils_toJson(jsonResponse)
  lang = "fr"
  for item in jsonData:
        ficheId = item["id"]
        ficheExport = scrutariDataExport.newFiche(ficheId)
        ficheExport.setTitre(wp_getText(item, "title"))
        ficheExport.setHref(SHORT_URL + str(ficheId))
        ficheExport.setDate(item["modified"][0:10])
        ficheExport.setSoustitre(getSoustitre(item))
        lang="fr"
        for dossier in item["dossier"]:
            if dossier == 515:
                lang="en"
            elif dossier not in EXCLUDE_SET:
                scrutariDataExport.addIndexation("post", ficheId, "dossier", str(dossier), 1)
        ficheExport.setLang(lang)
        author = getAuthor(item)
        if author:
            ficheExport.addAttributeValue("sct", "authors", author)

def getSoustitre(item):
    content = item["content"]["rendered"]
    idx = content.find("<!--more-->")
    if idx > 0:
        content=content[0:idx]
    text = ""
    soup = BeautifulSoup(content,"lxml")
    for p in soup.find_all("p"):
        text = text + " " + p.get_text()
        if (len(text) > 150):
            break
    return text

def getAuthor(item):
    if not item["author"]:
        return ""
    if item["author"] in AUTHOR_EXCLUDE:
        return ""
    authorId = str(item["author"])
    if authorId in AUTHOR_DICT:
        return AUTHOR_DICT[authorId]
    else:
        jsonData = utils_toJson(utils_urlopen(AUTHOR_URL + authorId, HOST))
        name = jsonData["name"]
        AUTHOR_DICT[authorId] = name
        return name


#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#start
scrutariDataFile = utils_openScrutariDataFile(DESTINATION_DIRECTORY, NAME)
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))
metadataWrapper = utils_initMetadataWrapper(YAML_FILE)

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_post
corpusMetadataExport = scrutariDataExport.newCorpus("post");
metadataWrapper.fillCorpus(corpusMetadataExport)
dossierBuffer = ThesaurusBuffer()
dossierParam = loadDossiers(dossierBuffer)
wp_readPages(POSTS_URL + dossierParam, HOST, readPostPage)


#thesaurus_dossier
thesaurusMetadataExport = scrutariDataExport.newThesaurus("dossier");
metadataWrapper.fillThesaurus(thesaurusMetadataExport)
dossierBuffer.exportMotscles(scrutariDataExport)

#end
scrutariDataExport.endExport()
utils_closeScrutariDataFile(DESTINATION_DIRECTORY, NAME, scrutariDataFile)
utils_writeScrutariInfoFile(DESTINATION_DIRECTORY, NAME)
