#!/usr/bin/env python3
#-------------------------------------------------------------------
# Script de génération du fichier ScrutariData pour le site https://www.asso-sherpa.org
#-------------------------------------------------------------------

#-------------------------------------------------------------------
# Ce script utilise le module BeautifulSoup http://www.crummy.com/software/BeautifulSoup/
# ainsi que le module lxml (voir l'appel de BeautifulSoup(htmlContent, "lxml"))
# Les autres modules utilisés sont standards
# scrutaridataexport correspond au contenu du fichier scrutaridataexport.py
#-------------------------------------------------------------------

import sys, shutil, urllib.request
from bs4 import BeautifulSoup
from scrutaridataexport import *


#-------------------------------------------------------------------
# Ce script a un seul argument obligatoire : le chemin du répertoire de destination
# où sera enregistré les fichiers sherpa.scrutari-data.xml et sherpa.scrutari-info.xml
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
RACINE_SITE="https://www.asso-sherpa.org/"
ANGLAIS=["4152","4178"]

def checkCategory(url):
    htmlContent = urllib.request.urlopen(RACINE_SITE + url)
    soup = BeautifulSoup(htmlContent,"lxml")
    checkListePosts(soup)
    checkSuivant(soup)
    
def checkSuivant(soup):
    paginationDiv = soup.find("div", class_="pagination")
    if paginationDiv:
        links = paginationDiv.find_all("a")
        lastLink = links[len(links) - 1]
        if lastLink.get_text() == "Suivant":
            loadSuivant("category/" + lastLink["href"])

def loadSuivant(url):
    htmlContent = urllib.request.urlopen(RACINE_SITE + url)
    soup = BeautifulSoup(htmlContent,"lxml")
    checkListePosts(soup)
    checkSuivant(soup)

def checkListePosts(soup):
    posts = soup.find_all("div", class_="post")
    for post in posts:
        link = post.find("a", class_="button")
        if link:
            checkPost(link["href"])
            
def checkPost(url):
    htmlContent = urllib.request.urlopen(RACINE_SITE + url)
    soup = BeautifulSoup(htmlContent,"lxml")
    link = soup.find("link", rel="shortlink")
    href = link["href"]
    postId = href[3:]
    ficheExport = scrutariDataExport.newFiche(postId)
    h1 = soup.find("h1")
    ficheExport.setTitre(h1.get_text());
    ficheExport.setHref(RACINE_SITE + href)
    if postId in ANGLAIS:
        ficheExport.setLang("en")
    else:
        ficheExport.setLang("fr")
    ficheExport.setSoustitre(checkSoustitre(soup.find("meta", property="og:description")))
    date = soup.find("meta", property="article:published_time")
    if date:
        dateContent = date["content"]
        ficheExport.setDate(dateContent[0:10])
    
def checkSoustitre(description):
    if not description:
        return ""
    soustitre = description["content"]
    soustitreLen = len(soustitre)
    if soustitreLen < 150:
        return soustitre
    idx = soustitre.find(".", 150, soustitreLen)
    if idx > 0:
        return soustitre[0:(idx+1)]
    return soustitre

#-------------------------------------------------------------------
# Procédure principale
#-------------------------------------------------------------------

#Départ de l'exportation
scrutariDataPath = DESTINATION_DIRECTORY + "sherpa.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#Initialisation de la base de sherap
baseMetadataExport = scrutariDataExport.startExport()
baseMetadataExport.setAuthority("asso-sherpa.org")
baseMetadataExport.setBaseName("site")
baseMetadataExport.setBaseIcon("http://static.scrutari.net/sherpa.png")
baseMetadataExport.setIntitule(INTITULE_SHORT, "fr", "SHERPA")
baseMetadataExport.setIntitule(INTITULE_LONG, "fr", "SHERPA, Protection et défense des victimes de crimes économiques")
baseMetadataExport.addLangUI("fr")

#Création du corpus etude
corpusMetadataExport = scrutariDataExport.newCorpus("etude");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Recherche et études")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Étude n°")

#Récupération des études à partir de la page de la catégorie recherche-et-etudes 
checkCategory('category/recherche-et-etudes')

#Fin de l'export
scrutariDataExport.endExport()
scrutariDataFile.close()

#Renommage du fichier temporaire
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#Création du fichier d'information avec la date du jour
scrutariInfoFile = open(DESTINATION_DIRECTORY + "sherpa.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "sherpa.scrutari-data.xml")
scrutariInfoFile.close()
