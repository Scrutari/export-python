#!/usr/bin/env python3

#-------------------------------------------------------------------
# http://semeoz.info
#-------------------------------------------------------------------

import sys, shutil, urllib.request, json, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
API_URL = "http://semeoz.info/wp-json/wp/v2/posts"
HTML_URL = "http://semeoz.info/?geo_mashup_content=render-map&map_data_key=a4c300a723f15b6266d2e506ea7baa41&object_id=2297&map_content=global&name=gm-map-1"
START_STRING = "getElementById(\"geo-mashup\"),"
END_STRING = "}}}}}"


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def loadAPI():
    itemDict = dict()
    apiJsonResponse = urllib.request.urlopen(API_URL)
    apiJsonObj = json.loads(apiJsonResponse.read().decode("UTF-8"))
    itemDict = dict()
    for item in apiJsonObj:
        itemDict[str(item["id"])] = item
    return itemDict
        

def extractJson():
    itemDict = loadAPI()
    htmlResponse =  urllib.request.urlopen(HTML_URL)
    htmlBs = BeautifulSoup(htmlResponse.read(), "lxml")
    htmlContent = htmlBs.prettify()
    startIndex = htmlContent.find(START_STRING) + len(START_STRING)
    endIndex = htmlContent.find(END_STRING, startIndex) + len(END_STRING)
    jsonObj = json.loads(htmlContent[startIndex:endIndex])
    for obj in jsonObj["object_data"]["objects"]:
        ficheId = obj["object_id"]
        ficheExport = scrutariDataExport.newFiche(ficheId)
        #ficheExport.setDate(year)
        ficheExport.setLang("fr")
        ficheExport.setGeoloc(obj["lat"], obj["lng"])
        ficheExport.setHref("http://semeoz.info/?p=" + obj["object_id"])
        ficheExport.setTitre(obj["title"])
        if ficheId in itemDict:
            apiObj = itemDict[ficheId]
            if "excerpt" in apiObj:
                ficheExport.setSoustitre(apiObj["excerpt"])

    
#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding="UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "semeoz-geo.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#Création du corpus Post
corpusMetadataExport = scrutariDataExport.newCorpus("post");
metadataWrapper.fillCorpus(corpusMetadataExport)

extractJson()
   
#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "semeoz-geo.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "semeoz-geo.scrutari-data.xml")
scrutariInfoFile.close()
