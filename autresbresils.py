#!/usr/bin/env python3
#-------------------------------------------------------------------
# Script de génération du fichier ScrutariData pour le site http://www.autresbresils.net/
#
#-------------------------------------------------------------------

#-------------------------------------------------------------------
# Ce script utilise le module BeautifulSoup http://www.crummy.com/software/BeautifulSoup/
# ainsi que le module lxml (voir l'appel de BeautifulSoup(htmlContent, "lxml"))
# Les autres modules utilisés sont standards
# scrutaridataexport correspond au contenu du fichier scrutaridataexport.py
#-------------------------------------------------------------------

import sys, shutil, urllib.request
from bs4 import BeautifulSoup
from scrutaridataexport import *

#-------------------------------------------------------------------
# Ce script a un seul argument obligatoire : le chemin du répertoire de destination
# où sera enregistré les fichiers autresbresils.scrutari-data.xml et autresbresils.scrutari-info.xml
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
RACINE_SITE="http://www.autresbresils.net/"

def checkRubrique(url):
    htmlContent = urllib.request.urlopen(RACINE_SITE + url)
    soup = BeautifulSoup(htmlContent,"lxml")
    checkListeArticles(soup)
    iconForward = soup.find("i", class_="icon-fast-forward")
    last = int(iconForward.parent["title"])
    for i in range(1,last):
        paginationHtmlContent = urllib.request.urlopen(RACINE_SITE + url + "?debut_article_numerotes2=" + str(i * 10))
        paginationSoup = BeautifulSoup(paginationHtmlContent,"lxml")
        checkListeArticles(paginationSoup)
    
def checkListeArticles(soup):
    articles = soup.find_all("article", class_="article")
    for article in articles:
        h3 = article.find("strong", class_="h3-like")
        if h3:
            link = h3.find("a")
            href = link["href"]
            checkArticle(href)
        
def checkArticle(href):
    htmlContent = urllib.request.urlopen(RACINE_SITE + href)
    soup = BeautifulSoup(htmlContent,"lxml")
    content = soup.find("div", id="content")
    h1 = content.find("h1")
    h1Span = h1.find("span")
    ficheExport = scrutariDataExport.newFiche(getFicheId(h1Span["class"]))
    ficheExport.setLang("fr")
    ficheExport.setTitre(h1Span.get_text())
    ficheExport.setHref(RACINE_SITE + href)
    time = content.find("time")
    if time:
        ficheExport.setDate(time["datetime"][0:10])
    authors = content.select("span.vcard.author")
    for author in authors:
        ficheExport.addAttributeValue("sct","authors",author.get_text())
    chapoDiv = content.find("div", class_="chapo")
    if chapoDiv:
        chapoP = chapoDiv.find("p")
        ficheExport.setSoustitre(checkSoustitre(chapoP))

        
def getFicheId(classAttribute):
    for className in classAttribute:
        if className.startswith("article-titre-"):
            return className[14:]
    return ""

def checkSoustitre(pElement):
    if not pElement:
        return ""
    soustitre = pElement.get_text()
    soustitreLen = len(soustitre)
    if soustitreLen < 150:
        return soustitre
    idx = soustitre.find(".", 150, soustitreLen)
    if idx > 0:
        return soustitre[0:(idx+1)]
    return soustitre
    
    

#-------------------------------------------------------------------
# Procédure principale
#-------------------------------------------------------------------

#Départ de l'exportation
scrutariDataPath = DESTINATION_DIRECTORY + "autresbresils.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))


baseMetadataExport = scrutariDataExport.startExport()
baseMetadataExport.setAuthority("autresbresils.net")
baseMetadataExport.setBaseName("site")
baseMetadataExport.setBaseIcon("http://static.scrutari.net/autresbresils.png")
baseMetadataExport.setIntitule(INTITULE_SHORT, "fr", "Autres Brésils")
baseMetadataExport.setIntitule(INTITULE_LONG, "fr", "Autres Brésils - Un décryptage de la société brésilienne pour un public francophone")
baseMetadataExport.addLangUI("fr")


#Création du corpus des analyses
corpusMetadataExport = scrutariDataExport.newCorpus("analyse");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Analyses")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Analyse n° ")

#Récupération des pages avec la liste des articles par langue
checkRubrique("analyses")

#Création du corpus des chroniques
corpusMetadataExport = scrutariDataExport.newCorpus("chronique");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Chroniques")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Chronique n° ")

#Récupération des pages avec la liste des articles par langue
checkRubrique("chroniques")

#Création du corpus des entretiens
corpusMetadataExport = scrutariDataExport.newCorpus("entretien");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Entretiens")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Entretien n° ")

#Récupération des pages avec la liste des articles par langue
checkRubrique("entretiens")

#Création du corpus des reportages
corpusMetadataExport = scrutariDataExport.newCorpus("reportage");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Reportages")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Reportage n° ")

#Récupération des pages avec la liste des articles par langue
checkRubrique("reportages")

#Fin de l'export
scrutariDataExport.endExport()
scrutariDataFile.close()

#Renommage du fichier temporaire
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#Création du fichier d'information avec la date du jour
scrutariInfoFile = open(DESTINATION_DIRECTORY + "autresbresils.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "autresbresils.scrutari-data.xml")
scrutariInfoFile.close()