#!/usr/bin/env python3

#-------------------------------------------------------------------
#  https://gresea.be
#-------------------------------------------------------------------

import re
import ssl
from bs4 import BeautifulSoup
from scrutaridataexport import *
from scrutaridataexport_utils import *
from wordpressapi import *

#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
NAME = "gresea"
HOST = "gresea.be"
RACINE_SITE="https://gresea.be/"
DATE_RE = re.compile("([0-9]+)/([0-9]+)/([0-9]+)")
TOTAL_RE = re.compile("\\(([0-9]+)\\)")
MAX=100


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

#To solve [SSL: CERTIFICATE_VERIFY_FAILED] error
ssl._create_default_https_context = ssl._create_unverified_context

def checkMotcle(url):
    href = RACINE_SITE + url
    htmlContent = utils_urlopen(href, HOST)
    soup = BeautifulSoup(htmlContent,"lxml")
    checkListe(soup)
    h2 = soup.find("h2", class_="icon-list")
    total = 500
    if h2:
        total = int(TOTAL_RE.search(h2.get_text()).group(1))
    pagination = 50
    while(pagination < total):
        htmlContent = utils_urlopen(href + "?debut_articles=" + str(pagination), HOST)
        soup = BeautifulSoup(htmlContent,"lxml")
        checkListe(soup)
        pagination = pagination + 50
        
   
def checkListe(soup):
    ul = soup.find("ul", class_ = "liste-items")
    lis = ul.find_all("li")
    for li in lis:
       link = li.find("a")
       href = link["href"]
       checkArticle(href)

    
def checkArticle(href):
    url = RACINE_SITE + href
    htmlContent = utils_urlopen(url, HOST)
    soup = BeautifulSoup(htmlContent,"lxml")
    ficheId = getFicheId(soup)
    if ficheId == "":
        return
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setLang("fr")
    ficheExport.setHref(url)
    title = soup.find("title").get_text()
    idx = title.rfind("-")
    ficheExport.setTitre(title[0:idx])
    description = soup.find("meta", attrs={'name':'description'})
    if description:
        ficheExport.setSoustitre(description["content"])
    header = soup.find("header", class_="content-header")
    if header:
        details = header.find("p", class_="note")
        dateSearch = DATE_RE.search(details.get_text())
        if dateSearch:
            ficheExport.setDate(dateSearch.group(3) + "-" + dateSearch.group(2))
        authorList = details.find_all("span", class_="vcard")
        for author in authorList:
            ficheExport.addAttributeValue("sct","authors", author.get_text())

def getFicheId(soup):
    ul = soup.find("ul", class_="nav-social")
    if ul:
        link = ul.find("a", title="Imprimer")
        if link:
            href = link["href"]
            idx = href.rfind("=")
            return href[idx+1:]
    return ""

#def checkDate(detailText):
    #idx = detailText.find(",")
    #dateString = detailText[0:idx].strip()
    #idx2 = dateString.rfind(" ")
    #date = dateString[idx2+1:]
    #monthString = dateString[0:idx2].strip()
    #idx3 = monthString.rfind(" ")
    #month = checkMonth(monthString[idx3+1:])
    #if month:
        #date = date + "-" + month
    #return date

#def checkMonth(month):
    #if month == 'janvier':
        #return "01"
    #if month == 'février':
        #return "02"
    #if month == 'mars':
        #return "03"
    #if month == 'avril':
        #return "04"
    #if month == 'mai':
        #return "05"
    #if month == 'juin':
        #return "06"
    #if month == 'juillet':
        #return "07"
    #if month == 'août':
        #return "08"
    #if month == 'septembre':
        #return "09"
    #if month == 'octobre':
        #return "10"
    #if month == 'novembre':
        #return "11"
    #if month == 'décembre':
        #return "12"
    #return ""  


#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------


#start
scrutariDataFile = utils_openScrutariDataFile(DESTINATION_DIRECTORY, NAME)
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))
metadataWrapper = utils_initMetadataWrapper(YAML_FILE)

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_report
corpusMetadataExport = scrutariDataExport.newCorpus("analyse");
metadataWrapper.fillCorpus(corpusMetadataExport)


checkMotcle("+-Analyse-+")

#end
scrutariDataExport.endExport()
utils_closeScrutariDataFile(DESTINATION_DIRECTORY, NAME, scrutariDataFile)
utils_writeScrutariInfoFile(DESTINATION_DIRECTORY, NAME)
