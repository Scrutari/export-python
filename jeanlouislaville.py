#!/usr/bin/env python3

#-------------------------------------------------------------------
# http://www.jeanlouislaville.fr
#-------------------------------------------------------------------

import json
from scrutaridataexport import *
from scrutaridataexport_utils import *
from wordpressapi import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
NAME = "jeanlouislaville"
HOST = "jeanlouislaville.fr"
LANG_DICT = {
    22: "en",
    46: "es",
    45: "it",
    44: "pt"
}
EXCLUDE_SET = {1}
PER_PAGE = "100"
POSTS_URL = "https://www.jeanlouislaville.fr/wp-json/wp/v2/posts?per_page=" + PER_PAGE
SHORT_URL = "https://www.jeanlouislaville.fr/?p="


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def readPostPage(jsonResponse):
    jsonData = json.loads(jsonResponse.read().decode("UTF-8"))
    for item in jsonData:
        categoryArray = item["categories"]
        check = checkCategory(categoryArray)
        if not check:
            continue
        ficheId = item["id"]
        ficheExport = scrutariDataExport.newFiche(ficheId)
        ficheExport.setTitre(wp_getText(item, "title"))
        ficheExport.setLang(getLang(categoryArray))
        ficheExport.setHref(SHORT_URL + str(ficheId))
        ficheExport.setDate(wp_getDate(item))
        ficheExport.setSoustitre(wp_getText(item, "excerpt"))
        ficheExport.addAttributeValue("sct", "authors", "Jean-Louis Laville")
       
        
def getLang(categoryArray):
    for categoryId in categoryArray:
        if categoryId in LANG_DICT:
            return LANG_DICT[categoryId]
    return "fr"

def checkCategory(categoryArray):
    for categoryId in categoryArray:
        if categoryId not in EXCLUDE_SET:
            return True
    return False   
        

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#start
scrutariDataFile = utils_openScrutariDataFile(DESTINATION_DIRECTORY, NAME)
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))
metadataWrapper = utils_initMetadataWrapper(YAML_FILE)

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_article
corpusMetadataExport = scrutariDataExport.newCorpus("publication");
metadataWrapper.fillCorpus(corpusMetadataExport)
wp_readPages(POSTS_URL, HOST, readPostPage)

#end
scrutariDataExport.endExport()
utils_closeScrutariDataFile(DESTINATION_DIRECTORY, NAME, scrutariDataFile)
utils_writeScrutariInfoFile(DESTINATION_DIRECTORY, NAME)
