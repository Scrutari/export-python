#!/usr/bin/env python3

#-------------------------------------------------------------------
# http://www.economie-tunisie.org/
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
INDEX_URL = "http://www.economie-tunisie.org/fr/observatoire/"
ROOT_URL = "http://www.economie-tunisie.org"


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def loadList(url):
    htmlContent = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlContent,"lxml")
    checkArticles(soup)
    checkNext(soup)
    
def checkNext(soup):
    nextLi = soup.find("li", class_="pager-next")
    if nextLi:
        link = nextLi.find("a")
        if link:
            loadList(ROOT_URL + link["href"])
            
def checkArticles(soup):
    h2s = soup.find_all("h2", class_="titre_liste_article")
    for h2 in h2s:
        link = h2.find("a")
        if link:
            loadArticle(ROOT_URL + link["href"])
            
def loadArticle(url):
    htmlContent = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlContent,"lxml")
    link = soup.find("link", rel="shortlink")
    href = link["href"]
    ficheId = getId(href)
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setLang("fr")
    ficheExport.setHref(href)
    titleMeta = soup.find("meta", attrs={'name':"dcterms.title"})
    if titleMeta:
        ficheExport.setTitre(titleMeta["content"])
    descriptionMeta = soup.find("meta", attrs={'name':"description"})
    if descriptionMeta:
        ficheExport.setSoustitre(descriptionMeta["content"])
    dateMeta = soup.find("meta", attrs={'name':"dcterms.date"})
    if dateMeta:
        ficheExport.setDate(dateMeta["content"][0:10])
    authors = soup.find_all("a", property="foaf:name")
    for author in authors:
        ficheExport.addAttributeValue("sct", "authors", author.get_text())
                             
 
def getId(shortlink):
    idx = shortlink.rfind("/")
    return shortlink[idx+1:]
    


#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding = "UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "economietunisie.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding="UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_analyse
corpusMetadataExport = scrutariDataExport.newCorpus("analyse");
metadataWrapper.fillCorpus(corpusMetadataExport)

loadList(INDEX_URL + "analysiseconomics")

#corpus_information
corpusMetadataExport = scrutariDataExport.newCorpus("information");
metadataWrapper.fillCorpus(corpusMetadataExport)

loadList(INDEX_URL + "infoeconomics")

#corpus_infographie
corpusMetadataExport = scrutariDataExport.newCorpus("infographie");
metadataWrapper.fillCorpus(corpusMetadataExport)

loadList(INDEX_URL + "visualeconomics")

#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "economietunisie.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "economietunisie.scrutari-data.xml")
scrutariInfoFile.close()
