#!/usr/bin/env python3

#-------------------------------------------------------------------
# http://mappa.italiachecambia.org
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re, yaml
from bs4 import BeautifulSoup
from lxml import etree
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
XML_URL = "http://api.checambia.org/spoi/xml/italiachecambia-socioeco.xml"
ROOT_URL = "http://mappa.italiachecambia.org/scheda/"


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def checkXml():
    tree = etree.parse(XML_URL)
    pois = tree.xpath("poi")
    for poi in pois:
        name = poi.xpath("name")[0].text
        ficheId = poi.xpath("oid")[0].text
        ficheExport = scrutariDataExport.newFiche(ficheId)
        ficheExport.setLang("it")
        ficheExport.setTitre(name)
        slug = poi.xpath("slug")[0].text
        slug = urllib.parse.quote(slug)
        ficheExport.setHref(ROOT_URL + slug + "/")
        lat = poi.xpath("lat")[0].text
        lon = poi.xpath("long")[0].text
        ficheExport.setGeoloc(lat, lon)
        description = poi.xpath("description")[0]
        descriptionSoup = BeautifulSoup(description.text,"lxml")
        ficheExport.setSoustitre(descriptionSoup.get_text())
        tags = poi.xpath("tags/tag")
        for tag in tags:
            ficheExport.addAttributeValue("sct", "tags", tag.text)
        cities = poi.xpath("address/city")
        for city in cities:
            ficheExport.addAttributeValue("geo", "city", city.text)
     

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding = "UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "checambia-geo.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding="UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_scheda
corpusMetadataExport = scrutariDataExport.newCorpus("scheda");
metadataWrapper.fillCorpus(corpusMetadataExport)

checkXml()

#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "checambia-geo.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "checambia-geo.scrutari-data.xml")
scrutariInfoFile.close()
