#!/usr/bin/env python3

#-------------------------------------------------------------------
# http://bed.bzh/
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
INDEX_URL = "http://bed.bzh/fr/index-films/"
ROOT_URL = "http://bed.bzh/"


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def checkIndex():
    htmlContent = urllib.request.urlopen(INDEX_URL)
    soup = BeautifulSoup(htmlContent,"lxml")
    globalDiv = soup.find("div", class_="globalIndex")
    trs = globalDiv.find_all("tr")
    for tr in trs:
        td = tr.find("td")
        if td:
            link = td.find("a")
            titre = link.get_text()
            href = link["href"]
            id = href[9:len(href) -1]
            checkPage(id, href, titre)

def checkPage(ficheId, href, titre):
    url = ROOT_URL + href
    htmlContent = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlContent,"lxml")
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setLang("fr")
    ficheExport.setTitre(titre)
    ficheExport.setHref(url)
    h2 = soup.find("h2", class_="subtitle")
    if h2:
        span = h2.find("span", class_="nolink")
        if span:
            ficheExport.addAttributeValue("sct", "authors", span.get_text())
    h1 = soup.find("h1", class_="title")
    if h1:
        dateSpan = h1.find("span", class_="date")
        if dateSpan:
            date = dateSpan.get_text()
            date = date[1:len(date) - 1]
            ficheExport.setDate(date)
    resume = soup.find("div", class_="resume")
    if resume:
        p = resume.find("p")
        if p:
            ficheExport.setSoustitre(p.get_text())

    


#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding = "UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "bretagneetdiversite-films.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding="UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_scheda
corpusMetadataExport = scrutariDataExport.newCorpus("film");
metadataWrapper.fillCorpus(corpusMetadataExport)

checkIndex()

#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "bretagneetdiversite-films.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "bretagneetdiversite-films.scrutari-data.xml")
scrutariInfoFile.close()
