#!/usr/bin/env python3

#-------------------------------------------------------------------
# http://http://www.laboress-afrique.org
#-------------------------------------------------------------------

import re
from bs4 import BeautifulSoup
from scrutaridataexport import *
from scrutaridataexport_utils import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
NAME = "laboressafrique"
HOST = "www.laboress-afrique.org"
RACINE_SITE = "http://www.laboress-afrique.org/"


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def checkCategory(category):
    rootUrl = RACINE_SITE + "ressources/index.php/Ct_gestionPages/categories/" + str(category) + "/"
    page = 1
    while(True):
        url = rootUrl + str(page) + ".html"
        htmlContent = utils_urlopen(url, HOST)
        soup = BeautifulSoup(htmlContent,"lxml")
        divList = soup.select("div.contenaire div.contenu")
        if len(divList) > 0:
            for div in divList:
                link = div.find("a")
                checkArticle(link["href"])
            page = page + 1
        else:
            break
        
def checkArticle(href):
    last = href.rfind('/')
    ficheId = href[last+1:len(href) - 5]
    htmlContent = utils_urlopen(href, HOST)
    soup = BeautifulSoup(htmlContent,"lxml")
    telechargement = soup.find("a", id="telechargement")
    if not telechargement:
        return
    pdf = telechargement["href"]
    if pdf.find("//www.laboress-afrique.org/") == -1:
        return
    if not pdf.endswith(".pdf"):
        return
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setLang("fr")
    ficheExport.setHref(href)
    h3 = soup.find("h3", class_="panel-title")
    if h3:
        br = h3.find("br")
        if br:
            ficheExport.setTitre(br.next_sibling)
        else:
            ficheExport.setTitre(h3.get_text)
    label_auteur = soup.find(text=re.compile("le\(s\) auteur\(s\)"))
    if label_auteur:
        td_auteurs = label_auteur.parent.find_next_sibling("td")
        if td_auteurs:
            auteurs = td_auteurs.get_text().split(',')
            for auteur in auteurs:
                ficheExport.addAttributeValue("sct","authors", auteur)
    label_resume = soup.find(text=re.compile("Resumé du document"))
    if label_resume:
        td_resume = label_resume.parent.find_next_sibling("td")
        if td_resume:
            resume = td_resume.get_text()
            ficheExport.setSoustitre(checkSoustitre(resume))

def checkSoustitre(soustitre):
    idx = soustitre.find("\n")
    if idx > 0:
        soustitre = soustitre[0:idx]
    soustitreLen = len(soustitre)
    if soustitreLen < 150:
        return soustitre
    idx = soustitre.find(".", 150, soustitreLen)
    if idx > 0:
        return soustitre[0:(idx+1)]
    return soustitre
    

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#start
scrutariDataFile = utils_openScrutariDataFile(DESTINATION_DIRECTORY, NAME)
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))
metadataWrapper = utils_initMetadataWrapper(YAML_FILE)

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_these
corpusMetadataExport = scrutariDataExport.newCorpus("these");
metadataWrapper.fillCorpus(corpusMetadataExport)
checkCategory(10)

#corpus_ouvrage
corpusMetadataExport = scrutariDataExport.newCorpus("ouvrage");
metadataWrapper.fillCorpus(corpusMetadataExport)
checkCategory(6)

#corpus_rapport
corpusMetadataExport = scrutariDataExport.newCorpus("rapport");
metadataWrapper.fillCorpus(corpusMetadataExport)
checkCategory(2)

#corpus_article
corpusMetadataExport = scrutariDataExport.newCorpus("article");
metadataWrapper.fillCorpus(corpusMetadataExport)
checkCategory(5)

#corpus_divers
corpusMetadataExport = scrutariDataExport.newCorpus("divers");
metadataWrapper.fillCorpus(corpusMetadataExport)
checkCategory(9)

#end
scrutariDataExport.endExport()
utils_closeScrutariDataFile(DESTINATION_DIRECTORY, NAME, scrutariDataFile)
utils_writeScrutariInfoFile(DESTINATION_DIRECTORY, NAME)
