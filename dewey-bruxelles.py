#!/usr/bin/env python3

import sys, shutil, urllib.request, json
from scrutaridataexport import *
import re
import unicodedata

#-------------------------------------------------------------------
# Ce script a un seul argument obligatoire : le chemin du répertoire de destination
# où sera enregistré les fichiers semeoz-geo.scrutari-data.xml et semeoz-geo.scrutari-info.xml
#-------------------------------------------------------------------
DESTINATION_DIRECTORY = sys.argv[1]
JSON_URL = "http://maps.dewey.be/api/markers/"
EXCLUDE_TUPLES = (100,173,129,91,111,98,110,139,117,96,115,170,124)


def slugify(value):
    """
    Convert to ASCII if 'allow_unicode' is False. Convert spaces to hyphens.
    Remove characters that aren't alphanumerics, underscores, or hyphens.
    Convert to lowercase. Also strip leading and trailing whitespace.
    """
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub('[^\w\s-]', '', value).strip().lower()
    return re.sub('[-\s]+', '-', value)

def categories(array):
    complement = ""
    for category in array:
        if not category["id"] in EXCLUDE_TUPLES:
            if len(complement) > 0:
                complement += ", "
            complement += category["name"]
    return complement


#-------------------------------------------------------------------
# Procédure principale
#-------------------------------------------------------------------

#Départ de l'exportation
scrutariDataPath = DESTINATION_DIRECTORY + "dewey-bruxelles.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#Initialisation de la base de Dewey - Bruxelles
baseMetadataExport = scrutariDataExport.startExport()
baseMetadataExport.setAuthority("dewey.be")
baseMetadataExport.setBaseName("bruxelles")
baseMetadataExport.setBaseIcon("http://static.scrutari.net/dewey-bruxelles.png")
baseMetadataExport.setIntitule(INTITULE_SHORT, "fr", "Bruxelles mode d’emploi")
baseMetadataExport.setIntitule(INTITULE_LONG, "fr", "Bruxelles mode d’emploi")
baseMetadataExport.addLangUI("fr")

#Création du corpus Post
corpusMetadataExport = scrutariDataExport.newCorpus("marker");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Points")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Point n°")
numero = corpusMetadataExport.addComplement()
corpusMetadataExport.setComplementIntitule(numero, "fr", "Catégories")

#Récupération du JSON
jsonResponse = urllib.request.urlopen(JSON_URL)
jsonObj = json.loads(jsonResponse.read().decode("UTF-8"))



for obj in jsonObj:
    complement = categories(obj["subcategories"])
    if len(complement) > 0:
        name = obj["name"]
        ficheExport = scrutariDataExport.newFiche(slugify(name))
        ficheExport.setTitre(name)
        ficheExport.setGeoloc(str(obj["lat"]), str(obj["lon"]))
        ficheExport.setHref("http://maps.dewey.be")
        ficheExport.addComplement(numero, complement)


#Fin de l'export
scrutariDataExport.endExport()
scrutariDataFile.close()

#Renommage du fichier temporaire
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#Création du fichier d'information avec la date du jour
scrutariInfoFile = open(DESTINATION_DIRECTORY + "dewey-bruxelles.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "dewey-bruxelles.scrutari-data.xml")
scrutariInfoFile.close()