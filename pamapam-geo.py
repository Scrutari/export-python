#!/usr/bin/env python3

#-------------------------------------------------------------------
# http://pamapam.org
#-------------------------------------------------------------------

import sys, shutil, urllib.request, json, yaml, csv
from bs4 import BeautifulSoup
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
PAMAPAM_FILE = sys.argv[2]
YAML_FILE = sys.argv[3]
THESAURUS_CSV_FILE = sys.argv[4]
ROOT_URL = "http://pamapam.org/ca/directori-de-punts/directori-article/"


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

                    
def scanJson(jsonData):
    for feature in jsonData["response"]:
        addFeature(feature)

def addFeature(feature):
    properties = feature["properties"]
    coordinates = feature["geometry"]["coordinates"]
    ficheId = str(properties["id"])
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setLang("ca")
    ficheExport.setGeoloc(coordinates[1], coordinates[0])
    ficheExport.setTitre(properties["name"])
    ficheExport.setSoustitre(properties["description"])
    ficheExport.setHref(ROOT_URL + ficheId)
    if "sectorMapIconUrl" in properties:
        url = properties["sectorMapIconUrl"]
        motcleId = url[len(url) -2:]
        scrutariDataExport.addIndexation("iniciativa", ficheId, "sector", motcleId, 1)
    if "town" in properties:
        ficheExport.addAttributeValue("geo", "city", properties["town"])
    

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding="UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#buffer
thesaurusBuffers = ThesaurusBuffers()
csvFile = open(THESAURUS_CSV_FILE, "r", encoding="UTF-8", newline='')
csvreader = csv.reader(csvFile, delimiter=',')
thesaurusBuffers.readCsv(csvreader)
csvFile.close()

#start
scrutariDataPath = DESTINATION_DIRECTORY + "pamapam-geo.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_iniciativa
corpusMetadataExport = scrutariDataExport.newCorpus("iniciativa");
metadataWrapper.fillCorpus(corpusMetadataExport)

#loadIndex()
with open(PAMAPAM_FILE, encoding="UTF-8") as data_file:    
    jsonData = json.load(data_file)
    scanJson(jsonData)

#thesaurus_sector
thesaurusMetadataExport = scrutariDataExport.newThesaurus("sector");
metadataWrapper.fillThesaurus(thesaurusMetadataExport)
thesaurusBuffers.fill("sector", scrutariDataExport)

#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "pamapam-geo.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "pamapam-geo.scrutari-data.xml")
scrutariInfoFile.close()