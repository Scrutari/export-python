#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://www.uv.es/uvweb/institut-universitari-economia-social-cooperativa-IUDESCOOP/ca/cidec/biblioteca-virtual-1286109782751.html
#-------------------------------------------------------------------

import re
from bs4 import BeautifulSoup
from scrutaridataexport_utils import *
from scrutaridataexport import *

#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
NAME = "cidec"
HOST = "www.uv.es"
START_URL="https://www.uv.es/sites/Satellite?pagename=IUI_Economia_Social_Cooperativa%2FPage%2FTPGListat&cid=1286109782751&locale=ca_ES&site=IUI_Economia_Social_Cooperativa&p0=true&p1=Publicacio&p2=0&p4=&p5=&p6=&p9=&p10=&p8="

#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def start(tipo):
    nextLink = START_URL + tipo
    while True:
        nextLink = loadPage(nextLink)
        if not nextLink:
            break
    
def loadPage(url):
    htmlContent = utils_urlopen(url, HOST)
    soup = BeautifulSoup(htmlContent,"lxml")
    ul = soup.find("ul", class_="publicacioNoArena")
    if not ul:
        return ""
    lis = ul.find_all("li", class_="publicacio", recursive=False)
    for li in lis:
        addFiche(li)
    nextSpan = soup.find("span", class_="p-seguent")
    if nextSpan:
        nextLink = nextSpan.find("a")
        if nextLink:
            return nextLink["href"]
    return ""
            
def addFiche(li):
    infoDiv = li.find("div", class_="publicacioInfo")
    if not infoDiv:
        return
    detallaDiv = li.find("div", class_="infoDetallada")
    if not detallaDiv:
        return
    detallaLink = detallaDiv.find("a")
    if not detallaLink:
        return
    href = detallaLink["href"]
    m = re.search("id=([0-9]+)", href)
    if not m:
        return
    ficheId = m.group(1)
    h3 = li.find("h3")
    if not h3:
        return
    titre = h3.get_text()
    idx = titre.find(" / ")
    if idx > 0:
        titre = titre[0:idx]
    lang = utils_langidentify(titre)
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setHref(href)
    ficheExport.setTitre(titre)
    ficheExport.setLang(lang)
    year = re.search("([0-9]{4})\.", infoDiv.get_text())
    if year:
        ficheExport.setDate(year.group(1))
    authors = h3.next_sibling
    if authors and hasattr(authors, 'name') and authors.name == "p":
        ficheExport.addAttributeValue("sct","authors", authors.get_text())
    


#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#start
scrutariDataFile = utils_openScrutariDataFile(DESTINATION_DIRECTORY, NAME)
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))
metadataWrapper = utils_initMetadataWrapper(YAML_FILE)

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)


#corpus_article
corpusMetadataExport = scrutariDataExport.newCorpus("article");
metadataWrapper.fillCorpus(corpusMetadataExport)
start("Article")

#corpus_informes
corpusMetadataExport = scrutariDataExport.newCorpus("informes");
metadataWrapper.fillCorpus(corpusMetadataExport)
start("Informes")

#corpus_llibre
corpusMetadataExport = scrutariDataExport.newCorpus("llibre");
metadataWrapper.fillCorpus(corpusMetadataExport)
start("Llibre")

#corpus_tesi
corpusMetadataExport = scrutariDataExport.newCorpus("tesi");
metadataWrapper.fillCorpus(corpusMetadataExport)
start("Tesi")

#end
scrutariDataExport.endExport()
utils_closeScrutariDataFile(DESTINATION_DIRECTORY, NAME, scrutariDataFile)
utils_writeScrutariInfoFile(DESTINATION_DIRECTORY, NAME)
