#!/usr/bin/env python3
#-------------------------------------------------------------------
# Script de génération du fichier ScrutariData pour le site http://coraggioeconomia.org/ 
# Ce script lit une seule page, celle contenant la liste des publications.
#-------------------------------------------------------------------

#-------------------------------------------------------------------
# Ce script utilise le module BeautifulSoup http://www.crummy.com/software/BeautifulSoup/
# ainsi que le module lxml (voir l'appel de BeautifulSoup(htmlContent, "lxml"))
# Les autres modules utilisés sont standards
# scrutaridataexport correspond au contenu du fichier scrutaridataexport.py
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re
from bs4 import BeautifulSoup
from scrutaridataexport import *


#-------------------------------------------------------------------
# Ce script a un seul argument obligatoire : le chemin du répertoire de destination
# où sera enregistré les fichiers ijcrr.scrutari-data.xml et ijcrr.scrutari-info.xml
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
HREF="http://coraggioeconomia.org/jlc_public_complet.htm"

def checkList(htmlContent):
    soup = BeautifulSoup(htmlContent, "lxml")
    table = soup.find("table", class_="NormalText")
    rows = table.find_all("tr", recursive=False)
    for row in rows:
        cells = row.find_all("td")
        if len(cells) < 2:
            continue
        datespan = cells[0].find("strong")
        if not datespan:
            datespan = cells[0].find("b")
        if not datespan:
            continue
        date = datespan.get_text().strip()
        link = cells[1].find("a")
        if not link:
            continue
        href = link["href"]
        if href.startswith("http"):
            continue
        ficheExport = scrutariDataExport.newFiche(toId(href))
        ficheExport.setLang("es")
        ficheExport.setHref("http://coraggioeconomia.org/" + href)
        ficheExport.setDate(date)
        ficheExport.setTitre(link.get_text().strip(" \"”“"))
        ficheExport.addAttributeValue("sct", "authors", "José Luis Coraggio")
        soustitre = getSoustitre(link)
        if soustitre:
            ficheExport.setSoustitre(soustitre)

def getSoustitre(link):
    result = ""
    for sibling in link.next_siblings:
        if sibling:
            if hasattr(sibling, "get_text"):
                result += sibling.get_text()
            else:
                result += sibling
    parent = link.parent
    if parent.name != "td":
        for sibling in parent.next_siblings:
            if sibling:
                if hasattr(sibling, "get_text"):
                    result += sibling.get_text()
                else:
                    result += sibling
    result = result.strip()
    result = result.lstrip(",. \"”")
    if len(result) == 0:
        return None
    return result
   
def toId(href):
    index = href.rfind("/")
    name = href[index+1:].lower()
    name = name.replace("%20", " ")
    name = re.sub(r"\s+[-]*", "-", name)
    return name
        

#-------------------------------------------------------------------
# Procédure principale
#-------------------------------------------------------------------

#Départ de l'exportation
scrutariDataPath = DESTINATION_DIRECTORY + "coraggioeconomia.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#Initialisation de la base de Jean-Louis Laville
baseMetadataExport = scrutariDataExport.startExport()
baseMetadataExport.setAuthority("9aac8aa2-f9da-4a11-914a-85b914b56be2")
baseMetadataExport.setBaseName("coraggioeconomia")
baseMetadataExport.setBaseIcon("http://static.scrutari.net/coraggioeconomia.png")
baseMetadataExport.setIntitule(INTITULE_SHORT, "es", "José Luis Coraggio")
baseMetadataExport.setIntitule(INTITULE_LONG, "es", "José Luis Coraggio, publicaciones")
baseMetadataExport.addLangUI("es")


#Création du corpus des publications
corpusMetadataExport = scrutariDataExport.newCorpus("publicacion");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "es","Publicaciones")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "es", "Publicacion #")


#Récupération de la liste des publications
htmlContent = urllib.request.urlopen(HREF)
checkList(htmlContent)

#Fin de l'export
scrutariDataExport.endExport()
scrutariDataFile.close()

#Renommage du fichier temporaire
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#Création du fichier d'information avec la date du jour
scrutariInfoFile = open(DESTINATION_DIRECTORY + "coraggioeconomia.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "coraggioeconomia.scrutari-data.xml")
scrutariInfoFile.close()
