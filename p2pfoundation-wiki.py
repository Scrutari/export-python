#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://wiki.p2pfoundation.net
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
CATEGORY_URL = "https://wiki.p2pfoundation.net/Category:"
ROOT_URL = "https://wiki.p2pfoundation.net"
IGNORE_LIST = ["/French_language", "/Qui_Sommes-Nous"]


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def checkCategory(category, lang):
    #htmlContent = urllib.request.urlopen(CATEGORY_URL + category)
    #soup = BeautifulSoup(htmlContent,"lxml")
    with open(DESTINATION_DIRECTORY + category + ".html") as data_file:    
        soup = BeautifulSoup(data_file,"lxml")
    pagesDiv = soup.find("div", id="mw-pages")
    groups = pagesDiv.find_all("div", class_="mw-category-group")
    for group in groups:
        links = group.find_all("a")
        for link in links:
            checkPage(link, lang)

def checkPage(link,lang):
    href = link["href"]
    if href not in IGNORE_LIST:
        ficheId = href[1:]
        ficheId = ficheId.replace("%", "-")
        ficheId = ficheId.replace("(", "")
        ficheId = ficheId.replace(")", "")
        ficheId = ficheId.replace(".", "_")
        ficheId = ficheId.replace("/", "_")
        ficheExport = scrutariDataExport.newFiche(ficheId)
        ficheExport.setLang(lang)
        ficheExport.setTitre(link["title"])
        ficheExport.setHref(ROOT_URL + href)



#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

tagBuffer = ThesaurusBuffer("tag")


#wrapper
yamlFile = open(YAML_FILE, encoding = "UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "p2pfoundation-wiki.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding="UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_page
corpusMetadataExport = scrutariDataExport.newCorpus("page");
metadataWrapper.fillCorpus(corpusMetadataExport)

checkCategory("French", "fr")



#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "p2pfoundation-wiki.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "p2pfoundation-wiki.scrutari-data.xml")
scrutariInfoFile.close()
