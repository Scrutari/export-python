#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://www.atemis-lir.fr
#-------------------------------------------------------------------

import sys, shutil, urllib.request, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
URL="https://www.atemis-lir.fr/publications/"
LANG="fr"
existingSet={}

def checkPage(url):
    htmlContent = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlContent,"lxml")
    for div in soup.find_all("div", class_="zp-List"):
        for entry in div.find_all("div", class_="zp-Entry"):
            ficheId = getFicheId(entry)
            if ficheId:
                if not ficheId in existingSet:
                    existingSet[ficheId] = True
                    addEntry(ficheId, entry)
            

def getFicheId(entry):
    wholeId = entry["id"]
    if wholeId:
        lastIndex = wholeId.rfind("-")
        ficheId = wholeId[lastIndex+1:]
        return ficheId
    return False

def addEntry(ficheId, entry):
    div = entry.find("div", "csl-entry")
    link = div.find("a")
    if not link:
        return
    text = div.get_text()
    retrieveIndex = text.find("Retrieved from")
    text = text[0:retrieveIndex]
    dateParenthesis = text.find(").")
    firstPart = text[0:dateParenthesis]
    secondPart = text[dateParenthesis + 2:]
    dateParenthesis2 = firstPart.rfind("(")
    annee = firstPart[dateParenthesis2+1:]
    if not is_integer(annee):
        return
    firstPunct = secondPart.find(".")
    titre = secondPart[0:firstPunct]
    soustitre = firstPart[0:dateParenthesis2] + " " + secondPart[firstPunct + 1:]
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setTitre(titre)
    ficheExport.setLang(LANG)
    ficheExport.setHref(URL + "#zp-ID--1048896-" + ficheId)
    ficheExport.setDate(annee)
    ficheExport.setSoustitre(soustitre)
    


def is_integer(n):
    try:
        float(n)
    except ValueError:
        return False
    else:
        return float(n).is_integer()
        
    
    
#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------


#wrapper
yamlFile = open(YAML_FILE, encoding="UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "atemis.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_post
corpusMetadataExport = scrutariDataExport.newCorpus("publication");
metadataWrapper.fillCorpus(corpusMetadataExport)

checkPage(URL)

#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "atemis.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "atemis.scrutari-data.xml")
scrutariInfoFile.close()
