#!/usr/bin/env python3
#-------------------------------------------------------------------
# Script de génération du fichier ScrutariData pour le site http://multinationales.org
#
#-------------------------------------------------------------------

#-------------------------------------------------------------------
# Ce script utilise le module BeautifulSoup http://www.crummy.com/software/BeautifulSoup/
# ainsi que le module lxml (voir l'appel de BeautifulSoup(htmlContent, "lxml"))
# Les autres modules utilisés sont standards
# scrutaridataexport correspond au contenu du fichier scrutaridataexport.py
#-------------------------------------------------------------------

import sys, shutil, urllib.request
from bs4 import BeautifulSoup
from scrutaridataexport import *

#-------------------------------------------------------------------
# Ce script a un seul argument obligatoire : le chemin du répertoire de destination
# où sera enregistré les fichiers multinationales.scrutari-data.xml et multinationales.scrutari-info.xml
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
RACINE_SITE="http://multinationales.org/"

def checkEnquetes(url, lang):
    htmlContent = urllib.request.urlopen(RACINE_SITE + url)
    soup = BeautifulSoup(htmlContent,"lxml")
    pagination = soup.find("p", class_="pagination")
    links = pagination.find_all("a")
    lastLink = links[len(links) - 1]
    lastNumber = int(lastLink.get_text())
    checkListeArticles(soup, lang)
    for number in range(2,lastNumber):
        start = ((number - 1) * 5)
        pageHtmlContent = urllib.request.urlopen(RACINE_SITE + url + "?debut_articles=" + str(start) +"#pagination_articles")
        checkListeArticles(BeautifulSoup(pageHtmlContent,"lxml"), lang)
            
def checkListeArticles(soup, lang):
    anchor = soup.find("a", id="pagination_articles")
    links = anchor.parent.select("h3 > a")
    for link in links:
        articleHtmlContent = urllib.request.urlopen(RACINE_SITE + link["href"])
        articleSoup = BeautifulSoup(articleHtmlContent,"lxml")
        header = articleSoup.find("header", class_="cartouche")
        if not header:
            return
        titreH1 = header.find("h1")
        if not titreH1:
            return
        ficheId = ""
        for className in titreH1["class"]:
            if className.startswith("article-titre-"):
                ficheId = className[14:]
        if len(ficheId) == 0:
            return
        ficheExport = scrutariDataExport.newFiche(ficheId)
        ficheExport.setTitre(titreH1.get_text())
        ficheExport.setLang(lang)
        ficheExport.setHref(RACINE_SITE + "spip.php?article" + ficheId)
        time = header.find("time", pubdate="pubdate")
        if time:
            ficheExport.setDate(time["datetime"][0:10])
        authors = header.select("span.vcard.author")
        for author in authors:
            ficheExport.addAttributeValue("sct","authors",author.get_text())
        mainDiv = articleSoup.find("div", class_="main")
        soustitre = checkSoustitre(mainDiv.find("div", class_="chapo"))
        ficheExport.setSoustitre(soustitre)

def checkSoustitre(element):
    if not element:
        return ""
    soustitre = element.get_text()
    idx = soustitre.find("\n")
    if idx > 0:
        soustitre = soustitre[0:idx]
    soustitreLen = len(soustitre)
    if soustitreLen < 150:
        return soustitre
    idx = soustitre.find(".", 150, soustitreLen)
    if idx > 0:
        return soustitre[0:(idx+1)]
    return soustitre

    
    

#-------------------------------------------------------------------
# Procédure principale
#-------------------------------------------------------------------

#Départ de l'exportation
scrutariDataPath = DESTINATION_DIRECTORY + "multinationales.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))


baseMetadataExport = scrutariDataExport.startExport()
baseMetadataExport.setAuthority("multinationales.org")
baseMetadataExport.setBaseName("site")
baseMetadataExport.setBaseIcon("http://static.scrutari.net/multinationales.png")
baseMetadataExport.setIntitule(INTITULE_SHORT, "fr", "Observatoire des multinationales")
baseMetadataExport.setIntitule(INTITULE_SHORT, "en", "Multinationals Observatory")
baseMetadataExport.setIntitule(INTITULE_LONG, "fr", "Observatoire des multinationales, publié par l'association Alter-médias")
baseMetadataExport.setIntitule(INTITULE_LONG, "en", "Multinationals Observatory, tracking french corporations worldwide")
baseMetadataExport.addLangUI("fr")
baseMetadataExport.addLangUI("en")


#Création du corpus des enquêtes
corpusMetadataExport = scrutariDataExport.newCorpus("enquete");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Enquêtes")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "en","Investigations")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Enquête n° ")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "en", "Investigation #")


#Récupération des pages avec la liste des articles par langue
checkEnquetes("Enquetes", "fr")
checkEnquetes("Investigations", "en")

#Fin de l'export
scrutariDataExport.endExport()
scrutariDataFile.close()

#Renommage du fichier temporaire
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#Création du fichier d'information avec la date du jour
scrutariInfoFile = open(DESTINATION_DIRECTORY + "multinationales.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "multinationales.scrutari-data.xml")
scrutariInfoFile.close()