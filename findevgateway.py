#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://www.altramerica.info
#-------------------------------------------------------------------

import re
from bs4 import BeautifulSoup
from scrutaridataexport import *
from scrutaridataexport_utils import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
NAME = "findevgateway"
HOST = "www.findevgateway.org"
ROOT_URL = "https://www.findevgateway.org"
ENGLISH_URL = "https://www.findevgateway.org/publications"
SPANISH_URL = "https://www.findevgateway.org/es/publicaciones"
FRENCH_URL = "https://www.findevgateway.org/fr/publications"


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

ficheIdSet = set()

def start(url, lang):
    nextQuery = ""
    while True:
        nextQuery = loadPage(url, nextQuery, lang)
        if not nextQuery:
            break

def loadPage(url, query, lang):
    htmlContent = utils_urlopen(url + query, HOST)
    soup = BeautifulSoup(htmlContent,"lxml")
    div = soup.find("div", id="block-findev-content")
    if not div:
        return
    listingTexts = div.find_all("div", class_="listing__text")
    for listingText in listingTexts:
        addFiche(listingText, lang)
    return getNextQuery(soup)        
    
def addFiche(element, lang):
    h3 = element.find("h3")
    if not h3:
        return
    titleLink = h3.find("a")
    if not titleLink:
        return
    href = titleLink["href"]
    idx = href.rfind('/')
    ficheId = href[idx+1:]
    if ficheId in ficheIdSet:
        return
    ficheIdSet.add(ficheId)
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setLang(lang)
    ficheExport.setHref(ROOT_URL + href)
    ficheExport.setTitre(titleLink.get_text())
    ficheExport.setSoustitre(getSoustitre(element))
    ficheExport.setDate(getDate(element))
    authors = getAuthors(element)
    if authors:
        ficheExport.addAttributeValue("sct", "authors", authors)
    
def getSoustitre(element):
    desc = element.find("div", class_="listing__desc")
    if not desc:
        return ""
    return desc.get_text()

def getAuthors(element):
    meta = element.find("div", class_="postmeta")
    if not meta:
        return ""
    return meta.get_text()

def getDate(element):
    date = element.find("time", class_="datetime")
    if not date:
        return ""
    datetime = date["datetime"]
    return datetime[0:10]

def getNextQuery(soup):
    paginationList = soup.find("ul", class_="pagination")
    if not paginationList:
        return ""
    nextLink = paginationList.find("a", rel="next")
    if not nextLink:
        return ""
    return nextLink["href"]


#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#start
scrutariDataFile = utils_openScrutariDataFile(DESTINATION_DIRECTORY, NAME)
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))
metadataWrapper = utils_initMetadataWrapper(YAML_FILE)

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_publication
corpusMetadataExport = scrutariDataExport.newCorpus("publication");
metadataWrapper.fillCorpus(corpusMetadataExport)
start(ENGLISH_URL, "en")
start(SPANISH_URL, "es")
start(FRENCH_URL, "fr")

#end
scrutariDataExport.endExport()
utils_closeScrutariDataFile(DESTINATION_DIRECTORY, NAME, scrutariDataFile)
utils_writeScrutariInfoFile(DESTINATION_DIRECTORY, NAME)
