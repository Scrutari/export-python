#!/usr/bin/env python3

#-------------------------------------------------------------------
# OSM - identity=*
# Need Overpy / pip3 install overpy
#-------------------------------------------------------------------

import sys, shutil, yaml, csv
import overpy
from scrutaridataexport import *
    

#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
THESAURUS_CSV_FILE = sys.argv[3]
API_URL = "https://api.openstreetmap.fr/api/interpreter"
ROOT_URL = "https://www.openstreetmap.org/node/"


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def loadOapi():
    api = overpy.Overpass(url=API_URL)
    result = api.query("""
        node["identity"];
        out body;
        """)
    for node in result.nodes:
        tags = node.tags
        name = tags.get("name")
        if name:
            nodeId = str(node.id)
            ficheExport = scrutariDataExport.newFiche(nodeId)
            ficheExport.setTitre(name)
            ficheExport.setGeoloc(node.lat, node.lon)
            ficheExport.setHref(ROOT_URL + nodeId)
            identities = tags["identity"].split(";")
            for identity in identities:
                scrutariDataExport.addIndexation("node", nodeId, "identity", identity, 1)
            website = tags.get("website")
            if website:
                ficheExport.addAttributeValue("sct", "website", website)


#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding="UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#buffer
thesaurusBuffers = ThesaurusBuffers()
csvFile = open(THESAURUS_CSV_FILE, "r", encoding="UTF-8", newline='')
csvreader = csv.reader(csvFile, delimiter=',')
thesaurusBuffers.readCsv(csvreader)
csvFile.close()

##start
scrutariDataPath = DESTINATION_DIRECTORY + "osm-transformmap.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_node
corpusMetadataExport = scrutariDataExport.newCorpus("node");
metadataWrapper.fillCorpus(corpusMetadataExport)

loadOapi()

#thesaurus_identity
thesaurusMetadataExport = scrutariDataExport.newThesaurus("identity");
metadataWrapper.fillThesaurus(thesaurusMetadataExport)
thesaurusBuffers.fill("identity", scrutariDataExport)
    
#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "osm-transformmap.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "osm-transformmap.scrutari-data.xml")
scrutariInfoFile.close()

