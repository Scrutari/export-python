#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://france.attac.org
#-------------------------------------------------------------------

from bs4 import BeautifulSoup
from scrutaridataexport import *
from scrutaridataexport_utils import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
NAME = "attac-france"
HOST = "france.attac.org"
RACINE_ATTAC = "https://france.attac.org/"
LES_POSSIBLES = "https://france.attac.org/nos-publications/les-possibles/"

def checkLesPossibles():
    htmlContent = utils_urlopen(LES_POSSIBLES, HOST)
    soup = BeautifulSoup(htmlContent,"lxml")
    links = soup.select("div.menu.rubriques li a")
    for link in links:
        checkNumero(link["href"])
        
def checkNumero(href):
    htmlContent = utils_urlopen(RACINE_ATTAC + href, HOST)
    soup = BeautifulSoup(htmlContent,"lxml")
    articles = soup.select("div.article-body")
    for article in articles:
        link = article.find("a", recursive=False)
        if link and hasattr(link, "href"):
            href = link["href"]
            if href.startswith("nos-publications/"):
                introduction = article.find("div", class_="introduction")
                time = article.find("time")
                if introduction:
                    ficheExport = scrutariDataExport.newFiche(getFicheId(introduction["class"]))
                    ficheExport.setLang("fr")
                    ficheExport.setTitre(article.find("h3").get_text())
                    soustitre = checkSoustitre(introduction.find("p"))
                    ficheExport.setSoustitre(soustitre)
                    ficheExport.setHref(RACINE_ATTAC + href)
                    if time:
                        ficheExport.setDate(time["datetime"][0:10])
                    authors = article.select("span.vcard.author")
                    for author in authors:
                        ficheExport.addAttributeValue("sct","authors",author.get_text())

def checkSoustitre(pElement):
    if not pElement:
        return ""
    soustitre = pElement.get_text()
    idx = soustitre.find("\n")
    if idx > 0:
        soustitre = soustitre[0:idx]
    soustitreLen = len(soustitre)
    if soustitreLen < 150:
        return soustitre
    idx = soustitre.find(".", 150, soustitreLen)
    if idx > 0:
        return soustitre[0:(idx+1)]
    return soustitre
    
        
def getFicheId(classAttribute):
    for className in classAttribute:
        if className.startswith("article-intro-"):
            return className[14:]
    return ""
    
    

#-------------------------------------------------------------------
# Procédure principale
#-------------------------------------------------------------------

#start
scrutariDataFile = utils_openScrutariDataFile(DESTINATION_DIRECTORY, NAME)
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))
metadataWrapper = utils_initMetadataWrapper(YAML_FILE)

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_lespossibles
corpusMetadataExport = scrutariDataExport.newCorpus("lespossibles")
metadataWrapper.fillCorpus(corpusMetadataExport)
checkLesPossibles()

#end
scrutariDataExport.endExport()
utils_closeScrutariDataFile(DESTINATION_DIRECTORY, NAME, scrutariDataFile)
utils_writeScrutariInfoFile(DESTINATION_DIRECTORY, NAME)
