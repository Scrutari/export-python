#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://knowledgehub.unsse.org
#-------------------------------------------------------------------

from scrutaridataexport import *
from scrutaridataexport_utils import *
from wordpressapi import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
NAME = "unsse-hub"
HOST = "knowledgehub.unsse.org"
PER_PAGE = "100"
POSTS_URL = "https://knowledgehub.unsse.org/wp-json/wp/v2/knowledge-hub?per_page=" + PER_PAGE
AUTHORS_URL = "https://knowledgehub.unsse.org/wp-json/wp/v2/publication_author?per_page=" + PER_PAGE
TAGS_URL = "https://knowledgehub.unsse.org/wp-json/wp/v2/publication_tag?per_page=" + PER_PAGE
COUNTRIES_URL = "https://knowledgehub.unsse.org/wp-json/wp/v2/country?per_page=" + PER_PAGE
SDG_URL = "https://knowledgehub.unsse.org/wp-json/wp/v2/sdg?per_page=" + PER_PAGE
LANGUAGES_URL = "https://knowledgehub.unsse.org/wp-json/wp/v2/hub_language?per_page=" + PER_PAGE
#Voir https://developer.wordpress.org/rest-api/using-the-rest-api/global-parameters/
MATCH = {"english":"en","francais":"fr","portugues":"pt","espanol":"es"}
SHORT_URL = "https://knowledgehub.unsse.org/?p="


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------
        
def initLanguageMap(itemMap, url):
    jsonData = utils_toJson(utils_urlopen(url, HOST))
    for item in jsonData:
        slug = item["slug"]
        if slug in MATCH:
          itemMap[item["id"]] = MATCH[slug]

def readPostPage(jsonResponse):
    jsonData = utils_toJson(jsonResponse)
    for item in jsonData:
        langCode = None
        languageArray = item["hub_language"]
        for languageId in languageArray:
            if languageId in languageMap:
                langCode = languageMap[languageId]
        if not langCode:
            continue
        ficheId = item["id"]
        ficheExport = scrutariDataExport.newFiche(ficheId)
        ficheExport.setTitre(wp_getText(item, "title"))
        ficheExport.setHref(SHORT_URL + str(ficheId))
        ficheExport.setLang(langCode)
        ficheExport.setDate(wp_getDate(item))
        authorArray = item["publication_author"]
        for authorId in authorArray:
            if authorId in authorMap:
                authorName = authorMap[authorId]
                ficheExport.addAttributeValue("sct", "authors", authorName)
        tagArray = item["publication_tag"]
        for tagId in tagArray:
            if tagId in tagMap:
                tagName = tagMap[tagId]
                ficheExport.addAttributeValue("sct", "tags", tagName)
                

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

authorMap = {}
tagMap = {}
languageMap = {}

#init
wp_populateTaxonomyMap(authorMap, AUTHORS_URL, HOST)
wp_populateTaxonomyMap(tagMap, TAGS_URL, HOST)
initLanguageMap(languageMap, LANGUAGES_URL)  

#start
scrutariDataFile = utils_openScrutariDataFile(DESTINATION_DIRECTORY, NAME)
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))
metadataWrapper = utils_initMetadataWrapper(YAML_FILE)

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_post
corpusMetadataExport = scrutariDataExport.newCorpus("post");
metadataWrapper.fillCorpus(corpusMetadataExport)
wp_readPages(POSTS_URL, HOST, readPostPage)

#end
scrutariDataExport.endExport()
utils_closeScrutariDataFile(DESTINATION_DIRECTORY, NAME, scrutariDataFile)
utils_writeScrutariInfoFile(DESTINATION_DIRECTORY, NAME)
