#!/usr/bin/env python3
#-------------------------------------------------------------------
# Script de génération du fichier ScrutariData pour le site http://http://www.gsef-net.org/
#
#-------------------------------------------------------------------

#-------------------------------------------------------------------
# Ce script utilise le module BeautifulSoup http://www.crummy.com/software/BeautifulSoup/
# ainsi que le module lxml (voir l'appel de BeautifulSoup(htmlContent, "lxml"))
# Les autres modules utilisés sont standards
# scrutaridataexport correspond au contenu du fichier scrutaridataexport.py
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re
from bs4 import BeautifulSoup
from scrutaridataexport import *

#-------------------------------------------------------------------
# Ce script a un seul argument obligatoire : le chemin du répertoire de destination
# où sera enregistré les fichiers gfsenet.scrutari-data.xml et gfsenet.scrutari-info.xml
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
RACINE_SITE="http://www.gsef-net.org"

def checkCategory(url):
    rootUrl = RACINE_SITE + url
    page = 0
    while(True):
        url = rootUrl + "?keys=&page="
        if page > 0:
            url = url + str(page)
        htmlContent = urllib.request.urlopen(url)
        soup = BeautifulSoup(htmlContent,"lxml")
        liList = soup.select("div.view-content li")
        if len(liList) > 0:
            for li in liList:
                checkLi(li)
            page = page + 1
        else:
            break
        
def checkLi(liElement):
    h1 = liElement.find("h1")
    link = h1.find("a")
    href = link["href"]
    last = href.rfind('/')
    ficheId = href[last+1:]
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setLang("en")
    ficheExport.setHref(RACINE_SITE + href)
    ficheExport.setTitre(link.get_text())
    h2 = liElement.find("h2")
    if h2:
        ficheExport.setSoustitre(h2.get_text())
    checkNode(RACINE_SITE + href, ficheExport)

def checkNode(url, ficheExport):
    htmlContent = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlContent,"lxml")
    span = soup.find("span", class_="date-display-single")
    if span:
        date = span["content"][0:10]
        ficheExport.setDate(date)
    

#-------------------------------------------------------------------
# Procédure principale
#-------------------------------------------------------------------

#Départ de l'exportation
scrutariDataPath = DESTINATION_DIRECTORY + "gfsenet.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))


baseMetadataExport = scrutariDataExport.startExport()
baseMetadataExport.setAuthority("gsef-net.org")
baseMetadataExport.setBaseName("site")
baseMetadataExport.setBaseIcon("http://static.scrutari.net/gfsenet.png")
baseMetadataExport.setIntitule(INTITULE_SHORT, "en", "Global Social Economy Forum (GSEF)")
baseMetadataExport.setIntitule(INTITULE_LONG, "en", "Global Social Economy Forum, The Global platform for promoting social economy")
baseMetadataExport.addLangUI("en")


#Création du corpus des mémoires et thèses
corpusMetadataExport = scrutariDataExport.newCorpus("resource");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "en","Resources")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "en", "Resource # ")

checkCategory('/en/publication')

#Fin de l'export
scrutariDataExport.endExport()
scrutariDataFile.close()

#Renommage du fichier temporaire
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#Création du fichier d'information avec la date du jour
scrutariInfoFile = open(DESTINATION_DIRECTORY + "gfsenet.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "gfsenet.scrutari-data.xml")
scrutariInfoFile.close()