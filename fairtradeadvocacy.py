#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://fairtrade-advocacy.org
#-------------------------------------------------------------------

import re
from bs4 import BeautifulSoup
from scrutaridataexport import *
from scrutaridataexport_utils import *

#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
NAME = "fairtradeadvocacy"
HOST = "fairtrade-advocacy.org"
INDEX_URL = "https://fairtrade-advocacy.org/ftao-publications/publications-statements/"



#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def checkIndex():
    htmlContent = utils_urlopen(INDEX_URL, HOST)
    soup = BeautifulSoup(htmlContent,"lxml")
    parentDiv = soup.find("div", class_="hentry")
    elementList = parentDiv.find_all(["h3", "p"], recursive=False)
    year = ""
    for element in elementList:
        if element.name == "h3":
            year = element.get_text()
        else:
            addFiche(element, year)
            
def addFiche(element, year):
    links = element.find_all("a", recursive=False)
    title = ""
    ficheId = False
    ficheHref = ""
    for link in links:
        if not link.has_attr("href"):
            continue
        href = link["href"]
        currentId = toId(href)
        if not currentId:
            continue
        else:
            if not ficheId:
                ficheId = currentId
                title = link.get_text()
                ficheHref = href
            else:
                if ficheId == currentId:
                    title = title + " " + link.get_text()
    if ficheId and len(title) > 10:
        ficheExport = scrutariDataExport.newFiche(toId(href))
        ficheExport.setLang("en")
        ficheExport.setHref(ficheHref)
        ficheExport.setDate(year)
        ficheExport.setTitre(title)
        
def toId(href):
    advocacy = href.find("fairtrade-advocacy")
    if advocacy < 0:
        return False
    return utils_hrefToFicheId(href)
        

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#start
scrutariDataFile = utils_openScrutariDataFile(DESTINATION_DIRECTORY, NAME)
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))
metadataWrapper = utils_initMetadataWrapper(YAML_FILE)

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_publication
corpusMetadataExport = scrutariDataExport.newCorpus("publication");
metadataWrapper.fillCorpus(corpusMetadataExport)
checkIndex()

#end
scrutariDataExport.endExport()
utils_closeScrutariDataFile(DESTINATION_DIRECTORY, NAME, scrutariDataFile)
utils_writeScrutariInfoFile(DESTINATION_DIRECTORY, NAME)
