# Scripts Python de « scrutarisation »

Ce dépôt contient des scripts écrits en Python pour récupérer directement sur des sites des métadonnées nécessaires à la création de fichier au format ScrutariData.

Le fichier scrutaridataexport.py est l'implémentation en Python de l'API ScrutariDataExport.

Les scripts sont sous licence MIT.

## Pour en savoir plus :
- Scrutari : http://www.scrutari.net/
- API ScrutariDataExport : http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
- sur les scripts Python : http://www.scrutari.net/dokuwiki/scrutaridata:exportapi:python
