#!/usr/bin/env python3
#-------------------------------------------------------------------
# Script de génération du fichier ScrutariData pour le site http://reliees
# Ce script récupère la page « centre de documentation » où sont listés toutes les fiches
# dans les trois langues.
# Les thésaurus sont prédéfinis : il manque la mise à jour du thésaurus « keyword » qui n'est
# pas possible en l'état car la correspondance entre version anglaise, espagnole et française
# d'un mot-clé ne peut pas être établie en lisant la page
#-------------------------------------------------------------------

#-------------------------------------------------------------------
# Ce script utilise le module BeautifulSoup http://www.crummy.com/software/BeautifulSoup/
# ainsi que le module lxml (voir l'appel de BeautifulSoup(htmlContent, "lxml"))
# Les autres modules utilisés sont standards
# scrutaridataexport correspond au contenu du fichier scrutaridataexport.py
#-------------------------------------------------------------------

import sys, shutil, urllib.request
from bs4 import BeautifulSoup
from scrutaridataexport import *


#-------------------------------------------------------------------
# Ce script a un seul argument obligatoire : le chemin du répertoire de destination
# où sera enregistré les fichiers reliess.scrutari-data.xml et reliess.scrutari-info.xml
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]


#-------------------------------------------------------------------
# Fonctions de traitement
#-------------------------------------------------------------------

def exportCorpus(typeClass, corpusName):
    "Lit les trois pages pour un corpus donné"
    exportLangCorpus(typeClass, corpusName, frReliess, "langue_1", "fr")
    exportLangCorpus(typeClass, corpusName, enReliess, "langue_2", "en")
    exportLangCorpus(typeClass, corpusName, esReliess, "langue_3", "es")

def exportLangCorpus(typeClass, corpusName, htmlContent, langClass, langCode):
    "Lecture d'une page et extraction des fiches dans la langue et du corpus"
    ficheDivs = htmlContent.find_all("div", class_="file")
    for ficheDiv in ficheDivs:
        if ficheDiv.find("div", class_=langClass) and ficheDiv.find("div", class_=typeClass):
            ficheId = ficheDiv["id"][3:]
            ficheExport = scrutariDataExport.newFiche(ficheId)
            ficheExport.setLang(langCode)
            titleDiv = ficheDiv.find("div", class_="file_title")
            if titleDiv:
                ficheExport.setTitre(titleDiv.get_text())
            descDiv = ficheDiv.find("div", class_="file_desc")
            if descDiv:
                ficheExport.setSoustitre(descDiv.get_text().strip())
            ficheExport.setHref(getHref(langCode) + '#id_' + ficheId)
            porteeDiv = ficheDiv.find("div", class_="file_portee")
            if porteeDiv:
                porteeId = getId(porteeDiv['class'], "portee_")
                if porteeId:
                    scrutariDataExport.addIndexation(corpusName, ficheId, "portee", porteeId, 1)
            politiqueDiv = ficheDiv.find("div", class_="file_politique")
            if politiqueDiv:
                politiqueId = getId(politiqueDiv['class'], "politique_")
                if politiqueId:
                    scrutariDataExport.addIndexation(corpusName, ficheId, "politique", politiqueId, 1)
            keywordDiv = ficheDiv.find("div", class_="file_keywords")
            if keywordDiv:
                keywordLis = keywordDiv.find_all("li")
                for keywordLi in keywordLis:
                    keywordId = keywordThesaurus.getMotcleId(langCode, keywordLi.get_text())
                    if keywordId:
                        scrutariDataExport.addIndexation(corpusName, ficheId, "keyword", keywordId, 1)

def getHref(langCode):
    "Donne l'adresse de la page en fonction de la langue"
    if langCode == "fr":
        return 'http://reliess.org/centre-de-documentation/'
    if langCode == "en":
        return 'http://reliess.org/documentation-centre/?lang=en'
    if langCode == "es":
        return 'http://reliess.org/Centro-de-documentacion/?lang=es'
    return ""

def getId(classes, prefix):
    "Récupère les identifiants à partir des classes de type portee_14, politique_10"
    prefixlen = len(prefix)
    for cssClass in classes:
        if cssClass.startswith(prefix):
            return cssClass[prefixlen:]
    return None
            


#-------------------------------------------------------------------
# Procédure principale
#-------------------------------------------------------------------

#Construction des thésaurus initiaux
keywordThesaurus = ThesaurusBuffer(DESTINATION_DIRECTORY + "thesaurus-keyword.xml")
porteeThesaurus = ThesaurusBuffer(DESTINATION_DIRECTORY + "thesaurus-portee.xml")
politiqueThesaurus = ThesaurusBuffer(DESTINATION_DIRECTORY + "thesaurus-politique.xml")


#Récupération du contenu des trois pages
#frContent = open("/home/vic/en_cours/socioeco/reliess_fr.html", "r", encoding = "UTF-8")
frContent = urllib.request.urlopen(getHref("fr"))
frReliess = BeautifulSoup(frContent, "lxml")
frContent.close()

#enContent = open("/home/vic/en_cours/socioeco/reliess_en.html", "r", encoding = "UTF-8")
enContent = urllib.request.urlopen(getHref("en"))
enReliess = BeautifulSoup(enContent, "lxml")
enContent.close()

#esContent = open("/home/vic/en_cours/socioeco/reliess_es.html", "r", encoding = "UTF-8")
esContent = urllib.request.urlopen(getHref("es"))
esReliess = BeautifulSoup(esContent, "lxml")
esContent.close()


#Départ de l'exportation
scrutariDataPath = DESTINATION_DIRECTORY + "reliess.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#Initialisation de la base du Reliess
baseMetadataExport = scrutariDataExport.startExport()
baseMetadataExport.setAuthority("eabb0a80-072f-11e2-892e-0800200c9a66")
baseMetadataExport.setBaseName("reliess")
baseMetadataExport.setBaseIcon("http://static.scrutari.net/reliess.png")
baseMetadataExport.setIntitule(INTITULE_SHORT, "fr", "Reliess")
baseMetadataExport.setIntitule(INTITULE_SHORT, "en", "Reliess")
baseMetadataExport.setIntitule(INTITULE_SHORT, "es", "Reliess")
baseMetadataExport.setIntitule(INTITULE_LONG, "fr", "Reliess - Politiques publiques en économie sociale et solidaire")
baseMetadataExport.setIntitule(INTITULE_LONG, "en", "Reliess - Public policy for the social & solidarity economy")
baseMetadataExport.setIntitule(INTITULE_LONG, "es", "Reliess - Políticas públicas en economía social y solidaria")
baseMetadataExport.addLangUI("fr")
baseMetadataExport.addLangUI("en")
baseMetadataExport.addLangUI("es")

corpusMetadataExport = scrutariDataExport.newCorpus("analyse")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Analyses")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Analyse")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "en","Analysis")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "en", "Analysis")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "es","Análisis")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "es", "Análisis")
exportCorpus("type_1", "analyse")

corpusMetadataExport = scrutariDataExport.newCorpus("etudedecas")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Études de cas")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Étude de cas")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "en","Case studies")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "en", "Case study")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "es","Estudios de caso")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "es", "Estudio de caso")
exportCorpus("type_2", "etudedecas")

corpusMetadataExport = scrutariDataExport.newCorpus("outilpublic")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Outils publics")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Outil public")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "en","Public tools")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "en", "Public tool")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "es","Herramientas públicas")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "es", "Herramienta pública")
exportCorpus("type_11", "outilpublic")

corpusMetadataExport = scrutariDataExport.newCorpus("fiche")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Fiches")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Fiche")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "en","Sheets")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "en", "Sheet")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "es","Fichas")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "es", "Ficha")
exportCorpus("type_14", "fiche")

thesaurusMetadataExport = scrutariDataExport.newThesaurus("portee")
thesaurusMetadataExport.setIntitule(INTITULE_THESAURUS,"fr","Portée")
thesaurusMetadataExport.setIntitule(INTITULE_THESAURUS,"en","Scope")
thesaurusMetadataExport.setIntitule(INTITULE_THESAURUS,"es","Ámbito de aplicación")
porteeThesaurus.exportMotscles(scrutariDataExport)

thesaurusMetadataExport = scrutariDataExport.newThesaurus("politique")
thesaurusMetadataExport.setIntitule(INTITULE_THESAURUS,"fr","Politique")
thesaurusMetadataExport.setIntitule(INTITULE_THESAURUS,"en","Policy")
thesaurusMetadataExport.setIntitule(INTITULE_THESAURUS,"es","Política")
politiqueThesaurus.exportMotscles(scrutariDataExport)

thesaurusMetadataExport = scrutariDataExport.newThesaurus("keyword")
thesaurusMetadataExport.setIntitule(INTITULE_THESAURUS,"fr","Mots-clés")
thesaurusMetadataExport.setIntitule(INTITULE_THESAURUS,"en","Keywords")
thesaurusMetadataExport.setIntitule(INTITULE_THESAURUS,"es","Palabras claves")
keywordThesaurus.exportMotscles(scrutariDataExport)


#Fin de l'export
scrutariDataExport.endExport()
scrutariDataFile.close()

#Renommage du fichier temporaire
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#Création du fichier d'information avec la date du jour
scrutariInfoFile = open(DESTINATION_DIRECTORY + "reliess.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "reliess.scrutari-data.xml")
scrutariInfoFile.close()