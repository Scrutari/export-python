#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://kartevonmorgen.org
#-------------------------------------------------------------------

import sys, shutil, urllib.request, json, yaml
import langid
from bs4 import BeautifulSoup
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]

ROOT_URL = "https://kartevonmorgen.org/api/v0/search?text=&bbox=-90,-180,90,180&categories="
ENTRY_URL = "https://kartevonmorgen.org/#/?zoom=8.00&entry="


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def loadJson(category):
    jsonResponse = urllib.request.urlopen(ROOT_URL + category)
    jsonObj = json.loads(jsonResponse.read().decode("UTF-8"))
    for entry in jsonObj["visible"]:
        ficheExport = scrutariDataExport.newFiche(entry["id"])
        ficheExport.setLang(testLang(entry))
        ficheExport.setGeoloc(entry["lat"], entry["lng"])
        ficheExport.setTitre(entry["title"])
        ficheExport.setSoustitre(entry["description"])
        ficheExport.setHref(ENTRY_URL + entry["id"])
        for tag in entry["tags"]:
            if len(tag) > 0:
                ficheExport.addAttributeValue("sct", "tags", tag)
                
def testLang(entry):
    text = entry["description"]
    if not text:
        text = entry["title"]
    return utils_langidentify(text)

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding="UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "kartevonmorgen-geo.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_initiative
corpusMetadataExport = scrutariDataExport.newCorpus("initiative");
metadataWrapper.fillCorpus(corpusMetadataExport)
loadJson("2cd00bebec0c48ba9db761da48678134")

#corpus_initiative
corpusMetadataExport = scrutariDataExport.newCorpus("enterprise");
metadataWrapper.fillCorpus(corpusMetadataExport)

loadJson("77b3c33a92554bcf8e8c2c86cedd6f6f")
    

#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "kartevonmorgen-geo.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "kartevonmorgen-geo.scrutari-data.xml")
scrutariInfoFile.close()
