#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://www.altramerica.info
#-------------------------------------------------------------------

import re
from bs4 import BeautifulSoup
from scrutaridataexport import *
from scrutaridataexport_utils import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
NAME = "altramerica"
HOST = "www.altramerica.info"
ARTICULOS_URL = "https://www.altramerica.info/america-latina-y-europa/"


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

ficheIdSet = set()

def checkArticulos():
    htmlContent = utils_urlopen(ARTICULOS_URL, HOST)
    soup = BeautifulSoup(htmlContent,"lxml")
    postContent = soup.find("div", class_="post-content")
    if postContent:
        pList = postContent.find_all("p", recursive=False)
        for p in pList:
            addFiche(p)
            
def addFiche(element):
    href = getHref(element)
    if href.find("altramerica.info") < 0:
        return
    ficheId = utils_hrefToFicheId(href)
    if ficheId in ficheIdSet:
        return
    title = getTitle(element)
    if not title:
        return
    ficheIdSet.add(ficheId)
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setTitre(title)
    ficheExport.setHref(href)
    ficheExport.setLang(utils_langidentify(title))
    ficheExport.setDate(getYear(element))
    ficheExport.addAttributeValue("sct", "authors", "Marco Coscione")    


def getTitle(element):
    ems = element.find_all("em")
    if not ems:
        return ""
    title = ""
    for em in ems:
        title = title + em.get_text()
    return title

def getHref(element):
    links = element.find_all("a")
    if not links:
        return ""
    for link in links:
        if link.get_text() == "PDF":
            return link["href"]
    em = element.find("em", recursive=False)
    if not em:
        return ""
    link = em.find("a")
    if not link:
        return ""
    return link["href"]

def getYear(element):
    strong = element.find("strong", recursive=False)
    if not strong:
        return ""
    yearSearch = re.search("([0-9]{4})", strong.get_text())
    if yearSearch:
        return yearSearch.group(1)


    

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#start
scrutariDataFile = utils_openScrutariDataFile(DESTINATION_DIRECTORY, NAME)
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))
metadataWrapper = utils_initMetadataWrapper(YAML_FILE)

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_publication
corpusMetadataExport = scrutariDataExport.newCorpus("articulo");
metadataWrapper.fillCorpus(corpusMetadataExport)
checkArticulos()

#end
scrutariDataExport.endExport()
utils_closeScrutariDataFile(DESTINATION_DIRECTORY, NAME, scrutariDataFile)
utils_writeScrutariInfoFile(DESTINATION_DIRECTORY, NAME)
