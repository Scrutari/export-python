#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://www.economiasolidaria.org
#-------------------------------------------------------------------

from scrutaridataexport import *
from scrutaridataexport_utils import *
from wordpressapi import *

#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
URL="https://www.economiasolidaria.org"
YAML_FILE = sys.argv[2]
NAME = "economiasolidaria"
HOST = "economiasolidaria.org"
PER_PAGE = "100"
RECURSOS_URL = "https://www.economiasolidaria.org/wp-json/wp/v2/recursos?per_page=" + PER_PAGE
TAGS_URL = "https://www.economiasolidaria.org/wp-json/wp/v2/tags?per_page=" + PER_PAGE
TEMATICAS_URL = "https://www.economiasolidaria.org/wp-json/wp/v2/tematicas?per_page=" + PER_PAGE
TIPOS_URL = "https://www.economiasolidaria.org/wp-json/wp/v2/tipos"
SHORT_URL= "https://www.economiasolidaria.org/?p="
LANG = "es"


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def getOtherTipos():
    jsonData = utils_toJson(utils_urlopen(TIPOS_URL, HOST))
    otherTipos = ""
    for item in jsonData:
        if item["id"] != 882:
            if len(otherTipos) > 0:
                otherTipos = otherTipos + ","
            otherTipos = otherTipos + str(item["id"])
    return otherTipos
    
def readRecursoPage(jsonResponse):
    readPostPage(jsonResponse, "recurso")
    
def readVideoPage(jsonResponse):
    readPostPage(jsonResponse, "video")

def readPostPage(jsonResponse, corpus):
  jsonData = utils_toJson(jsonResponse)
  for item in jsonData:
        ficheId = item["id"]
        ficheExport = scrutariDataExport.newFiche(ficheId)
        ficheExport.setTitre(wp_getText(item, "title"))
        ficheExport.setHref(SHORT_URL + str(ficheId))
        ficheExport.setLang(LANG)
        ficheExport.setDate(wp_getDate(item))
        ficheExport.setSoustitre(wp_getText(item, "excerpt"))
        tagArray = item["tags"]
        for tagId in tagArray:
            motcleId = str(tagId)
            if tagBuffer.containsMotcle(motcleId):
                scrutariDataExport.addIndexation(corpus, ficheId, "tag", motcleId, 1)
        tematicaArray = item["tematicas"]
        for tematicaId in tematicaArray:
            motcleId = str(tematicaId)
            if tematicaBuffer.containsMotcle(motcleId):
                scrutariDataExport.addIndexation(corpus, ficheId, "tematica", motcleId, 1)


#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#start
scrutariDataFile = utils_openScrutariDataFile(DESTINATION_DIRECTORY, NAME)
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))
metadataWrapper = utils_initMetadataWrapper(YAML_FILE)

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)


#thesaurusBuffer
tagBuffer = ThesaurusBuffer()
wp_populateThesaurus(tagBuffer, TAGS_URL, "es", HOST)
tematicaBuffer = ThesaurusBuffer()
wp_populateThesaurus(tematicaBuffer, TEMATICAS_URL, "es", HOST)

#corpus_recurso
corpusMetadataExport = scrutariDataExport.newCorpus("recurso");
metadataWrapper.fillCorpus(corpusMetadataExport)
wp_readPages(RECURSOS_URL + "&tipos=" + getOtherTipos(), HOST, readRecursoPage)

#corpus_video
corpusMetadataExport = scrutariDataExport.newCorpus("video");
metadataWrapper.fillCorpus(corpusMetadataExport)
wp_readPages(RECURSOS_URL + "&tipos=882", HOST, readVideoPage)

#thesaurus_tematica
thesaurusMetadataExport = scrutariDataExport.newThesaurus("tematica");
metadataWrapper.fillThesaurus(thesaurusMetadataExport)
tematicaBuffer.exportMotscles(scrutariDataExport)

#thesaurus_tag
thesaurusMetadataExport = scrutariDataExport.newThesaurus("tag");
metadataWrapper.fillThesaurus(thesaurusMetadataExport)
tagBuffer.exportMotscles(scrutariDataExport)

#end
scrutariDataExport.endExport()
utils_closeScrutariDataFile(DESTINATION_DIRECTORY, NAME, scrutariDataFile)
utils_writeScrutariInfoFile(DESTINATION_DIRECTORY, NAME)
