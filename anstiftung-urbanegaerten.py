#!/usr/bin/env python3

#-------------------------------------------------------------------
# http://redeconvergir.net/
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re, yaml
from bs4 import BeautifulSoup
from lxml import etree
from scrutaridataexport_utils import *
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
XML_URL = "https://anstiftung.de/index.php?option=com_storelocator&view=map&format=raw&searchall=1&Itemid=520&catid=-1&tagid=-1&featstate=0"


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def parseXml():
    tree = etree.parse(XML_URL)
    markers = tree.xpath("marker")
    for marker in markers:
        name = marker.xpath("name")[0].text
        ficheId = utils_slugify(name)
        ficheExport = scrutariDataExport.newFiche(ficheId)
        ficheExport.setLang("de")
        ficheExport.setTitre(name)
        lat = marker.xpath("lat")[0].text
        lon = marker.xpath("lng")[0].text
        ficheExport.setGeoloc(lat, lon)
        beschreibung =  marker.xpath("custom3")[0]
        beschreibungSoup = BeautifulSoup(beschreibung.text,"lxml")
        soustitre = beschreibungSoup.get_text()
        if len(soustitre) > 0:
            soustitre = soustitre[0:1000]
        ficheExport.setSoustitre(soustitre)
        url =  marker.xpath("url")[0].text
        if url:
            ficheExport.addAttributeValue("sct", "website", url)

    
#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding="UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "anstiftung-urbanegaerten.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding="UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_workshop
corpusMetadataExport = scrutariDataExport.newCorpus("workshop")
metadataWrapper.fillCorpus(corpusMetadataExport)
parseXml()
    
#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "anstiftung-urbanegaerten.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "anstiftung-urbanegaerten.scrutari-data.xml")
scrutariInfoFile.close()
