#!/usr/bin/env python3
#-------------------------------------------------------------------
# Script de génération du fichier ScrutariData pour le site http://portail.bastamag.net
#
#-------------------------------------------------------------------

#-------------------------------------------------------------------
# Ce script utilise le module BeautifulSoup http://www.crummy.com/software/BeautifulSoup/
# ainsi que le module lxml (voir l'appel de BeautifulSoup(htmlContent, "lxml"))
# Les autres modules utilisés sont standards
# scrutaridataexport correspond au contenu du fichier scrutaridataexport.py
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re
from bs4 import BeautifulSoup
from scrutaridataexport import *

#-------------------------------------------------------------------
# Ce script a un seul argument obligatoire : le chemin du répertoire de destination
# où sera enregistré les fichiers cetri.scrutari-data.xml et cetri.scrutari-info.xml
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
URL="https://portail.bastamag.net/spip.php?page=selection&debut_articles="
MAX=1000

def checkPagination():
    pagination = 0;
    previousTrace = ""
    while(True):
        htmlContent = urllib.request.urlopen(URL + str(pagination))
        soup = BeautifulSoup(htmlContent,"lxml")
        trace = checkTrace(soup)
        if trace == previousTrace:
            break
        previousTrace = trace
        checkList(soup)
        pagination = pagination + 50
        if (pagination > MAX):
            break

def checkTrace(soup):
    mainDiv = soup.find("div", class_="liste-box")
    articleList = mainDiv.find_all("article", class_="syndic_article")
    trace = ""
    for article in articleList:
        ficheId = getFicheId(article["class"])
        trace = trace + " " + ficheId
    return trace

def checkList(soup):
    mainDiv = soup.find("div", class_="liste-box")
    articleList = mainDiv.find_all("article", class_="syndic_article")
    for article in articleList:
        ficheId = getFicheId(article["class"])
        ficheExport = scrutariDataExport.newFiche(ficheId)
        ficheExport.setLang("fr")
        entryLink = article.find("a", class_="entry-link")
        ficheExport.setTitre(entryLink.get_text())
        ficheExport.setHref(entryLink["href"])
        time = article.find("time")
        if time:
            ficheExport.setDate(time["datetime"][0:10])
        entryContent = article.find("div", class_="entry-content")
        if entryContent:
            ficheExport.setSoustitre(entryContent.get_text())
        image = article.find("img", class_="favicon")
        if image:
            ficheExport.setFicheIcon("https://portail.bastamag.net/" + image["src"])
            ficheExport.addAttributeValue("sct","authors", image["alt"])

def getFicheId(classAttribute):
    for className in classAttribute:
        if className.startswith("syndic_article-resume-"):
            return className[22:]
    return ""    

#-------------------------------------------------------------------
# Procédure principale
#-------------------------------------------------------------------

#Départ de l'exportation
scrutariDataPath = DESTINATION_DIRECTORY + "bastamag-portail.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))


baseMetadataExport = scrutariDataExport.startExport()
baseMetadataExport.setAuthority("bastamag.net")
baseMetadataExport.setBaseName("portail")
baseMetadataExport.setBaseIcon("http://static.scrutari.net/bastamag-portail.png")
baseMetadataExport.setIntitule(INTITULE_SHORT, "fr", "Portail des médias libres")
baseMetadataExport.setIntitule(INTITULE_LONG, "fr", "Portail des médias libres, la sélection de Basta")
baseMetadataExport.addLangUI("fr")


#Création du corpus des mémoires et thèses
corpusMetadataExport = scrutariDataExport.newCorpus("article");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Articles")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Article n° ")

checkPagination()


#Fin de l'export
scrutariDataExport.endExport()
scrutariDataFile.close()

#Renommage du fichier temporaire
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#Création du fichier d'information avec la date du jour
scrutariInfoFile = open(DESTINATION_DIRECTORY + "bastamag-portail.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "bastamag-portail.scrutari-data.xml")
scrutariInfoFile.close()