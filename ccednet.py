#!/usr/bin/env python3
#-------------------------------------------------------------------
# https://ccednet-rcdec.ca
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *
import pycurl
from io import BytesIO

#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
RACINE_SITE="https://ccednet-rcdec.ca"

#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def checkTools():
    rootUrl = RACINE_SITE + "/en/toolbox"
    htmlContent =  urllib.request.urlopen(rootUrl)
    soup = BeautifulSoup(htmlContent,"lxml")
    checkList(soup)
    pagerLast = soup.find("li", class_= "pager-last")
    lastHref = pagerLast.find("a")["href"]
    lastIdx = lastHref.rfind("=")
    lastPage = int(lastHref[(lastIdx+1):])
    for i in range(1,lastPage):
        pageHtmlContent = urllib.request.urlopen(rootUrl +  "?page=" + str(i))
        pageSoup = BeautifulSoup(pageHtmlContent, "lxml")
        checkList(pageSoup)
    
    
def checkList(soup):
    viewContent = soup.find("div", class_="view-content")
    lis = viewContent.find_all("li")
    for li in lis:
        checkLi(li)
        
def checkLi(li):
    titleDiv = li.find("div", class_="views-field-title")
    if not titleDiv:
        return
    authorDiv = li.find("div", class_="views-field-field-author")
    if not authorDiv:
        return
    dateDiv = authorDiv.find_next_sibling("div")
    if not dateDiv:
        return
    link = titleDiv.find("a")
    href = link["href"]
    lang = href[1:3]
    idx = href.rfind("/")
    ficheExport = scrutariDataExport.newFiche(href[idx+1:])
    ficheExport.setTitre(link.get_text())
    ficheExport.setLang(lang)
    ficheExport.setHref(RACINE_SITE + href)
    authors = cleanText(authorDiv.find("div").get_text().strip())
    date = dateDiv.find("div").get_text().strip()
    if len(date) > 0:
        ficheExport.setDate(date)
    if len(authors) > 0:
        ficheExport.addAttributeValue("sct", "authors", authors)
    organizationDiv = li.find("div", class_="views-field-field-organization")
    if organizationDiv:
        ficheExport.setSoustitre(cleanText(organizationDiv.find("div").get_text().strip()))
    
def cleanText(text):
    textLength = len(text)
    if textLength > 0:
        if text[textLength - 1] == ",":
            text = text[0:(textLength - 1)]
    return text
    
    
    

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding = "UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "ccednet.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding="UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_article
corpusMetadataExport = scrutariDataExport.newCorpus("tool");
metadataWrapper.fillCorpus(corpusMetadataExport)

checkTools()


#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "ccednet.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "ccednet.scrutari-data.xml")
scrutariInfoFile.close()

