#!/usr/bin/env python3
#-------------------------------------------------------------------
# Script de génération du fichier ScrutariData pour le fichier CSV
# du site d'Handiplanet
# Ce script s'occupe de récupérer le fichier CSV (à condition de lui
# transmettre un identifiant et un mot de passe standard
#-------------------------------------------------------------------

#-------------------------------------------------------------------
# Les modules utilisés sont standards
# scrutaridataexport correspond au contenu du fichier scrutaridataexport.py
#-------------------------------------------------------------------

import sys, shutil, csv, urllib.request,  http.cookiejar
from scrutaridataexport import *


#-------------------------------------------------------------------
# Ce script a un seul argument obligatoire : le chemin du répertoire de destination
# où sera enregistré les fichiers handiplanet.scrutari-data.xml et  handiplanet.scrutari-info.xml
# (ce répertoire contient par ailleurs les thésaurus categories et souscategories),
# un répertoire de cache où est stocké le fichier CSV,
# l'identifiant
# le mot de passe
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
CACHE_DIRECTORY = sys.argv[2]
LOGIN = sys.argv[3]
PASSWORD = sys.argv[4]
HANDIPLANETLOGIN = "http://www.handiplanet-echanges.info/login_form"
HANDIPLANETCSV = "http://www.handiplanet-echanges.info/@@download-csv-contributions"
LOGINDATA = [("__ac_name",LOGIN), ("__ac_password", PASSWORD), ("js_enabled", "0"), ("form.submitted", "1"), ("pwd_empty", "0")]
CSVFILE = CACHE_DIRECTORY + "handiplanet.csv"
NUMEROPAYS = 1
NUMEROTAGS = 2


#-------------------------------------------------------------------
# Fonction de téléchargement
#-------------------------------------------------------------------

class NoRedirection(urllib.request.HTTPErrorProcessor):
    "Classe évitant la redirection de la page login, et ce afin de conserver le cookie"

    def http_response(self, request, response):
        return response

    https_response = http_response


def csvDownload():
    "Téléchargement en deux temps : identification avec récupératio du cookie, téléchargement"
    cookiejar = http.cookiejar.CookieJar()
    cookieProcessor = urllib.request.HTTPCookieProcessor(cookiejar)
    
    noRedirectionOpener = urllib.request.build_opener(NoRedirection, cookieProcessor)
    loginResponse = noRedirectionOpener.open(HANDIPLANETLOGIN, urllib.parse.urlencode(LOGINDATA).encode('utf-8'))
    
    csvOpener = urllib.request.build_opener(cookieProcessor)
    csvResponse = csvOpener.open(HANDIPLANETCSV)
    csvFile = open(CSVFILE, "wb")
    while not csvResponse.closed:
        buffer = csvResponse.read(1024)
        if not buffer:
            break
        csvFile.write(buffer)
    csvFile.close()


#-------------------------------------------------------------------
# Fonctions de traitement du CSV
#-------------------------------------------------------------------

def addComplements(currentCorpusMetadataExport):
    "Ajoute dans le corpus les deux champs complémentaires"
    currentCorpusMetadataExport.addComplement()
    currentCorpusMetadataExport.setComplementIntitule(NUMEROPAYS, "fr", "Pays")
    currentCorpusMetadataExport.setComplementIntitule(NUMEROPAYS, "en", "Country")
    currentCorpusMetadataExport.setComplementIntitule(NUMEROPAYS, "es", "País")
    currentCorpusMetadataExport.setComplementIntitule(NUMEROPAYS, "de", "Land")
    currentCorpusMetadataExport.addComplement()
    currentCorpusMetadataExport.setComplementIntitule(NUMEROTAGS, "fr", "Mots-clés libres")
    currentCorpusMetadataExport.setComplementIntitule(NUMEROTAGS, "en", "Tags")
    currentCorpusMetadataExport.setComplementIntitule(NUMEROTAGS, "es", "Tags")
    currentCorpusMetadataExport.setComplementIntitule(NUMEROTAGS, "de", "Tags")
    
def readCsv(ficheType, corpus):
    "Lit le fichier CSV en ne retenant que les fiches de type ficheType"
    with open(CSVFILE, "r", encoding="cp1252", newline='') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=';')
        first = True
        for row in csvreader:
            if not first:
                if len(row) > 18:
                    if row[18] == "published" and row[4] == ficheType:
                        parseRow(row, corpus)
            else:
                first = False

def parseRow(row, corpus):
    "Transforme une ligne en fiche"
    ficheId = row[7]
    ficheExport = scrutariDataExport.newFiche(ficheId)
    lang = row[5]
    ficheExport.setLang(lang)
    ficheExport.setTitre(row[0])
    ficheExport.setSoustitre(row[1])
    ficheExport.setHref(row[17])
    ficheExport.setDate(checkDate(row[13]))
    ficheExport.addComplement(NUMEROPAYS, row[10])
    ficheExport.addComplement(NUMEROTAGS, row[12])
    addAuthors(ficheExport, row[14])
    addAuthors(ficheExport, row[16])
    categorie = checkMotcle(row[8])
    if categorie:
        categorieId = categoriesThesaurus.getMotcleId(lang, categorie)
        if categorieId:
            scrutariDataExport.addIndexation(corpus, ficheId, "categories", categorieId, 1)
    souscategorie = checkMotcle(row[9])
    if souscategorie:
        souscategorieId = souscategoriesThesaurus.getMotcleId(lang, souscategorie)
        if souscategorieId:
            scrutariDataExport.addIndexation(corpus, ficheId, "souscategories", souscategorieId, 1)

def checkDate(value):
    "Transforme la valeur de la date au format ISO"
    idx1 = value.find('-')
    idx2 = value.find('-', idx1 + 1)
    return "20" + value[idx2 + 1:] + "-" + value[idx1 + 1: idx2] + "-" + value[0: idx1]

def checkMotcle(value):
    "Ne retient pas le mot-clé « Autre »"
    if value == 'Autre':
        return ""
    if value == 'Other':
        return ""
    if value == 'Andere':
            return "";
    if value == 'Otro':
        return ""
    return value

def addAuthors(ficheExport, value):
    "Ajoute les auteurs dans l'attribut sct:authors"
    auteurs = value.split(',')
    for auteur in auteurs:
        ficheExport.addAttributeValue("sct", "authors", auteur)


#-------------------------------------------------------------------
# Procédure principale
#-------------------------------------------------------------------

#Téléchargement du csv
csvDownload()

#Construction du thésaurus
categoriesThesaurus = ThesaurusBuffer(DESTINATION_DIRECTORY + "thesaurus-categories.xml")
souscategoriesThesaurus = ThesaurusBuffer(DESTINATION_DIRECTORY + "thesaurus-souscategories.xml")

#Départ de l'exportation
scrutariDataPath = DESTINATION_DIRECTORY + "handiplanet.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#Initialisation de la base Handiplanet
baseMetadataExport = scrutariDataExport.startExport()
baseMetadataExport.setAuthority("5e0ed927-f047-4c61-b618-2f78561a9480")
baseMetadataExport.setBaseName("handiplanet")
baseMetadataExport.setBaseIcon("http://static.scrutari.net/handiplanet.png")
baseMetadataExport.setIntitule(INTITULE_SHORT, "fr", "Handiplanet Échanges")
baseMetadataExport.setIntitule(INTITULE_SHORT, "en", "Handiplanet Exchanges")
baseMetadataExport.setIntitule(INTITULE_SHORT, "de", "Handiplanet Austauch")
baseMetadataExport.setIntitule(INTITULE_SHORT, "es", "Intercambios Handiplanet")
baseMetadataExport.setIntitule(INTITULE_LONG, "fr", "Handiplanet Échanges. Partage d'expérience sur le handicap mental et psychique")
baseMetadataExport.setIntitule(INTITULE_LONG, "en", "Handiplanet Exchanges. Practice and know-how sharing on mental and psychical handicap")
baseMetadataExport.setIntitule(INTITULE_LONG, "de", "Handiplanet Austauch")
baseMetadataExport.setIntitule(INTITULE_LONG, "es", "Intercambios Handiplanet")
baseMetadataExport.addLangUI("fr")
baseMetadataExport.addLangUI("en")
baseMetadataExport.addLangUI("de")
baseMetadataExport.addLangUI("es")

#Création du corpus Expérience
corpusMetadataExport = scrutariDataExport.newCorpus("experience");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Fiches d'expérience")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "en","Experience Index Cards")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "es","Fichas de experienca")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "de","Erfahrungskarte")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Fiche d'expérience n°")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "en", "Experience Index Cards #")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "es", "Ficha de experienca n°")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "de", "Erfahrungskarte #")
addComplements(corpusMetadataExport)
readCsv("FicheExperience", "experience")

#Création du corpus Témoignage
corpusMetadataExport = scrutariDataExport.newCorpus("temoignage");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Témoignages")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "en","Testimonies")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "es","Testimonios")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "de","Zeugenaussagen")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Témoignage n°")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "en", "Testimony #")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "es", "Testimonio n°")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "de", "Zeugenaussage #")
addComplements(corpusMetadataExport)
readCsv("Temoignage", "temoignage")

#Création du thésaurus Categories
thesaurusMetadataExport = scrutariDataExport.newThesaurus("categories")
thesaurusMetadataExport.setIntitule(INTITULE_THESAURUS, "fr","Catégories")
thesaurusMetadataExport.setIntitule(INTITULE_THESAURUS, "en","Categories")
thesaurusMetadataExport.setIntitule(INTITULE_THESAURUS, "es","Categorias")
thesaurusMetadataExport.setIntitule(INTITULE_THESAURUS, "de","Kategorien")
categoriesThesaurus.exportMotscles(scrutariDataExport)

#Création du thésaurus Sous-categories
thesaurusMetadataExport = scrutariDataExport.newThesaurus("souscategories")
thesaurusMetadataExport.setIntitule(INTITULE_THESAURUS, "fr","Sous-catégories")
thesaurusMetadataExport.setIntitule(INTITULE_THESAURUS, "en","Sub categories")
thesaurusMetadataExport.setIntitule(INTITULE_THESAURUS, "es","Subcategorias")
thesaurusMetadataExport.setIntitule(INTITULE_THESAURUS, "de","Unterkategorien")
souscategoriesThesaurus.exportMotscles(scrutariDataExport)


#Fin de l'export
scrutariDataExport.endExport()
scrutariDataFile.close()

#Renommage du fichier temporaire
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#Création du fichier d'information avec la date du jour
scrutariInfoFile = open(DESTINATION_DIRECTORY + "handiplanet.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "handiplanet.scrutari-data.xml")
scrutariInfoFile.close()
