#!/usr/bin/env python3
#-------------------------------------------------------------------
# https://www.cetri.be
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *
import pycurl
from io import BytesIO

#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
RACINE_SITE="https://www.cetri.be/"

#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------


def checkCategory(category):
    currentUrl =  category
    while(True):
        htmlContent = getContent(RACINE_SITE + currentUrl)
        soup = BeautifulSoup(htmlContent,"lxml")
        contentDiv = soup.find("div", class_ ="content")
        nextLink = contentDiv.find("a", rel ="next")
        checkList(contentDiv)
        if nextLink:
            currentUrl = nextLink["href"]
        else:
            break

def checkList(contentDiv):
    divList = contentDiv.find_all("div", class_="magazine-news-img")
    for div in divList:
        link = div.find("a")
        checkArticle(link["href"])
        
def checkArticle(href):
    url = RACINE_SITE + href
    htmlContent = getContent(url)
    soup = BeautifulSoup(htmlContent,"lxml") 
    contentDiv = soup.find("div", class_ ="content")
    titreH2  = contentDiv.find("h2")
    ficheId = getFicheId(titreH2["class"])
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setLang("fr")
    ficheExport.setHref(url)
    ficheExport.setTitre(titreH2.get_text())
    contenu = contentDiv.find("div", class_="onglets_contenu")
    soustitreP = contenu.find("p")
    if soustitreP:
        ficheExport.setSoustitre(checkSoustitre(soustitreP.get_text()))
    rows = contenu.find_all("tr")
    for row in rows:
        tds = row.find_all("td")
        name = tds[0].get_text()
        if name == "Date":
            ficheExport.setDate(checkDate(tds[1].get_text()))
    authorList = contenu.find_all("span", class_="vcard")
    for author in authorList:
        ficheExport.addAttributeValue("sct","authors", author.get_text())

def checkDate(dateText):
    idx = dateText.find("/")
    if idx > 0:
        return dateText[(idx+1):] + "-" + dateText[0:idx]
    else:
        return dateText

def checkSoustitre(soustitre):
    idx = soustitre.find("\n")
    if idx > 0:
        soustitre = soustitre[0:idx]
    soustitreLen = len(soustitre)
    if soustitreLen < 150:
        return soustitre
    idx = soustitre.find(".", 150, soustitreLen)
    if idx > 0:
        return soustitre[0:(idx+1)]
    return soustitre

def getFicheId(classAttribute):
    for className in classAttribute:
        if className.startswith("article-titre-"):
            return className[14:]
    return ""    

def getContent(url):
    buffer = BytesIO()
    c = pycurl.Curl()
    c.setopt(c.URL, url)
    c.setopt(c.WRITEDATA, buffer)
    c.setopt(c.HTTPHEADER, ['Host: www.cetri.be', 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8', 'Accept-Language: fr,fr-FR;q=0.5'])
    c.perform()
    c.close()
    body = buffer.getvalue()
    return body.decode('UTF-8')

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding = "UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "cetri-publications.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding="UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_alternativessud
corpusMetadataExport = scrutariDataExport.newCorpus("alternativessud");
metadataWrapper.fillCorpus(corpusMetadataExport)

checkCategory("-alternatives-sud-151-")


#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "cetri-publications.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "cetri-publications.scrutari-data.xml")
scrutariInfoFile.close()
