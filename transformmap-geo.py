#!/usr/bin/env python3

#-------------------------------------------------------------------
# http://www.transformap.co
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re, json, yaml, csv
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
THESAURUS_CSV_FILE = sys.argv[3]
JSON_URL = "https://data.transformap.co/place/type_of_initiative"


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def loadJson():
    jsonResponse = urllib.request.urlopen(JSON_URL)
    jsonObj = json.loads(jsonResponse.read().decode("UTF-8"))
    for feature in jsonObj["features"]:
        properties = feature["properties"]
        coordinates = feature["geometry"]["coordinates"]
        ficheId = properties["_id"]
        ficheExport = scrutariDataExport.newFiche(ficheId)
        ficheExport.setTitre(properties["name"])
        ficheExport.setGeoloc(coordinates[1], coordinates[0])
        #ficheExport.setHref()
        if "identity" in properties:
            identities = properties["identity"].split(";")
            for identity in identities:
                scrutariDataExport.addIndexation("place", ficheId, "identity", identity, 1)
        if "contact:website" in properties:
            ficheExport.addAttributeValue("sct", "website", properties["contact:website"])
        if "description" in properties:
            ficheExport.setSoustitre(properties["description"])


#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding="UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#buffer
thesaurusBuffers = ThesaurusBuffers()
csvFile = open(THESAURUS_CSV_FILE, "r", encoding="UTF-8", newline='')
csvreader = csv.reader(csvFile, delimiter=',')
thesaurusBuffers.readCsv(csvreader)
csvFile.close()

#start
scrutariDataPath = DESTINATION_DIRECTORY + "transformmap-geo.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding="UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_place
corpusMetadataExport = scrutariDataExport.newCorpus("place");
metadataWrapper.fillCorpus(corpusMetadataExport)

loadJson()

#thesaurus_domain
thesaurusMetadataExport = scrutariDataExport.newThesaurus("identity");
metadataWrapper.fillThesaurus(thesaurusMetadataExport)
thesaurusBuffers.fill("identity", scrutariDataExport)
    
#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "transformmap-geo.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "transformmap-geo.scrutari-data.xml")
scrutariInfoFile.close()
