#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://transiscope.org
#-------------------------------------------------------------------

import sys, shutil, urllib.request, json, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
CONF_JSON_FILE="/home/vic/en_cours/transiscope/gogocartojs-conf.json"
ELEMENTS_JSON_FILE="/home/vic/en_cours/transiscope/elements.json"
CONF_JSON_URL="https://transiscope.gogocarto.fr/api/gogocartojs-conf.json"
ELEMENTS_JSON_URL="https://transiscope.gogocarto.fr/api/elements"
ROOT_URL="https://transiscope.org/carte-des-alternatives/#/fiche/id/"

indexationMap = {}
domaineArray = []
sourceArray = []
parentMap = {}



#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def scanConfLocal():
  with open(CONF_JSON_FILE, encoding="UTF-8") as data_file:    
    jsonData = json.load(data_file)
    scanConfJson(jsonData)
    
def scanConfUrl():
  jsonResponse = urllib.request.urlopen(CONF_JSON_URL)
  jsonData = json.loads(jsonResponse.read().decode("UTF-8"))
  scanConfJson(jsonData)

def scanConfJson(jsonData):
  taxonomyArray = jsonData["data"]["taxonomy"]
  for taxonomy in taxonomyArray:
    name= taxonomy["name"]
    thesaurusName = ""
    parentId = False
    if name == "Domaines":
      thesaurusName = "domaine"
      thesaurusArray = domaineArray
    elif name == "Sources":
      thesaurusName = "source"
      thesaurusArray = sourceArray
      parentId = "none"
    if len(thesaurusName) > 0:
      scanCategories(thesaurusName, taxonomy["options"], thesaurusArray, parentId)
      
def scanCategories(thesaurusName, array, thesaurusArray, parentId):
  for category in array:
    categoryId = category["id"]
    if not parentId:
      parentId = categoryId
    if categoryId == "56" or categoryId == "63":
      continue
    indexationMap[categoryId] = thesaurusName
    parentMap[categoryId] = parentId
    thesaurusArray.append(category)
    if "subcategories" in category:
      scanCategories(thesaurusName, category["subcategories"][0]["options"], thesaurusArray, parentId)

def scanElementsLocal():
  with open(ELEMENTS_JSON_FILE, encoding="UTF-8") as data_file:    
    jsonData = json.load(data_file)
    scanElementsJson(jsonData)
    
def scanElementsUrl():
  jsonResponse = urllib.request.urlopen(ELEMENTS_JSON_URL)
  jsonData = json.loads(jsonResponse.read().decode("UTF-8"))
  scanElementsJson(jsonData)

def scanElementsJson(jsonData):
    for feature in jsonData["data"]:
        addFeature(feature)
        
def addFeature(feature):
    coordinates = feature["geo"]
    ficheId = str(feature["id"])
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setLang("fr")
    ficheExport.setGeoloc(coordinates["latitude"], coordinates["longitude"])
    ficheExport.setTitre(feature["name"])
    if "abstract" in feature:
        ficheExport.setSoustitre(feature["abstract"])
    elif "descriptionMore" in feature:
        ficheExport.setSoustitre(cutSoustitre(feature["descriptionMore"]))
    if "showUrl" in feature:
        ficheExport.setHref(feature["showUrl"])
    else:
        ficheExport.setHref(ROOT_URL + ficheId + "/")
    if "website" in feature:
        ficheExport.addAttributeValue("sct", "website", feature["website"])
    if "address" in feature:
        address = feature["address"]
        if "addressLocality" in address:
            ficheExport.addAttributeValue("geo", "city", address["addressLocality"])
    if "categoriesFull" in feature:
        for category in feature["categoriesFull"]:
          motcleId = str(category["id"])
          if motcleId in indexationMap:
            thesaurusName = indexationMap[motcleId]
            scrutariDataExport.addIndexation("alternative", ficheId, thesaurusName, motcleId, 1)
            parentId = parentMap[motcleId]
            if parentId != motcleId and parentId != "none":
              scrutariDataExport.addIndexation("alternative", ficheId, thesaurusName, parentId, 1)

def cutSoustitre(summary):
    """Découpe le sous-titre pour qu'il ne soit pas trop volumineux,
    ne conserve que la première phrases moins ce qui peut être entre parenthèse
    """
    point = 0
    soustitre = ""
    parenthese = False
    previousSpace = False
    for char in summary:
        if parenthese:
            if char == ')':
                parenthese = False
        elif char == '.':
            soustitre = soustitre + char
            point = point + 1
        elif char == '(':
            parenthese = True
            previousSpace = False
        elif char == ' ':
            previousSpace = True
        else:
            if previousSpace:
                soustitre = soustitre + " " + char
                previousSpace = False
            else:
                soustitre = soustitre + char
        if point == 1:
                break
    return soustitre

def populateThesaurus(thesaurusArray):
  for motcle in thesaurusArray:
    motcleExport = scrutariDataExport.newMotcle(motcle["id"])
    motcleExport.setLibelle("fr", motcle["name"])

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#chargement de la taxonomie
#scanConfLocal()
scanConfUrl()

#wrapper
yamlFile = open(YAML_FILE, encoding="UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "transiscope-geo.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_acteur
corpusMetadataExport = scrutariDataExport.newCorpus("alternative");
metadataWrapper.fillCorpus(corpusMetadataExport)

#chargement des éléments
#scanElementsLocal()
scanElementsUrl()
    
#thesaurus_domaine
thesaurusMetadataExport = scrutariDataExport.newThesaurus("domaine");
metadataWrapper.fillThesaurus(thesaurusMetadataExport)

populateThesaurus(domaineArray)

#thesaurus_source
thesaurusMetadataExport = scrutariDataExport.newThesaurus("source");
metadataWrapper.fillThesaurus(thesaurusMetadataExport)

populateThesaurus(sourceArray)


#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "transiscope-geo.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "transiscope-geo.scrutari-data.xml")
scrutariInfoFile.close()
