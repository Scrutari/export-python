#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://saw-b.be
#-------------------------------------------------------------------

import json
from scrutaridataexport import *
from scrutaridataexport_utils import *
from wordpressapi import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
NAME = "saw_b"
HOST = "saw-b.be"
PER_PAGE = "100"
PUBLICATION_URL = "https://saw-b.be/wp-json/wp/v2/publication?per_page=" + PER_PAGE
SHORT_URL="https://saw-b.be/?p="
ANNEE_URL = "https://saw-b.be/wp-json/wp/v2/anne_publication?per_page=" + PER_PAGE
MOTSCLES_URL = "https://saw-b.be/wp-json/wp/v2/mots_cles?per_page=" + PER_PAGE


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def readPublicationPage(jsonResponse):
    jsonData = json.loads(jsonResponse.read().decode("UTF-8"))
    for item in jsonData:
        ficheId = item["id"]
        ficheExport = scrutariDataExport.newFiche(ficheId)
        ficheExport.setTitre(wp_getText(item, "title"))
        ficheExport.setLang("fr")
        ficheExport.setHref(SHORT_URL + str(ficheId))
        ficheExport.setDate(getDate(item))
        ficheExport.setSoustitre(wp_getFirstParagraph(item, "content"))
        authors = getAuthors(item)
        if len(authors) > 0:
            ficheExport.addAttributeValue("sct", "authors", authors)
        motcleArray = item["mots_cles"]
        for motcleId in motcleArray:
            motcleId = str(motcleId)
            if motcleBuffer.containsMotcle(motcleId):
                scrutariDataExport.addIndexation("publication", ficheId, "motcle", motcleId, 1)
        
def getDate(item):
    for anneeId in item["anne_publication"]:
        if anneeId in anneeMap:
            return anneeMap[anneeId]
    return wp_getDate(item)

def getAuthors(item):
    fieldValue = item["content"]
    if not fieldValue:
        return ""
    content = fieldValue["rendered"]
    if not content:
        return ""
    search = re.search("<strong>par (.*)</strong>", content)
    if not search:
        return ""
    return search.group(1)            


#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

anneeMap = {}
wp_populateTaxonomyMap(anneeMap, ANNEE_URL, HOST)

#start
scrutariDataFile = utils_openScrutariDataFile(DESTINATION_DIRECTORY, NAME)
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))
metadataWrapper = utils_initMetadataWrapper(YAML_FILE)

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#thesaurusBuffer
motcleBuffer = ThesaurusBuffer()
wp_populateThesaurus(motcleBuffer, MOTSCLES_URL, "fr", HOST)

#corpus_publication
corpusMetadataExport = scrutariDataExport.newCorpus("publication")
metadataWrapper.fillCorpus(corpusMetadataExport)
wp_readPages(PUBLICATION_URL, HOST, readPublicationPage)

#thesaurus_motcle
thesaurusMetadataExport = scrutariDataExport.newThesaurus("motcle");
metadataWrapper.fillThesaurus(thesaurusMetadataExport)
motcleBuffer.exportMotscles(scrutariDataExport)

#end
scrutariDataExport.endExport()
utils_closeScrutariDataFile(DESTINATION_DIRECTORY, NAME, scrutariDataFile)
utils_writeScrutariInfoFile(DESTINATION_DIRECTORY, NAME)
