#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://www.communecter.org
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re, json, yaml, unicodedata
from bs4 import BeautifulSoup
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
JSON_ROOT = "https://www.communecter.org/api/"


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def loadList(relativePath):
    index = 0
    while(True):
        jsonResponse = urllib.request.urlopen(JSON_ROOT + relativePath + "/limit/500/index/" + str(index))
        index = index + 500
        jsonObj = json.loads(jsonResponse.read().decode("UTF-8"))
        if len(jsonObj["entities"]) == 0:
            break;
        for key, value in jsonObj["entities"].items():
            apiLink = value["url"]["api"]
            loadEntry(key, apiLink)

def loadEntry(entryId, apiLink):
    entryJsonResponse = urllib.request.urlopen(apiLink)
    entryJsonObj = json.loads(entryJsonResponse.read().decode("UTF-8"))
    entryDict = entryJsonObj["entities"][entryId]
    if "geo" in entryDict:
        geo = entryDict["geo"]
        if communecter in entryDict["url"]:
            ficheExport = scrutariDataExport.newFiche(entryId)
            ficheExport.setTitre(entryDict["name"])
            ficheExport.setGeoloc(geo["latitude"], geo["longitude"])
            ficheExport.setLang("fr")
            ficheExport.setHref(entryDict["url"]["communecter"])
            if "description" in entryDict:
                descriptionSoup = BeautifulSoup(entryDict["description"], "lxml")
                description = descriptionSoup.get_text()
                if len(description) > 1000:
                    description = description[0:1000]
                ficheExport.setSoustitre(description)
            if "tags" in entryDict:
                for tag in entryDict["tags"]:
                    ficheExport.addAttributeValue("sct", "tags", tag)
            if "website" in entryDict["url"]:
                website = entryDict["url"]["website"]
                if website.find("://") == -1:
                    website = "http://" + website
                ficheExport.addAttributeValue("sct", "website", website)
            if "address" in entryDict:
                if "addressLocality" in entryDict["address"]:
                    ficheExport.addAttributeValue("geo", "city", entryDict["address"]["addressLocality"])


#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding = "UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "communecter-geo.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_organization
corpusMetadataExport = scrutariDataExport.newCorpus("organization");
metadataWrapper.fillCorpus(corpusMetadataExport)

loadList("organization/get")

#corpus_project
corpusMetadataExport = scrutariDataExport.newCorpus("project");
metadataWrapper.fillCorpus(corpusMetadataExport)

loadList("project/get")

#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "communecter-geo.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "communecter-geo.scrutari-data.xml")
scrutariInfoFile.close()
