#!/usr/bin/env python3

#-------------------------------------------------------------------
# http://www.cerdd.org
#-------------------------------------------------------------------

import sys, shutil, urllib.request, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *

#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
ROOT="http://www.cerdd.org"
INDEX_URL="http://www.cerdd.org/Ressources/Ressources/(filtre)/cerdd_label_only"
LANG="fr"
PREFIX="node_id_"
PREFIX_LENGTH=len(PREFIX)

def checkIndex(url):
    htmlContent = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlContent,"lxml")
    maindiv = soup.find("div", id="resultat-ressources")
    if maindiv:
        linkLis = maindiv.find_all("li", class_="sommaire-objet")
        for linkLi in linkLis:
            link = linkLi.find("a", recursive=False)
            loadResource(link["href"])
        nextLink = maindiv.find("a", title="Page suivante")
        if nextLink:
            checkIndex(ROOT + nextLink["href"])
            
def loadResource(href):
    url = ROOT + href
    htmlContent = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlContent,"lxml")
    body = soup.find("body")
    ficheId = getFicheId(body)
    if ficheId:
        pageContent = body.find("section", class_="page-content")
        ficheExport = scrutariDataExport.newFiche(ficheId)
        ficheExport.setTitre(getTitre(pageContent))
        ficheExport.setLang(LANG)
        ficheExport.setHref(url)
        ficheExport.setDate(getDate(pageContent))
        ficheExport.setSoustitre(getSoustitre(pageContent))
        category = getCategory(body)
        if category:
            ficheExport.addAttributeValue("sct", "tags", category)
    
    
def getFicheId(body):
    for className in body["class"]:
        if className.startswith(PREFIX):
            return className[PREFIX_LENGTH:]
    return False

def getTitre(element):
    return element.find("h1").get_text()

def getSoustitre(element):
    div = element.find("div", class_="lead")
    if div:
        return div.get_text()
    return False

def getDate(element):
    div = element.find("div", class_="date")
    if div:
        p = div.find("p")
        if p:
            text = p.get_text()
            lastIndex = text.rfind(" ")
            if lastIndex:
                return text[lastIndex + 1:]
    return False

def getCategory(body):
    ol = body.find("ol", "breadcrumb")
    if ol:
        p = 0
        for li in ol.find_all("li"):
            p = p + 1
            if p == 3:
                link = li.find("a")
                if link:
                    return link["title"]
    return False
    
    

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------


#wrapper
yamlFile = open(YAML_FILE, encoding="UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "cerdd.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_post
corpusMetadataExport = scrutariDataExport.newCorpus("ressource");
metadataWrapper.fillCorpus(corpusMetadataExport)

checkIndex(INDEX_URL)


#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "cerdd.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "cerdd.scrutari-data.xml")
scrutariInfoFile.close()
