#!/usr/bin/env python3

#-------------------------------------------------------------------
# http://redeconvergir.net/
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re, json, yaml, csv
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
THESAURUS_CSV_FILE = sys.argv[3]
JSON_URL = "http://redeconvergir.net/api/v1/initiatives"
ROOT_URL = "http://redeconvergir.net/iniciativas/"


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def loadJson():
    jsonResponse = urllib.request.urlopen(JSON_URL)
    jsonData = json.loads(jsonResponse.read().decode("UTF-8"))
    for item in jsonData:
        ficheId = item["id"]
        ficheExport = scrutariDataExport.newFiche(ficheId)
        ficheExport.setTitre(item["name"])
        coordinates = item["coordinates"]
        ficheExport.setGeoloc(coordinates[0], coordinates[1])
        ficheExport.setHref(ROOT_URL + item["slug"])
        description = item["description"]
        if len(description) > 1000:
            description = description[0:1000]
        ficheExport.setSoustitre(description)
        url = item["url"]
        if len(url) > 0:
            ficheExport.addAttributeValue("sct", "website", url)
        typeId = item["typeId"]
        if len(typeId) > 0:
            if typeId == "type_999_other":
                ficheExport.addAttributeValue("sct", "tags", item["typeOther"])
            else:
                scrutariDataExport.addIndexation("initiative", ficheId, "type", typeId, 1)
        for domain in item["domains"]:
            if domain == "domain_999_other":
                tags = item["domainsOther"].split(";")
                for tag in tags:
                    ficheExport.addAttributeValue("sct", "tag", tag)
            else:
                scrutariDataExport.addIndexation("initiative", ficheId, "domain", domain, 1)

    
#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding="UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#buffer
thesaurusBuffers = ThesaurusBuffers()
csvFile = open(THESAURUS_CSV_FILE, "r", encoding="UTF-8", newline='')
csvreader = csv.reader(csvFile, delimiter=',')
thesaurusBuffers.readCsv(csvreader)
csvFile.close()

#start
scrutariDataPath = DESTINATION_DIRECTORY + "redeconvergir-geo.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding="UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_initiative
corpusMetadataExport = scrutariDataExport.newCorpus("initiative")
metadataWrapper.fillCorpus(corpusMetadataExport)
loadJson()

#thesaurus_type
thesaurusMetadataExport = scrutariDataExport.newThesaurus("type");
metadataWrapper.fillThesaurus(thesaurusMetadataExport)
thesaurusBuffers.fill("type", scrutariDataExport)

#thesaurus_domain
thesaurusMetadataExport = scrutariDataExport.newThesaurus("domain");
metadataWrapper.fillThesaurus(thesaurusMetadataExport)
thesaurusBuffers.fill("domain", scrutariDataExport)
    
#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "redeconvergir-geo.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "redeconvergir-geo.scrutari-data.xml")
scrutariInfoFile.close()