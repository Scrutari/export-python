#!/usr/bin/env python3
#-------------------------------------------------------------------
# Script de génération du fichier ScrutariData pour le site http://www.recma.org
# Ce script lit les pages de chaque revue http://www.recma.org/articles?field_numrevue_value=331
# y récupère les adresses de chaque page d'article et lit ensuite les pages d'article une par une
# pour récupérer les métadonnées des fiches et les mots-clés
#-------------------------------------------------------------------


#-------------------------------------------------------------------
# Ce script utilise le module BeautifulSoup http://www.crummy.com/software/BeautifulSoup/
# ainsi que le module lxml (voir l'appel de BeautifulSoup(htmlContent, "lxml"))
# Les autres modules utilisés sont standards
# scrutaridataexport correspond au contenu du fichier scrutaridataexport.py
#-------------------------------------------------------------------

import sys, shutil, urllib.request, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *


#-------------------------------------------------------------------
# Ce script a un deux arguments obligatoires : le chemin du répertoire de destination
# où sera enregistré les fichiers recma.scrutari-data.xml et recma.scrutari-info.xml  et le fichier metadata utilisé
# REVUE_START est le premier numéro de revue traité (272 (année 1999) en l'occurrence, les fiches
# les fiches des revues précédentes ne sont pas remplies),
# REVUE_END le dernier
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
RECMA = "http://www.recma.org"
REVUE_START = 272
REVUE_END = 358


#-------------------------------------------------------------------
# Fonctions de traitement du HTML des pages du site Recma
#-------------------------------------------------------------------

def checkRevuePage(htmlContent, lang):
    "Lecture d'une page de présentation de la revue pour récupérer les articles"
    revue = BeautifulSoup(htmlContent, "lxml")
    nodeList = revue.select("article.node.node-teaser")
    for div in nodeList:
        ficheId = div["id"][5:]
        h2 = div.find("h2")
        if h2:
            a = h2.find("a")
            href = RECMA +  a["href"]
            response = urllib.request.urlopen(href)
            checkArticlePage(response.read(), ficheId, href, lang)

def checkArticlePage(htmlContent, ficheId, href, lang):
    "Lecture d'une page Article avec la création d'une fiche"
    ficheExport = scrutariDataExport.newFiche(ficheId)
    article = BeautifulSoup(htmlContent, "lxml")
    titre = article.find("h1").get_text()
    annee = article.find("span", class_="date-display-single").get_text()
    ficheExport.setTitre(titre)
    ficheExport.setDate(annee)
    ficheExport.setLang(lang)
    ficheExport.setHref(href)
    bodyDiv = article.find("div", class_="field-name-body")
    if bodyDiv:
        ficheExport.setSoustitre(cutSoustitre(bodyDiv.get_text()))
    auteursDiv = article.find("div", class_="field-name-field-auteur")
    if auteursDiv:
        auteurList = auteursDiv.find_all("div", class_="field-item")
        for auteur in auteurList:
            valueList = splitAuteur(auteur.get_text())
            for value in valueList:
                ficheExport.addAttributeValue("sct", "authors", value.strip())
    numrevueDiv = article.find("div", class_="field-name-field-numrevue")
    if numrevueDiv:
        numrevue = numrevueDiv.find("a").get_text()
        ficheExport.addComplement(1, numrevue)
    motsclesDiv = article.find("div", class_="field-name-taxonomy-vocabulary-3")
    if motsclesDiv:
        for taxonomyLink in motsclesDiv.find_all("a"):
            text = taxonomyLink.get_text().strip()
            taxonomyHref = taxonomyLink["href"]
            motcleId = taxonomyHref[taxonomyHref.rfind("/") + 1:]
            scrutariDataExport.addIndexation("article", ficheId, "motcle", motcleId, 1)
            motcleThesaurus.checkMotcle(motcleId, "fr", text)
        
def splitAuteur(auteurs):
    "Décompose un champ auteur suivant la virgule et « et »"
    valueList = auteurs.split(",")
    total = len(valueList)
    if total > 0:
        last = valueList[total - 1]
        etIndex = last.find(" et ")
        if etIndex > 0:
            last1 = last[0:etIndex]
            last2 = last[etIndex + 4:]
            valueList[total - 1] = last1
            valueList.append(last2)
    return valueList

def cutSoustitre(summary):
    """Découpe le sous-titre pour qu'il ne soit pas trop volumineux,
    ne conserve que les deux premières phrases moins ce qui peut être entre parenthèse
    """
    point = 0
    soustitre = ""
    parenthese = False
    previousSpace = False
    for char in summary:
        if parenthese:
            if char == ')':
                parenthese = False
        elif char == '.':
            soustitre = soustitre + char
            point = point + 1
        elif char == '(':
            parenthese = True
            previousSpace = False
        elif char == ' ':
            previousSpace = True
        else:
            if previousSpace:
                soustitre = soustitre + " " + char
                previousSpace = False
            else:
                soustitre = soustitre + char
        if point == 2:
                break
    return soustitre


#-------------------------------------------------------------------
# Procédure principale
#-------------------------------------------------------------------

#Construction du thésaurus
motcleThesaurus = ThesaurusBuffer()

#wrapper
yamlFile = open(YAML_FILE, encoding="UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)


#Départ de l'exportation
scrutariDataPath = DESTINATION_DIRECTORY + "recma.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_article
corpusMetadataExport = scrutariDataExport.newCorpus("article");
metadataWrapper.fillCorpus(corpusMetadataExport)
numero = corpusMetadataExport.addComplement()
corpusMetadataExport.setComplementIntitule(numero, "fr", "Revue n°")

#fiche = open("/home/vic/en_cours/socioeco/article.html", "r", encoding = "UTF-8")
#checkArticlePage(fiche.read(), "4983", "http://www.recma.org/essai")
#fiche.close()

#Boucle sur les pages Revues
for numrevue in range(REVUE_START,REVUE_END + 1):
    response = urllib.request.urlopen(RECMA + "/articles?field_numrevue_value=" + str(numrevue))
    checkRevuePage(response.read(), "fr")
    
#Cas particulier du hors-série en anglais
horsserie =  urllib.request.urlopen(RECMA + "/articles?field_numrevue_value=0")
checkRevuePage(horsserie.read(), "en")

#thesaurus_motcle
thesaurusMetadataExport = scrutariDataExport.newThesaurus("motcle");
metadataWrapper.fillThesaurus(thesaurusMetadataExport)
motcleThesaurus.exportMotscles(scrutariDataExport)

#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "recma.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "recma.scrutari-data.xml")
scrutariInfoFile.close()
    

