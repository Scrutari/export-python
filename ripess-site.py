#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://www.ripess.org/
#-------------------------------------------------------------------

import json
from bs4 import BeautifulSoup
from scrutaridataexport import *
from scrutaridataexport_utils import *
from wordpressapi import *

#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
NAME = "ripess-site"
HOST = "ripess.org"
PER_PAGE = "100"
MEDIA_URL = "https://www.ripess.org/wp-json/wp/v2/media?per_page=" + PER_PAGE
VIDEO_FR_URL = "https://www.ripess.org/wp-json/wp/v2/posts?categories=13&per_page=" + PER_PAGE
VIDEO_EN_URL = "https://www.ripess.org/wp-json/wp/v2/posts?categories=222&lang=en&per_page=" + PER_PAGE
VIDEO_ES_URL = "https://www.ripess.org/wp-json/wp/v2/posts?categories=202&lang=es&per_page=" + PER_PAGE
SHORT_URL = "https://www.ripess.org/?p="
ROOT_SITE = "http://www.ripess.org/"


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def readMediaPage(jsonResponse):
    jsonData = utils_toJson(jsonResponse)
    for item in jsonData:
        if "media_details" in item:
            if "sizes" in item["media_details"]:
                if "thumbnail" in item["media_details"]["sizes"]:
                    thumbnailMap[item["id"]] =  item["media_details"]["sizes"]["thumbnail"]["source_url"]

def readFr(jsonResponse):
    readPostPage(jsonResponse, "fr")
    
def readEn(jsonResponse):
    readPostPage(jsonResponse, "en")
    
def readEs(jsonResponse):
    readPostPage(jsonResponse, "es")

def readPostPage(jsonResponse, lang):
    jsonData = utils_toJson(jsonResponse)
    for item in jsonData:
        ficheId = item["id"]
        ficheExport = scrutariDataExport.newFiche(ficheId)
        ficheExport.setTitre(wp_getText(item, "title"))
        ficheExport.setHref(SHORT_URL + str(ficheId))
        ficheExport.setLang(lang)
        ficheExport.setSoustitre(wp_getFirstParagraph(item, "excerpt"))
        ficheExport.setDate(wp_getDate(item))
        if "featured_media" in item:
            mediaId = item["featured_media"]
            if mediaId in thumbnailMap:
                ficheExport.addAttributeValue("sct", "thumbnail", thumbnailMap[mediaId])         


#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#init

thumbnailMap = dict()

wp_readPages(MEDIA_URL, HOST, readMediaPage)

#start
scrutariDataFile = utils_openScrutariDataFile(DESTINATION_DIRECTORY, NAME)
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))
metadataWrapper = utils_initMetadataWrapper(YAML_FILE)

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_video
corpusMetadataExport = scrutariDataExport.newCorpus("video");
metadataWrapper.fillCorpus(corpusMetadataExport)
wp_readPages(VIDEO_FR_URL, HOST, readFr)
wp_readPages(VIDEO_EN_URL, HOST, readEn)
wp_readPages(VIDEO_ES_URL, HOST, readEs)


#end
scrutariDataExport.endExport()
utils_closeScrutariDataFile(DESTINATION_DIRECTORY, NAME, scrutariDataFile)
utils_writeScrutariInfoFile(DESTINATION_DIRECTORY, NAME)
