#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://attacmaroc.org/fr/
#-------------------------------------------------------------------

from bs4 import BeautifulSoup
from scrutaridataexport import *
from scrutaridataexport_utils import *
from wordpressapi import *

#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
NAME = "attac-maroc-fr"
HOST = "attacmaroc.org"
PER_PAGE = "100"
RACINE_SITE="https://attacmaroc.org/fr/"
EXCLUDE={486}
INCLUDE=[459, 37, 497]
NEOLIBERALISME_URL = "https://attacmaroc.org/fr/wp-json/wp/v2/posts?categories=5&per_page=" + PER_PAGE
POST_URL = "https://attacmaroc.org/fr/wp-json/wp/v2/posts/"
SHORT_URL = "https://attacmaroc.org/fr/?p="


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

ficheIdSet = set()
ficheIdSet.update(EXCLUDE)

def readPostPage(jsonResponse):
    jsonData = utils_toJson(jsonResponse)
    for item in jsonData:
        readItem(item)
        
def includePosts():
    for postId in INCLUDE:
        try:
            item = utils_toJson(utils_urlopen(POST_URL + str(postId), HOST))
            readItem(item)
        except:
            continue
        
        
def readItem(item):
    ficheId = item["id"]
    if ficheId in ficheIdSet:
        return
    else:
        ficheIdSet.add(ficheId)
    ficheIdSet.add(ficheId)
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setTitre(wp_getText(item, "title"))
    ficheExport.setHref(SHORT_URL + str(ficheId))
    ficheExport.setLang("fr")
    ficheExport.setDate(wp_getDate(item))
    ficheExport.setSoustitre(wp_getFirstParagraph(item, "excerpt"))


#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#start
scrutariDataFile = utils_openScrutariDataFile(DESTINATION_DIRECTORY, NAME)
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))
metadataWrapper = utils_initMetadataWrapper(YAML_FILE)

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_ressource_fr
corpusMetadataExport = scrutariDataExport.newCorpus("ressource_fr")
wp_readPages(NEOLIBERALISME_URL, HOST, readPostPage)
includePosts()

#end
scrutariDataExport.endExport()
utils_closeScrutariDataFile(DESTINATION_DIRECTORY, NAME, scrutariDataFile)
utils_writeScrutariInfoFile(DESTINATION_DIRECTORY, NAME)
