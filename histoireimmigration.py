#!/usr/bin/env python3
#-------------------------------------------------------------------
# Script de génération du fichier ScrutariData pour le fichier CSV
# de la médiathèque Abdelmalek Sayab (http://histoire-immigration.fr/)
# 
# Ce script ne s'occupe pas du téléchargement du fichier CSV
#-------------------------------------------------------------------

#-------------------------------------------------------------------
# Les modules utilisés sont standards
# scrutaridataexport correspond au contenu du fichier scrutaridataexport.py
#-------------------------------------------------------------------

import sys, shutil, csv
from scrutaridataexport import *


#-------------------------------------------------------------------
# Ce script a un seul argument obligatoire : le chemin du répertoire de destination
# où sera enregistré les fichiers histoireimmigration_ressources.scrutari-data.xml et histoireimmigration_ressources.scrutari-info.xml
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
CSVFILE = DESTINATION_DIRECTORY + "videos.csv"

#-------------------------------------------------------------------
# Fonctions de traitement
#-------------------------------------------------------------------

def parseRow(row):
    ficheId = row[0]
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setLang("fr")
    ficheExport.setTitre(row[1])
    ficheExport.setHref("http://catalogue.histoire-immigration.fr/cgi-bin/koha/opac-detail.pl?biblionumber=" + ficheId)
    ficheExport.setDate(row[8])
    ficheExport.addComplement(1, row[9])
    nom1 = row[2]
    if len(nom1) > 0:
        ficheExport.addAttributeValue("sct", "authors", row[3] + " " + nom1)
    nom2 = row[5]
    if len(nom2) > 0:
        ficheExport.addAttributeValue("sct", "authors", row[6] + " " + nom2)
    ficheExport.setSoustitre(row[10])
                
                
#-------------------------------------------------------------------
# Procédure principale
#-------------------------------------------------------------------


#Départ de l'exportation
scrutariDataPath = DESTINATION_DIRECTORY + "histoireimmigration.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#Initialisation de la base d'Alimenterre
baseMetadataExport = scrutariDataExport.startExport()
baseMetadataExport.setAuthority("cc407f4c-0984-47b5-9afb-07f16c0e4fb5")
baseMetadataExport.setBaseName("histoireimmigration")
baseMetadataExport.setBaseIcon("http://static.scrutari.net/histoireimmigration.png")
baseMetadataExport.setIntitule(INTITULE_SHORT, "fr", "Médiathèque A. Sayad")
baseMetadataExport.setIntitule(INTITULE_LONG, "fr", "Catalogue de la médiathèque Abdelmalek Sayad")

#Création du corpus Ressources
corpusMetadataExport = scrutariDataExport.newCorpus("video");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Images animées")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Images animées n°")
numero = corpusMetadataExport.addComplement()
corpusMetadataExport.setComplementIntitule(numero, "fr", "Producteurs")


with open(CSVFILE, "r", encoding="UTF-8", newline='') as csvfile:
    csvreader = csv.reader(csvfile, delimiter=';')
    first = True
    for row in csvreader:
        if not first:
            parseRow(row)
        else:
            first = False



#Fin de l'export
scrutariDataExport.endExport()
scrutariDataFile.close()

#Renommage du fichier temporaire
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#Création du fichier d'information avec la date du jour
scrutariInfoFile = open(DESTINATION_DIRECTORY + "histoireimmigration.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "histoireimmigration.scrutari-data.xml")
scrutariInfoFile.close()

            
