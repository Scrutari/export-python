#!/usr/bin/env python3

#-------------------------------------------------------------------
# http://www.transformap.co
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re, json, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
JSON_URL = "https://www.ritimo.org/spip.php?page=gis_json&objets=acteurs&limit=500"
URL_ROOT = "https://www.ritimo.org/"


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def loadJson():
    jsonResponse = urllib.request.urlopen(JSON_URL)
    jsonObj = json.loads(jsonResponse.read().decode("UTF-8"))
    for feature in jsonObj["features"]:
        ficheId = feature["id"]
        properties = feature["properties"]
        coordinates = feature["geometry"]["coordinates"]
        ficheExport = scrutariDataExport.newFiche(ficheId)
        titleSoup = BeautifulSoup(properties["title"],"lxml")
        link = titleSoup.find("a")
        ficheExport.setTitre(link.get_text())
        ficheExport.setGeoloc(coordinates[1], coordinates[0])
        ficheExport.setHref(URL_ROOT + link["href"])

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding="UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "ritimo-geo.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding="UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_place
corpusMetadataExport = scrutariDataExport.newCorpus("lieu");
metadataWrapper.fillCorpus(corpusMetadataExport)

loadJson()

#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "ritimo-geo.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "ritimo-geo.scrutari-data.xml")
scrutariInfoFile.close()
