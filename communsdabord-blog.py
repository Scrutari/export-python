#!/usr/bin/env python3

#-------------------------------------------------------------------
# http://www.ciriec.ulg.ac.be/
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
CATEGORY_URL = "http://www.les-communs-dabord.org/category/"
POST_URL = "http://www.les-communs-dabord.org/?p="


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def checkCategory(path, corpus):
    checkCategoryPage(CATEGORY_URL + path + "/", corpus)

def checkCategoryPage(url, corpus):
    htmlContent = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlContent,"lxml")
    next = soup.find("link", rel="next")
    divs = soup.find_all("div", class_="blogpost-entry")
    for div in divs:
        addPost(div, corpus)
    if next:
        checkCategoryPage(next["href"], corpus)

def addPost(div, corpus):
    ficheId = div["id"][5:]
    titreH2 = div.find("h2", class_="entry-title")
    soustitreDiv = div.find("div", class_="entry-content")
    authorLine = div.find("p", class_="author").get_text()
    idx = len(authorLine) -5
    annee = authorLine[idx:idx+4]
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setLang("fr")
    ficheExport.setTitre(titreH2.get_text())
    ficheExport.setSoustitre(soustitreDiv.get_text())
    ficheExport.setHref(POST_URL + ficheId)
    ficheExport.setDate(annee)
    lis = div.find_all("li", class_="keyword")
    for li in lis:
        link = li.find("a")
        href = link["href"]
        href = href[:len(href)-1]
        index = href.rfind("/")
        key = href[index +1:]
        scrutariDataExport.addIndexation(corpus, ficheId, "tag", key, 1)
        label = link.get_text()
        tagBuffer.checkMotcle(key, "fr", label[1:])
      
    


#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

tagBuffer = ThesaurusBuffer("tag")


#wrapper
yamlFile = open(YAML_FILE, encoding = "UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "communsdabord-blog.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding="UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_action
corpusMetadataExport = scrutariDataExport.newCorpus("action");
metadataWrapper.fillCorpus(corpusMetadataExport)

checkCategory("communs-action", "action")

#corpus_debat
corpusMetadataExport = scrutariDataExport.newCorpus("debat");
metadataWrapper.fillCorpus(corpusMetadataExport)

checkCategory("debats", "debat")

#thesaurus_sector
thesaurusMetadataExport = scrutariDataExport.newThesaurus("tag");
metadataWrapper.fillThesaurus(thesaurusMetadataExport)
tagBuffer.exportMotscles(scrutariDataExport)

#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "communsdabord-blog.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "communsdabord-blog.scrutari-data.xml")
scrutariInfoFile.close()
