#!/usr/bin/env python3

#-------------------------------------------------------------------
# http://redeconvergir.net/
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re, json, yaml, csv
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
JSON_URL = "https://www.reparatur-initiativen.de/pmc.ajax.php?task=map"
ROOT_URL = "https://www.reparatur-initiativen.de/"


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def loadJson():
    jsonResponse = urllib.request.urlopen(JSON_URL)
    jsonData = json.loads(jsonResponse.read().decode("UTF-8"))
    for item in jsonData:
        workshop = item["Workshop"]
        ficheId = workshop["uid"]
        ficheExport = scrutariDataExport.newFiche(ficheId)
        ficheExport.setTitre(workshop["name"])
        ficheExport.setHref(ROOT_URL + workshop["url"])
        ficheExport.setGeoloc(workshop["lat"], workshop["lng"])
        ficheExport.setLang("de")
        if "city" in workshop:
            ficheExport.addAttributeValue("geo", "city", workshop["city"])

    
#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding="UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "reparatur-geo.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding="UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_workshop
corpusMetadataExport = scrutariDataExport.newCorpus("workshop")
metadataWrapper.fillCorpus(corpusMetadataExport)
loadJson()
    
#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "reparatur-geo.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "reparatur-geo.scrutari-data.xml")
scrutariInfoFile.close()