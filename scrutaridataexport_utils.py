import sys, shutil, urllib.request, unicodedata, re, yaml, json, csv
import langid
from scrutaridataexport import *

def utils_hrefToFicheId(href):
    query = href.find("?")
    if query > 0:
        href = href[0:query]
    index = href.rfind("/")
    lastIndex = len(href) - 1
    if index == lastIndex:
        href = href[0:lastIndex]
        index = href.rfind("/")
    name = href[index+1:].lower()
    name = name.replace("%20", " ")
    name = name.replace(".", "_")
    name = utils_slugify(name)
    return name

def utils_slugify(value):
    """
    Convert to ASCII if 'allow_unicode' is False. Convert spaces to hyphens.
    Remove characters that aren't alphanumerics, underscores, or hyphens.
    Convert to lowercase. Also strip leading and trailing whitespace.
    """
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub('[^\w\s-]', '', value).strip().lower()
    return re.sub('[-\s]+', '-', value)

def utils_urlopen(url, host):
    request = urllib.request.Request(url, headers={"Host": host, "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0", "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8", "Accept-Language": "fr,fr-FR;q=0.5"})
    return urllib.request.urlopen(request)

def utils_toJson(response):
    return json.loads(response.read().decode("UTF-8"))

def utils_langidentify(text):
    result = langid.classify(text)
    return result[0]

def utils_initMetadataWrapper(yamlPath):
    yamlFile = open(yamlPath, encoding="UTF-8")
    dataMap = yaml.safe_load(yamlFile)
    yamlFile.close()
    return MetadataWrapper(dataMap)

def utils_openScrutariDataFile(directory, name):
    return open(directory + name + ".scrutari-data.xml.part", "w", encoding = "UTF-8")

def utils_closeScrutariDataFile(directory, name, scrutariDataFile):
    scrutariDataFile.close()
    shutil.move(directory + name + ".scrutari-data.xml.part", directory + name + ".scrutari-data.xml")

def utils_writeScrutariInfoFile(directory, name):
    scrutariInfoFile = open(directory + name + ".scrutari-info.xml", "w", encoding = "UTF-8")
    writeScrutariInfo(XmlWriter(scrutariInfoFile), name + ".scrutari-data.xml")
    scrutariInfoFile.close()
    
def utils_initDictFromCsv(csvPath):
    result = dict() 
    csvFile = open(csvPath, "r", encoding="UTF-8", newline='')
    csvreader = csv.reader(csvFile, delimiter=',')
    for row in csvreader:
        if len(row) > 1:
            firstCell = row[0].strip()
            if len(firstCell) > 0:
                result[firstCell] = row[1]
    csvFile.close()
    return result
