#!/usr/bin/env python3
#-------------------------------------------------------------------
# http://www.microfinancegateway.org
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re, yaml
from bs4 import BeautifulSoup
import bs4
from scrutaridataexport import *
import pycurl
from io import BytesIO

#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
RACINE_SITE = "http://www.microfinancegateway.org"
RACINE_ANGLAIS = "http://www.microfinancegateway.org/library?f[0]=field_publication_type%3A"
RACINE_FRANCAIS = "http://www.microfinancegateway.org/fr/biblioth%C3%A8que?f[0]=field_publication_type%3A"
RACINE_ESPAGNOL = "http://www.microfinancegateway.org/es/biblioteca?f%5B0%5D=field_publication_type%3A"

#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def check(root, lang):
    checkType(root, lang, "17")
    checkType(root, lang, "33")
    checkType(root, lang, "19")
    checkType(root, lang, "20")
    
def checkType(root, lang, type):
    startUrl = root + type
    htmlContent = getContent(startUrl)
    soup = BeautifulSoup(htmlContent,"lxml")
    parseList(soup, lang)
    lastLink = soup.find("a", class_="pager__link pager__link--last")
    lastPage = getLastPage(lastLink["href"])
    for i in range(1,lastPage):
        url = startUrl + "&page=" + str(i)
        pageContent = getContent(url)
        parseList(BeautifulSoup(pageContent,"lxml"), lang)
    
def parseList(soup, lang):
    divs = soup.find_all("div", class_="views-row")
    for div in divs:
        parsePage(div, lang)
        
def parsePage(page, lang):
    titleDiv = page.find("div", class_="field-name-title-field")
    titleLink = titleDiv.find("a")
    href = titleLink["href"]
    idx = href.rfind('/')
    ficheId = href[idx+1:]
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setLang(lang)
    ficheExport.setHref(RACINE_SITE + href)
    ficheExport.setTitre(titleLink.get_text())
    soustitreDiv = page.find("div", class_="field-name-field-publication-sub-title")
    if soustitreDiv:
        ficheExport.setSoustitre(soustitreDiv.get_text())
    authorDiv = page.find("div", class_="field-name-field-publication-author")
    if authorDiv:
        ficheExport.addAttributeValue("sct", "authors", getAuthors(authorDiv))
    dateDiv = page.find("div", class_="field-name-field-publication-date-published")
    if dateDiv:
        dateSpan = dateDiv.find("span", class_="date-display-single")
        if dateSpan:
            date = dateSpan.get_text()
            ficheExport.setDate(date[len(date) -4:])
    
    

def getContent(url):
    buffer = BytesIO()
    c = pycurl.Curl()
    c.setopt(c.URL, url)
    c.setopt(c.WRITEDATA, buffer)
    c.setopt(c.HTTPHEADER, ['User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0'])
    c.perform()
    c.close()
    body = buffer.getvalue()
    return body.decode('UTF-8')

def getAuthors(div):
    s = ''.join(e for e in div if type(e) is bs4.element.NavigableString)
    return s

def getLastPage(href):
    idx = href.find("page=")
    part = href[(idx +5):]
    idx2 = part.find("&")
    part2 = part[:idx2]
    return int(part2)

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding = "UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "microfinancegateway-library.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding="UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_multimedia
corpusMetadataExport = scrutariDataExport.newCorpus("page");
metadataWrapper.fillCorpus(corpusMetadataExport)

check(RACINE_ANGLAIS, "en")
check(RACINE_FRANCAIS, "fr")
check(RACINE_ESPAGNOL, "es")


#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "microfinancegateway-library.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "microfinancegateway-library.scrutari-data.xml")
scrutariInfoFile.close()
