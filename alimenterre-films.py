#!/usr/bin/env python3
#-------------------------------------------------------------------
# Script de génération du fichier ScrutariData pour le fichier CSV films
# d'Alimenterre
# Ce script ne s'occupe pas du téléchargement du fichier CSV
#-------------------------------------------------------------------

#-------------------------------------------------------------------
# Les modules utilisés sont standards
# scrutaridataexport correspond au contenu du fichier scrutaridataexport.py
#-------------------------------------------------------------------

import sys, shutil, csv
from scrutaridataexport import *


#-------------------------------------------------------------------
# Ce script a un seul argument obligatoire : le chemin du répertoire de destination
# où sera enregistré les fichiers alimenterre_ressources.scrutari-data.xml et alimenterre_ressources.scrutari-info.xml
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
CSVFILE = DESTINATION_DIRECTORY + "films.csv"


#-------------------------------------------------------------------
# Fonctions de traitement
#-------------------------------------------------------------------

def parseRow(row):
    lang = row[0].lower()
    if not lang == "fr":
        return
    ficheId = row[1]
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setLang(lang)
    ficheExport.setTitre(row[2])
    ficheExport.setHref(row[3])
    ficheExport.setDate(row[4])
    ficheExport.addComplement(1, row[5])
    realisateurs = row[6].split(',')
    for realisateur in realisateurs:
        ficheExport.addAttributeValue("sct", "authors", realisateur)
    motscles = row[7].split(",")
    for motcle in motscles:
        contenu = motcle.split(":")
        if len(contenu) > 1:
            motcleId = contenu[0].strip()
            texte = contenu[1].strip()
            if (len(motcleId) > 0) and (not texte.startswith("-")):
                motsclesThesaurus.checkMotcle(motcleId, lang, texte)
                scrutariDataExport.addIndexation("ressources", ficheId, "motscles", motcleId, 1)
            

#-------------------------------------------------------------------
# Procédure principale
#-------------------------------------------------------------------

#Construction du thésaurus
motsclesThesaurus = ThesaurusBuffer()

#Départ de l'exportation
scrutariDataPath = DESTINATION_DIRECTORY + "alimenterre-films.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#Initialisation de la base d'Alimenterre
baseMetadataExport = scrutariDataExport.startExport()
baseMetadataExport.setAuthority("17b13182-1980-4082-8c2a-b606ff378bff")
baseMetadataExport.setBaseName("alimenterre_films")
baseMetadataExport.setBaseIcon("http://static.scrutari.net/alimenterre.png")
baseMetadataExport.setIntitule(INTITULE_SHORT, "fr", "Alimenterre")
baseMetadataExport.setIntitule(INTITULE_LONG, "fr", "Films du site Alimenterre")

#Création du corpus Ressources
corpusMetadataExport = scrutariDataExport.newCorpus("ressources");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Films")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Film n°")
numero = corpusMetadataExport.addComplement()
corpusMetadataExport.setComplementIntitule(numero, "fr", "Producteurs")


with open(CSVFILE, "r", encoding="UTF-8", newline='') as csvfile:
    csvreader = csv.reader(csvfile, delimiter=';')
    first = True
    for row in csvreader:
        if not first:
            parseRow(row)
        else:
            first = False


#Création du thésaurus Mot-clé
thesaurusMetadataExport = scrutariDataExport.newThesaurus("motscles")
thesaurusMetadataExport.setIntitule(INTITULE_THESAURUS, "fr","Mots-clés")
motsclesThesaurus.exportMotscles(scrutariDataExport)

#Fin de l'export
scrutariDataExport.endExport()
scrutariDataFile.close()

#Renommage du fichier temporaire
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#Création du fichier d'information avec la date du jour
scrutariInfoFile = open(DESTINATION_DIRECTORY + "alimenterre-films.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "alimenterre-films.scrutari-data.xml")
scrutariInfoFile.close()
