#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://presdecheznous.fr
#-------------------------------------------------------------------

import sys, shutil, urllib.request, json, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
ELEMENTS_JSON_FILE = sys.argv[2]
TAXONOMY_JSON_FILE = sys.argv[3]
YAML_FILE = sys.argv[4]
ROOT_URL = "https://presdecheznous.fr/annuaire#/fiche/acteur/"


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

                    
def scanElementsJson(jsonData):
    for feature in jsonData["data"]:
        addFeature(feature)
        
def scanTaxonomyJson(jsonData):
    for option in jsonData[0]["options"]:
        scanOption(option)
        
def scanOption(option):
    motcleExport = scrutariDataExport.newMotcle(option["id"])
    motcleExport.setLibelle("fr", option["name"])
    if "subcategories" in option:
        for subcategory in option["subcategories"]:
            if "options" in subcategory:
                for opt2 in subcategory["options"]:
                    scanOption(opt2)
    

def addFeature(feature):
    coordinates = feature["geo"]
    ficheId = str(feature["id"])
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setLang("fr")
    ficheExport.setGeoloc(coordinates["latitude"], coordinates["longitude"])
    ficheExport.setTitre(feature["name"])
    if "description" in feature:
        ficheExport.setSoustitre(feature["description"])
    ficheExport.setHref(ROOT_URL + ficheId + "/")
    if "website" in feature:
        ficheExport.addAttributeValue("sct", "website", feature["website"])
    if "address" in feature:
        address = feature["address"]
        if "addressLocality" in feature:
            ficheExport.addAttributeValue("geo", "city", address["addressLocality"])
    if "categories" in feature:
        for motcleId in feature["categories"]:
            scrutariDataExport.addIndexation("acteur", ficheId, "categorie", motcleId, 1)
    

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding="UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "presdecheznous-geo.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_acteur
corpusMetadataExport = scrutariDataExport.newCorpus("acteur");
metadataWrapper.fillCorpus(corpusMetadataExport)

#chargement des éléments
with open(ELEMENTS_JSON_FILE, encoding="UTF-8") as data_file:    
    jsonData = json.load(data_file)
    scanElementsJson(jsonData)
    
#thesaurus_categorie
thesaurusMetadataExport = scrutariDataExport.newThesaurus("categorie");
metadataWrapper.fillThesaurus(thesaurusMetadataExport)

#chargement de la taxonomie
with open(TAXONOMY_JSON_FILE, encoding="UTF-8") as data_file:    
    jsonData = json.load(data_file)
    scanTaxonomyJson(jsonData)

#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "presdecheznous-geo.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "presdecheznous-geo.scrutari-data.xml")
scrutariInfoFile.close()
