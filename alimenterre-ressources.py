#!/usr/bin/env python3
#-------------------------------------------------------------------
# Script de génération du fichier ScrutariData pour le fichier CSV ressources
# d'Alimenterre
# Ce script ne s'occupe pas du téléchargement du fichier CSV
#-------------------------------------------------------------------

#-------------------------------------------------------------------
# Les modules utilisés sont standards
# scrutaridataexport correspond au contenu du fichier scrutaridataexport.py
#-------------------------------------------------------------------

import sys, shutil, csv
from scrutaridataexport import *


#-------------------------------------------------------------------
# Ce script a un seul argument obligatoire : le chemin du répertoire de destination
# où sera enregistré les fichiers alimenterre_ressources.scrutari-data.xml et alimenterre_ressources.scrutari-info.xml
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
CSVFILE = DESTINATION_DIRECTORY + "ressources.csv"


#-------------------------------------------------------------------
# Fonctions de traitement
#-------------------------------------------------------------------

def parseRow(row):
    lang = row[0].lower()
    ficheId = row[1]
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setLang(lang)
    ficheExport.setTitre(row[2])
    ficheExport.setHref(row[3])
    auteurs = row[4].split(',')
    for auteur in auteurs:
        ficheExport.addAttributeValue("sct", "authors", auteur)
    ficheExport.setDate(row[5])
    ficheExport.addComplement(1, row[6])
    motscles = row[7].split(",")
    for motcle in motscles:
        contenu = motcle.split(":")
        if len(contenu) > 1:
            motcleId = contenu[0].strip()
            texte = contenu[1].strip()
            if (len(motcleId) > 0) and (not texte.startswith("-")):
                motsclesThesaurus.checkMotcle(motcleId, lang, texte)
                scrutariDataExport.addIndexation("ressources", ficheId, "motscles", motcleId, 1)
                

#-------------------------------------------------------------------
# Procédure principale
#-------------------------------------------------------------------

#Construction du thésaurus
motsclesThesaurus = ThesaurusBuffer()

#Départ de l'exportation
scrutariDataPath = DESTINATION_DIRECTORY + "alimenterre-ressources.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#Initialisation de la base d'Alimenterre
baseMetadataExport = scrutariDataExport.startExport()
baseMetadataExport.setAuthority("17b13182-1980-4082-8c2a-b606ff378bff")
baseMetadataExport.setBaseName("alimenterre")
baseMetadataExport.setBaseIcon("http://static.scrutari.net/alimenterre.png")
baseMetadataExport.setIntitule(INTITULE_SHORT, "fr", "Alimenterre")
baseMetadataExport.setIntitule(INTITULE_SHORT, "en", "Alimenterre")
baseMetadataExport.setIntitule(INTITULE_LONG, "fr", "Ressources du site Alimenterre")
baseMetadataExport.setIntitule(INTITULE_LONG, "en", "Alimenterre Resources")
baseMetadataExport.addLangUI("fr")
baseMetadataExport.addLangUI("en")

#Création du corpus Ressources
corpusMetadataExport = scrutariDataExport.newCorpus("ressources");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Ressources documentaires")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "en","Resources")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Resource documentaire n°")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "en", "Resource #")
numero = corpusMetadataExport.addComplement()
corpusMetadataExport.setComplementIntitule(numero, "fr", "Éditeur/diffuseur")
corpusMetadataExport.setComplementIntitule(numero, "en", "Publisher/distributor")


with open(CSVFILE, "r", encoding="UTF-8", newline='') as csvfile:
    csvreader = csv.reader(csvfile, delimiter=';')
    first = True
    for row in csvreader:
        if not first:
            parseRow(row)
        else:
            first = False


#Création du thésaurus Mot-clé
thesaurusMetadataExport = scrutariDataExport.newThesaurus("motscles")
thesaurusMetadataExport.setIntitule(INTITULE_THESAURUS, "fr","Mots-clés")
thesaurusMetadataExport.setIntitule(INTITULE_THESAURUS, "en","Keywords")
motsclesThesaurus.exportMotscles(scrutariDataExport)

#Fin de l'export
scrutariDataExport.endExport()
scrutariDataFile.close()

#Renommage du fichier temporaire
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#Création du fichier d'information avec la date du jour
scrutariInfoFile = open(DESTINATION_DIRECTORY + "alimenterre-ressources.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "alimenterre-ressources.scrutari-data.xml")
scrutariInfoFile.close()