import sys, shutil, urllib.request, json
from bs4 import BeautifulSoup

def wp_getFirstParagraph(item, fieldName):
    if fieldName in item:
        fieldValue = item[fieldName]
        if "rendered" in fieldValue:
            soup = BeautifulSoup(fieldValue["rendered"], "lxml")
            firstP = soup.find("p")
            if firstP:
                return firstP.get_text()
    return ""

def wp_getText(item, fieldName):
    if fieldName in item:
        fieldValue = item[fieldName]
        if "rendered" in fieldValue:
            soup = BeautifulSoup(fieldValue["rendered"], "lxml")
            return soup.get_text()
    return ""

def wp_getDate(item):
    date = item["date"][0:10]
    if date[0] == "0":
        date = item["modified"][0:10]
    return date

def wp_readPages(rootUrl, host, readFunction):
    jsonResponse = wp_getJsonReponse(rootUrl, host)
    totalPages = int(jsonResponse.getheader("X-WP-TotalPages"))
    readFunction(jsonResponse)
    if totalPages > 1:
      for i in range(2, totalPages + 1):
        jsonResponse =  wp_getJsonReponse(rootUrl + "&page=" + str(i), host)
        readFunction(jsonResponse)

def wp_populateTaxonomyMap(itemMap, taxonomyUrl, host):
    jsonResponse = wp_getJsonReponse(taxonomyUrl, host)
    totalPages = int(jsonResponse.getheader("X-WP-TotalPages"))
    wp_populateTaxonomyMapFromPage(itemMap, jsonResponse)
    if totalPages > 1:
      for i in range(2, totalPages + 1):
        jsonResponse = wp_getJsonReponse(taxonomyUrl + "&page=" + str(i), host)
        wp_populateTaxonomyMapFromPage(itemMap, jsonResponse)
        
def wp_populateTaxonomyMapFromPage(itemMap, jsonResponse):
    jsonData = json.loads(jsonResponse.read().decode("UTF-8"))
    for item in jsonData:
        itemMap[item["id"]] = item["name"]
        

def wp_populateThesaurus(thesaurusBuffer, taxonomyUrl, lang, host):
    jsonResponse = wp_getJsonReponse(taxonomyUrl,  host)
    totalPages = int(jsonResponse.getheader("X-WP-TotalPages"))
    wp_checkMotcles(thesaurusBuffer, jsonResponse, lang)
    if totalPages > 1:
      for i in range(2, totalPages + 1):
        jsonResponse = wp_getJsonReponse(taxonomyUrl + "&page=" + str(i), host)
        wp_checkMotcles(thesaurusBuffer, jsonResponse, lang)
        
def wp_checkMotcles(thesaurusBuffer, jsonResponse, lang) :
    jsonData = json.loads(jsonResponse.read().decode("UTF-8"))
    for item in jsonData:
        label = BeautifulSoup(item["name"],"lxml").get_text()
        thesaurusBuffer.checkMotcle(str(item["id"]), lang, label)

def wp_getJsonReponse(url, host):
    request = urllib.request.Request(url, headers={"Host": host, "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0", "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8", "Accept-Language": "fr,fr-FR;q=0.5"})
    return urllib.request.urlopen(request)
