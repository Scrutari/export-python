#!/usr/bin/env python3

#-------------------------------------------------------------------
# http://www.resolis.org
#-------------------------------------------------------------------

import sys, shutil, urllib.request, json, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
ROOT_URL="https://www.resolis.org/"
PAGINATION_URL="https://www.resolis.org/consulter-les-pratiques-locales"

#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def scanList():
    url = ROOT_URL + "consulter-les-pratiques-locales"
    p = 2
    next = True
    while(next):
        next = False
        htmlContent = urllib.request.urlopen(url)
        soup = BeautifulSoup(htmlContent,"lxml")
        checkList(soup)
        nextLi = soup.find("li", class_="next")
        if nextLi:
            link = nextLi.find("a")
            if link:
                next = True
                url = ROOT_URL +  "consulter-les-pratiques-locales/" + str(p)
                p = p + 1
                
def checkList(soup):
    sections = soup.find_all("section", class_="mod")
    for section in sections:
        link = section.find("a", itemprop="url")
        if link:
            href = link["href"]
            idx = href.rfind("/")
            ficheId = href[idx+1:]
            ficheExport = scrutariDataExport.newFiche(ficheId)
            ficheExport.setLang("fr")
            ficheExport.setHref(ROOT_URL + href)
            h1 = section.find("h1")
            ficheExport.setTitre(h1.get_text())
            description = section.find("p", itemprop="description")
            if description:
                ficheExport.setSoustitre(getSoustitre(description))
            
            
def getSoustitre(description):
    br = description.find("br")
    if br:
        return br.previous_sibling
    else:
        return description.get_text()
        


#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding="UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "resolis-geo.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#Création du corpus Initiative
corpusMetadataExport = scrutariDataExport.newCorpus("initiative");
metadataWrapper.fillCorpus(corpusMetadataExport)

scanList()
   
#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "resolis-geo.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "resolis-geo.scrutari-data.xml")
scrutariInfoFile.close()
