#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://www.corporateeurope.org
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
RACINE_SITE="https://www.corporateeurope.org"
MAX=1000

def checkPagination():
    htmlContent = urllib.request.urlopen(RACINE_SITE)
    soup = BeautifulSoup(htmlContent,"lxml")
    checkFrontpage(soup)
    pagination = 1
    while(pagination < MAX):
        pageHtmlContent = urllib.request.urlopen(RACINE_SITE + "/?page=" + str(pagination))
        pageSoup = BeautifulSoup(pageHtmlContent,"lxml")
        done = checkFrontpage(pageSoup)
        pagination = pagination + 1
        if not done:
            break
    

def checkFrontpage(soup):
    divList = soup.find("div", class_="view-article-list").find_all("div", class_="views-row")
    done = False
    for div in divList:
        done = True
        link = div.find("h2").find("a")
        title = link.get_text().strip()
        summary = div.find("div", class_="summary").get_text()
        date = div.find("span", class_="date").get_text()
        checkArticle(link["href"], title, summary, date)
    return done
        
def checkArticle(href, title, summary, date):
    url = RACINE_SITE + href
    htmlContent = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlContent,"lxml")
    shortLink = soup.find("link", rel="shortlink")["href"]
    section = soup.find("section", id="section-content")
    h2 = section.find("h2")
    if h2:
        h2text = h2.get_text().strip()
        if len(h2text) > 0:
            if title[len(title) -1] != "?":
                title = title + "."
            title = title + " " + h2text
    idx = shortLink.rfind("/")
    lang = "en"
    if (shortLink.find("/fr/") > 0):
        lang = "fr"
    ficheId = shortLink[idx+1:]
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setLang(lang)
    ficheExport.setHref(shortLink)
    ficheExport.setTitre(title)
    ficheExport.setSoustitre(checkSoustitre(summary))
    ficheExport.setDate(checkDate(date))

def checkDate(dateText):
    idx = dateText.rfind(" ")
    date = dateText[idx+1:]
    idx2 = dateText.find(" ")
    month = checkMonth(dateText[0:idx2])
    if len(month) > 0:
        date = date + "-" +  month
    return date

def checkMonth(month):
    if month == 'January':
        return "01"
    if month == 'February':
        return "02"
    if month == 'March':
        return "03"
    if month == 'April':
        return "04"
    if month == 'May':
        return "05"
    if month == 'June':
        return "06"
    if month == 'July':
        return "07"
    if month == 'August':
        return "08"
    if month == 'September':
        return "09"
    if month == 'October':
        return "10"
    if month == 'November':
        return "11"
    if month == 'December':
        return "12"
    return ""

def checkSoustitre(soustitre):
    soustitreLen = len(soustitre)
    if soustitreLen < 150:
        return soustitre
    idx = soustitre.find(".", 150, soustitreLen)
    if idx > 0:
        return soustitre[0:(idx+1)]
    return soustitre
 

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding = "UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "corporateeurope.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_report
corpusMetadataExport = scrutariDataExport.newCorpus("report");
metadataWrapper.fillCorpus(corpusMetadataExport)

#checkPagination()
#checkCategory("power-lobbies")
#checkCategory("economy-finance")
#checkCategory("environment")
#checkCategory("food-and-agriculture")
#checkCategory("international-trade")


#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "corporateeurope.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "corporateeurope.scrutari-data.xml")
scrutariInfoFile.close()
