#!/usr/bin/env python3
#-------------------------------------------------------------------
# https://www.grain.org
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *
import pycurl
from io import BytesIO

#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
RACINE_SITE="https://www.grain.org"

#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def checkCategories(categoryId, langArray):
    for lang in langArray:
        checkCategory(categoryId, lang)


def checkCategory(categoryId, lang):
    rootUrl = RACINE_SITE + "/" + lang + "/category/" + categoryId
    htmlContent = getContent(rootUrl)
    soup = BeautifulSoup(htmlContent,"lxml")
    checkList(soup, lang)
    paginationUl = soup.find("ul", class_= "pagination")
    if paginationUl:
        pageLis = list(paginationUl.find_all("li"))
        if len(pageLis) > 0:
            lastLi = pageLis[len(pageLis) - 1]
            lastHref = lastLi.find("a")["href"]
            lastIdx = lastHref.rfind("=")
            lastPage = int(lastHref[(lastIdx+1):])
            for i in range(2,lastPage):
                pageHtmlContent = getContent(rootUrl +  "?page=" + str(i))
                pageSoup = BeautifulSoup(pageHtmlContent,"lxml")
                checkList(pageSoup, lang)
    

def checkList(soup, lang):
    entryList = soup.find_all("article", class_="post-news")
    for entry in entryList:
        entryId = entry["id"]
        idx = entryId.find("-")
        ficheId = entryId[(idx + 1):]
        titleLink = entry.find("h2").find("a")
        title = titleLink.get_text()
        href = titleLink["href"]
        ficheExport = scrutariDataExport.newFiche(ficheId)
        ficheExport.setLang(lang)
        ficheExport.setHref(RACINE_SITE + href)
        ficheExport.setTitre(title)
        metaP = entry.find("p", class_="entry-meta-article")
        if metaP:
            metaText = metaP.get_text()
            idx = metaText.find("|")
            metaText = metaText[(idx +1):].strip()
            idx = metaText.find("|")
            metaText = metaText[0:idx].strip()
            idx2 =  metaText.rfind(" ")
            ficheExport.setDate(metaText[idx2+1:])
        summaryP = entry.find("p", class_="summary-article")
        if summaryP:
            ficheExport.setSoustitre(checkSoustitre(summaryP.get_text().strip()))

def checkSoustitre(soustitre):
    soustitreLen = len(soustitre)
    if soustitreLen < 150:
        return soustitre
    idx = soustitre.find(".", 150, soustitreLen)
    if idx > 0:
        return soustitre[0:(idx+1)]
    return soustitre
 
def getContent(url):
    buffer = BytesIO()
    c = pycurl.Curl()
    c.setopt(c.URL, url)
    c.setopt(c.WRITEDATA, buffer)
    c.setopt(c.HTTPHEADER, ['User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0'])
    c.perform()
    c.close()
    body = buffer.getvalue()
    return body.decode('UTF-8')

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding = "UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "grain.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding="UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_article
corpusMetadataExport = scrutariDataExport.newCorpus("article");
metadataWrapper.fillCorpus(corpusMetadataExport)

checkCategories("537", ["en", "es", "fr"])
checkCategories("539", ["en", "es", "fr"])
checkCategories("540", ["en", "es", "fr"])
checkCategories("538", ["en", "es", "fr"])


#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "grain.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "grain.scrutari-data.xml")
scrutariInfoFile.close()
