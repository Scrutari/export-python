#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://wiki.remixthecommons.org
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re, json, yaml, unicodedata
from bs4 import BeautifulSoup
from lxml import etree
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
JSON_URL="https://wiki.remixthecommons.org/index.php?title=Special%3AAsk&q=%5B%5BCat%C3%A9gorie%3AObjet+m%C3%A9dia%5D%5D&po=%3FType+de+document%3DM%C3%A9dia%0A%3FDur%C3%A9e&p%5Blimit%5D=1000&p%5Boffset%5D=0&p%5Bsortkeys%5D%5B%5D=ASC&p%5Bmainlabel%5D=&p%5Bquerymode%5D=1&p%5Bformat%5D=json&p%5Bsource%5D=&p%5Blink%5D=all&p%5Bheaders%5D=plain&p%5Bintro%5D=&p%5Boutro%5D=&p%5Bsearchlabel%5D=JSON&p%5Bdefault%5D=&p%5Bclass%5D=table-bordered&p%5Btheme%5D=bootstrap"
URL_PREFIX="https://wiki.remixthecommons.org/index.php/"
MEDIA_DICT=dict()
RDF_ROOT="https://wiki.remixthecommons.org/index.php?xmlmime=rdf&title=Sp%C3%A9cial:Export_RDF/"
HREF_ROOT="https://wiki.remixthecommons.org/index.php?title="

RDF = '{http://www.w3.org/1999/02/22-rdf-syntax-ns#}'
RDFS = '{http://www.w3.org/2000/01/rdf-schema#}'
OWl = '{http://www.w3.org/2002/07/owl#}'
SWIVT = '{http://semantic-mediawiki.org/swivt/1.0#}'
WIKI = '{https://wiki.remixthecommons.org/index.php?title=Sp%C3%A9cial:URIResolver/}'
CATEGORY = '{https://wiki.remixthecommons.org/index.php?title=Sp%C3&%A9cial:URIResolver/Category-3A}'
PROPERTY = '{https://wiki.remixthecommons.org/index.php?title=Sp%C3%A9cial:URIResolver/Property-3A}'
NAMESPACES = {'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
              'rdfs': 'http://www.w3.org/2000/01/rdf-schema#',
              'owl': 'http://www.w3.org/2002/07/owl#',
              'swivt': 'http://semantic-mediawiki.org/swivt/1.0#',
              'property': 'https://wiki.remixthecommons.org/index.php?title=Sp%C3%A9cial:URIResolver/Property-3A'
             }

#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------
conceptSet = set()


def loadList():
    jsonResponse = urllib.request.urlopen(JSON_URL)
    jsonObj = json.loads(jsonResponse.read().decode("UTF-8"))
    values = jsonObj["results"].values()
    prefixLen = len(URL_PREFIX)
    for entry in values:
        fullurl = entry["fullurl"]
        name = fullurl[prefixLen:]
        if "Média" in entry["printouts"]:
            medias = entry["printouts"]["Média"]
            if len(medias) > 0:
                media = medias[0]
                if media in MEDIA_DICT:
                    MEDIA_DICT[media].append(name)
                else:
                    MEDIA_DICT[media] = [name]
                    
def loadRdf(media, corpus):
    for name in MEDIA_DICT[media]:
        url = RDF_ROOT + name
        file = urllib.request.urlopen(url)
        data = file.read()
        file.close()
        tree = etree.fromstring(data)
        swivtSubjects = tree.xpath("swivt:Subject", namespaces=NAMESPACES)
        if len(swivtSubjects) > 0:
            checkSubject(swivtSubjects[0], corpus)
            
            
def checkSubject(swivtSubject, corpus):
    about = swivtSubject.get(RDF + "about")
    idx = about.find("URIResolver/")
    ficheId = about[idx + 12:]
    ficheId = ficheId.replace("/", "-2F")
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setHref(about)
    label = swivtSubject.find("rdfs:label", namespaces=NAMESPACES)
    if label is not None:
        ficheExport.setTitre(label.text)
    lang = swivtSubject.find("property:Langue_du_contenu", namespaces=NAMESPACES)
    if lang is not None:
        langText = lang.text.lower()
        if len(langText) == 2:
            ficheExport.setLang(langText)
    description = swivtSubject.find("property:Description", namespaces=NAMESPACES)
    if description is not None:
        ficheExport.setSoustitre(checkSoustitre(description.text))
    date = swivtSubject.find("property:Date_de_publication", namespaces=NAMESPACES)
    if date is not None:
        ficheExport.setDate(date.text[0:10])
    auteurs = swivtSubject.xpath("property:Auteur", namespaces=NAMESPACES)
    for auteur in auteurs:
        text = convertResource(auteur.get(RDF + "resource"))
        ficheExport.addAttributeValue("sct","authors", text)
    checkConcept(swivtSubject, corpus, ficheId, "property:Enjeu")
    checkConcept(swivtSubject, corpus, ficheId, "property:Action_du_type")
    checkConcept(swivtSubject, corpus, ficheId, "property:Cat-C3-A9gorie_Hess")
    checkConcept(swivtSubject, corpus, ficheId, "property:R-C3-A9sultat")
  
def checkConcept(swivtSubject, corpus, ficheId, propertyName):
    concepts = swivtSubject.xpath(propertyName, namespaces=NAMESPACES)
    for concept in concepts:
        key = getKey(concept.get(RDF + "resource"))
        scrutariDataExport.addIndexation(corpus, ficheId, "concept", key, 1)
        conceptSet.add(key)
        
            
            
def convertResource(resource):
    text = getKey(resource)
    text = convertText(text)
    return text

def getKey(resource):
    idx = resource.rfind("/")
    return resource[idx+1:]

def convertText(text):
    text = text.replace("-", "%")
    text = urllib.parse.unquote(text)
    text = text.replace("_", " ")
    return text

def checkSoustitre(soustitre):
    soustitreLen = len(soustitre)
    if soustitreLen < 250:
        return soustitre
    idx = soustitre.find(".", 250, soustitreLen)
    if idx > 0:
        return soustitre[0:(idx+1)]
    return soustitre[0:250]
 
#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding = "UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "remixthecommons-wiki.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding="UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

loadList()

#corpus_video
corpusMetadataExport = scrutariDataExport.newCorpus("video");
metadataWrapper.fillCorpus(corpusMetadataExport)

loadRdf("Vidéo", "video")


#thesaurus_concept
thesaurusMetadataExport = scrutariDataExport.newThesaurus("concept");
metadataWrapper.fillThesaurus(thesaurusMetadataExport)

for concept in conceptSet:
    motcleExport = scrutariDataExport.newMotcle(concept)
    motcleExport.setLibelle("fr", convertText(concept))


#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "remixthecommons-wiki.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "remixthecommons-wiki.scrutari-data.xml")
scrutariInfoFile.close()
