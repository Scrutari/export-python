#!/usr/bin/env python3
#-------------------------------------------------------------------
# Script de génération du fichier ScrutariData pour le site https://www.wm-urban-habitat.org
# Ce script récupère sur la liste des pays à partir des pages d'accueil par langue
# et crèe une entrée par pays
#-------------------------------------------------------------------

#-------------------------------------------------------------------
# Ce script utilise le module BeautifulSoup http://www.crummy.com/software/BeautifulSoup/
# ainsi que le module lxml (voir l'appel de BeautifulSoup(htmlContent, "lxml"))
# Les autres modules utilisés sont standards
# scrutaridataexport correspond au contenu du fichier scrutaridataexport.py
#-------------------------------------------------------------------

import sys, shutil, urllib.request
from bs4 import BeautifulSoup
from scrutaridataexport import *

#-------------------------------------------------------------------
# Ce script a un seul argument obligatoire : le chemin du répertoire de destination
# où sera enregistré les fichiers habitatparticipation-mappemonde.scrutari-data.xml et habitatparticipation-mappemonde.scrutari-info.xml
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
ACCUEIL_MAPPEMONDE_FR="https://www.wm-urban-habitat.org/fra"
ACCUEIL_MAPPEMONDE_EN="https://www.wm-urban-habitat.org/eng"
ACCUEIL_MAPPEMONDE_ES="https://www.wm-urban-habitat.org/esp"
ACCUEIL_SITE_FR="https://www.habitat-participation.be/"



def checkMappemondeMenu(url, lang, menuId):
    htmlContent = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlContent,"lxml")
    links = soup.select("#" + menuId + " > li > ul li.menu-item-type-post_type > a")
    for link in links:
        checkMappemondeLink(link,lang)
        

def checkMappemondeLink(link,lang):
    href = link["href"]
    titre = link.get_text()
    if lang == "fr":
        titre = titre + " - Habitat urbain"
    elif lang == "en":
        titre = titre + " - Urban habitat"
    elif lang == "es":
        titre = titre + " - Habitat Urbano"
    length = len(href)
    idx1 = href.rfind("/", 0, length -1)
    idx2 = href.rfind("/", 0, idx1)
    ficheId = href[idx2+1:length -1]
    ficheId = ficheId.replace("/","_")
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setLang(lang)
    ficheExport.setTitre(titre)
    ficheExport.setHref(href)
    
def checkSiteMenu(url, lang):
    htmlContent = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlContent,"lxml")
    lis = soup.select("#menu-item-57 ul.sub-menu li")
    for li in lis:
        checkSiteLi(li,lang)
        
def checkSiteLi(li, lang):
    idAttr = li["id"]
    idx = idAttr.rfind("-")
    ficheId = idAttr[idx+1:]
    link = li.find("a")
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setLang(lang)
    ficheExport.setHref(link["href"])
    ficheExport.setTitre(link.get_text())
    
    

#-------------------------------------------------------------------
# Procédure principale
#-------------------------------------------------------------------

#Départ de l'exportation
scrutariDataPath = DESTINATION_DIRECTORY + "habitatparticipation-mappemonde.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))


baseMetadataExport = scrutariDataExport.startExport()
baseMetadataExport.setAuthority("eac435d3-2dca-44e3-be68-1a1df4424821")
baseMetadataExport.setBaseName("mappemonde")
baseMetadataExport.setBaseIcon("http://static.scrutari.net/habitatparticipation-mappemonde.png")
baseMetadataExport.setIntitule(INTITULE_SHORT, "fr", "Habitat Urbain")
baseMetadataExport.setIntitule(INTITULE_LONG, "fr", "Habitat Urbain, Mappemonde de l’Habitat vu par la Société Civile")
baseMetadataExport.setIntitule(INTITULE_SHORT, "en", "Urban Habitat")
baseMetadataExport.setIntitule(INTITULE_LONG, "en", "Urban Habitat, A World Map of Urban Habitat as Seen by Civil Society")
baseMetadataExport.setIntitule(INTITULE_SHORT, "es", "Habitat Urbano")
baseMetadataExport.setIntitule(INTITULE_LONG, "es", "Globo de Hábitat Urbano visto por la sociedad civil")
baseMetadataExport.addLangUI("fr")
baseMetadataExport.addLangUI("en")
baseMetadataExport.addLangUI("es")



#Création du corpus Pays
corpusMetadataExport = scrutariDataExport.newCorpus("pays");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Pays")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Pays n° ")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "en","Countries")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "en", "Article #")
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "es","Países")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "es", "País #")

#Récupération des pages avec la liste des articles par langue sur le site Mappemonde
checkMappemondeMenu(ACCUEIL_MAPPEMONDE_FR,"fr", "menu-top")
checkMappemondeMenu(ACCUEIL_MAPPEMONDE_EN,"en", "menu-menu")
checkMappemondeMenu(ACCUEIL_MAPPEMONDE_ES,"es", "menu-gauche")


#Création du corpus Thématique
corpusMetadataExport = scrutariDataExport.newCorpus("thematique");
corpusMetadataExport.setIntitule(INTITULE_CORPUS, "fr","Thématiques")
corpusMetadataExport.setIntitule(INTITULE_FICHE, "fr", "Thématique n° ")

checkSiteMenu(ACCUEIL_SITE_FR,"fr")



#Fin de l'export
scrutariDataExport.endExport()
scrutariDataFile.close()

#Renommage du fichier temporaire
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#Création du fichier d'information avec la date du jour
scrutariInfoFile = open(DESTINATION_DIRECTORY + "habitatparticipation-mappemonde.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "habitatparticipation-mappemonde.scrutari-data.xml")
scrutariInfoFile.close()