#!/usr/bin/env python3
#-------------------------------------------------------------------
# http://www.alliancesud.ch/
#-------------------------------------------------------------------

import sys, shutil, urllib.request, re, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *

#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
RACINE_SITE="http://www.alliancesud.ch"

#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def checkIndex(url, lang):
    page = 0
    validPage = True
    while(validPage):
        done = False
        pageUrl = url
        if page > 0:
            pageUrl = url + "?page=" + str(page)
        page = page + 1
        htmlContent = urllib.request.urlopen(pageUrl)
        soup = BeautifulSoup(htmlContent,"lxml")
        viewContent = soup.find("div", class_="view-content")
        if viewContent:
            teasers = viewContent.find_all("div", class_="node-teaser")
            if len(teasers) > 0:
                for teaser in teasers:
                    teaserDone = addTeaser(teaser, lang)
                    if (teaserDone):
                        done = True
        if not done:
            validPage = False


def addTeaser(teaser, lang):
    h2 = teaser.find("h2", class_="teaser-title")
    if not h2:
        return False
    link = h2.find("a")
    if not link:
        onclick = teaser["onclick"]
        href = onclick[len("location.href='"):len(onclick) - 1]
    else:
        href = link["href"]
    last = href.rfind('/')
    ficheId = href[last+1:]
    ficheExport = scrutariDataExport.newFiche(ficheId)
    ficheExport.setLang(lang)
    ficheExport.setHref(RACINE_SITE + href)
    ficheExport.setTitre(h2.get_text())
    textDiv = teaser.find("div", class_="teaser-text")
    if textDiv:
        ficheExport.setSoustitre(textDiv.get_text())
    dateDiv = teaser.find("div", class_="teaser-date")
    if dateDiv:
        date = dateDiv.get_text()
        last = date.rfind('.')
        if last:
            secondlast = date.rfind('.', 0, last -1)
            year = date[last+1:]
            month = date[secondlast + 1:last]
            if len(month) == 1:
                month = "0" + month
            ficheExport.setDate(year + "-" + month)
    return True
    



#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding = "UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "alliancesud-site.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding="UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_dossier
corpusMetadataExport = scrutariDataExport.newCorpus("dossier");
metadataWrapper.fillCorpus(corpusMetadataExport)

checkIndex("http://www.alliancesud.ch/de/infodoc/ressourcen/e-dossiers", "de")
checkIndex("http://www.alliancesud.ch/fr/infodoc/documentation/e-dossiers-et-fiches-pedagogiques", "fr")

#corpus_multimedia
corpusMetadataExport = scrutariDataExport.newCorpus("multimedia");
metadataWrapper.fillCorpus(corpusMetadataExport)

checkIndex("http://www.alliancesud.ch/fr/infodoc/documentation/multimedia", "und")



#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "alliancesud-site.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "alliancesud-site.scrutari-data.xml")
scrutariInfoFile.close()