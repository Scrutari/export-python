#!/usr/bin/env python3

#-------------------------------------------------------------------
# https://imaginationforpeople.org
#-------------------------------------------------------------------

import sys, shutil, urllib.request, json, yaml
from bs4 import BeautifulSoup
from scrutaridataexport import *


#-------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------

DESTINATION_DIRECTORY = sys.argv[1]
YAML_FILE = sys.argv[2]
JSON_LATLON_URL = "http://imaginationforpeople.org/fr/projects.json"
PROJECT_URL = "http://imaginationforpeople.org/fr/get-project-card"
ROOT_URL = "http://imaginationforpeople.org"


#-------------------------------------------------------------------
# Global
#-------------------------------------------------------------------

geoDict = dict()


#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

def loadGeoDict():
    jsonResponse = urllib.request.urlopen(JSON_LATLON_URL)
    jsonArray = json.loads(jsonResponse.read().decode("UTF-8"))
    for item in jsonArray:
        ficheId = str(item[0])
        if ficheId not in geoDict:
            url = PROJECT_URL + "?project_id=" + ficheId + "&location_id=" + str(item[1])
            response =  urllib.request.urlopen(url)
            soup = BeautifulSoup(response.read().decode("utf-8"), "lxml")
            ficheExport = scrutariDataExport.newFiche(ficheId)
            ficheExport.setGeoloc(str(item[3]), str(item[2]))
            parseProject(ficheExport, soup)
            geoDict[ficheId] = item
    
def parseProject(ficheExport, soup):
    top = soup.find("div", class_="top")
    h2 = top.find("h2")
    link = h2.find("a")
    href = link["href"]
    ficheExport.setTitre(h2.get_text())
    ficheExport.setHref(ROOT_URL + href)
    ficheExport.setLang(href[1:3])
    p = top.find("p")
    if p:
        ficheExport.setSoustitre(p.get_text())
        

#-------------------------------------------------------------------
# Main
#-------------------------------------------------------------------

#wrapper
yamlFile = open(YAML_FILE, encoding = "UTF-8")
dataMap = yaml.safe_load(yamlFile)
yamlFile.close()
metadataWrapper = MetadataWrapper(dataMap)

#start
scrutariDataPath = DESTINATION_DIRECTORY + "imaginationforpeople-geo.scrutari-data.xml"
scrutariDataFile = open(scrutariDataPath + ".part", "w", encoding = "UTF-8")
scrutariDataExport = ScrutariDataExport(XmlWriter(scrutariDataFile))

#metadata
baseMetadataExport = scrutariDataExport.startExport()
metadataWrapper.fillMetadata(baseMetadataExport)

#corpus_project
corpusMetadataExport = scrutariDataExport.newCorpus("organization");
metadataWrapper.fillCorpus(corpusMetadataExport)

loadGeoDict()

#end
scrutariDataExport.endExport()
scrutariDataFile.close()
shutil.move(scrutariDataPath + ".part", scrutariDataPath)

#scrutari-info.xml
scrutariInfoFile = open(DESTINATION_DIRECTORY + "imaginationforpeople-geo.scrutari-info.xml", "w", encoding = "UTF-8")
writeScrutariInfo(XmlWriter(scrutariInfoFile), "imaginationforpeople-geo.scrutari-data.xml")
scrutariInfoFile.close()
